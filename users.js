db.users.insertMany([
    {
        "_id" : "f9unuxzcaczizwZos",
        "createdAt" : ISODate("2019-11-26T12:18:59.425Z"),
        "services" : {
            "password" : {
                "bcrypt" : "$2b$10$N2a6W5Z.EdiYqxkyyvSYPeVKujDvJk4Cnvvf6d0Y9b.7Fh1mU4BdS"
            },
            "resume" : {
                "loginTokens" : [
                    {
                        "when" : ISODate("2019-12-07T17:51:55.977Z"),
                        "hashedToken" : "3L/7Fmv5d5arzIBGH8rMyKmYLwW3zbbXE3HOGysn6dw="
                    },
                    {
                        "when" : ISODate("2019-12-26T17:09:56.494Z"),
                        "hashedToken" : "vQsX73fEXCOIyW9Rx1PsnZc7Jv/KHqZupfqFGQLg/j4="
                    },
                    {
                        "when" : ISODate("2020-01-09T18:18:09.212Z"),
                        "hashedToken" : "CpHOou/jPY5UxA8u5WBMCUNShSuCnIu/SWC6Cm7EIrA="
                    },
                    {
                        "when" : ISODate("2020-01-16T09:06:58.775Z"),
                        "hashedToken" : "HxNgFNN3PFpRN4psOjtdGumLjkzN0QziaUzuUwPSVdI="
                    },
                    {
                        "when" : ISODate("2020-01-17T07:45:37.065Z"),
                        "hashedToken" : "EONPpmysPlWi4cABIaMdMwXdRKL5wz9gxUUrJzed+KQ="
                    },
                    {
                        "when" : ISODate("2020-01-17T07:56:12.714Z"),
                        "hashedToken" : "SSXa98VsRvdnH+iqJih2n1g9wY/8aMOGWjJmtxqD3GE="
                    },
                    {
                        "when" : ISODate("2020-01-17T07:56:13.669Z"),
                        "hashedToken" : "YuqnGWGMNSyUx08MBOaR/EPRSAiG+Q9EakFYVpeKQfY="
                    },
                    {
                        "when" : ISODate("2020-01-17T07:56:37.407Z"),
                        "hashedToken" : "snZyNek8kl8OG9viYdmEtj7gUlvxPtIGkV8HxSLPggI="
                    },
                    {
                        "when" : ISODate("2020-01-19T11:25:29.928Z"),
                        "hashedToken" : "Uow6nDrNb+cgfzV/em3WBMkVVAp6nYHlzz2HjcHe2o0="
                    },
                    {
                        "when" : ISODate("2020-01-24T15:15:22.822Z"),
                        "hashedToken" : "CfLqoiTMKZqYwr04aDbfLF/ZhLLZuSafDoIy/4tDCQs="
                    },
                    {
                        "when" : ISODate("2020-01-29T13:22:42.876Z"),
                        "hashedToken" : "/uNdB7dCuLtc/bb3wech1gpJiI/5mZdk8Fy1Mo/zJlE="
                    },
                    {
                        "when" : ISODate("2020-01-30T10:52:06.132Z"),
                        "hashedToken" : "OD/1coMmEE4HzLveUTnMeb6uYIKHufEeoVvxJzF65MQ="
                    },
                    {
                        "when" : ISODate("2020-01-31T10:12:35.549Z"),
                        "hashedToken" : "wcavYQtA1PeC/06B1Be3YciVivlgKWFGqes2AeywU7o="
                    },
                    {
                        "when" : ISODate("2020-01-31T10:14:52.860Z"),
                        "hashedToken" : "vlNjOet/3HLnupkgN7NmKg4fGScNS7S0uQkml7KGKZc="
                    },
                    {
                        "when" : ISODate("2020-02-10T06:07:21.055Z"),
                        "hashedToken" : "yJEklITR6eewbKzTAGfW0qPPvzmNQ3W4Vrgqt8YCtoY="
                    },
                    {
                        "when" : ISODate("2020-02-11T07:33:47.263Z"),
                        "hashedToken" : "T6glsugmub7ooJoNhPsQofnz2GRnJy0qPw6I5U2KXjc="
                    },
                    {
                        "when" : ISODate("2020-02-14T10:21:03.962Z"),
                        "hashedToken" : "z2NIYlKXODtv1dVNWUhxzPkAmVsmB3Ovt2+fSHpu0F0="
                    },
                    {
                        "when" : ISODate("2020-02-19T11:43:28.453Z"),
                        "hashedToken" : "/wmXYwObTfydSkF3V3g/m+d/igE5ETI55Xx+AyHwym4="
                    },
                    {
                        "when" : ISODate("2020-02-19T15:06:58.115Z"),
                        "hashedToken" : "BI+AN5ifAX3FyeaQrFIf1eQAOuzm8kM6YD3ngSWQViQ="
                    },
                    {
                        "when" : ISODate("2020-02-19T15:49:31.795Z"),
                        "hashedToken" : "2UdLBXNcoaqidGC+4PoEHtY9sS9tIhkSOzkIcnz+o1A="
                    },
                    {
                        "when" : ISODate("2020-02-25T15:39:27.778Z"),
                        "hashedToken" : "fGcEpyS6RFWckwU0eTsKTXc9VpoXhpuFTRE2wMGTNdo="
                    }
                ]
            }
        },
        "emails" : [
            {
                "address" : "Stuurgurs@yandex.ua",
                "verified" : true
            }
        ],
        "name" : "Юрий",
        "name2" : "Петров",
        "phone" : "89661910008",
        "password" : "G6YSWDRcSJpQL8cP3",
        "img" : "oWHdrqDzn64JWBQcM",
        "company" : "mf7JjKdGGnxNkXSYD",
        "status" : "active",
        "role" : "full",
        "dashboard" : true,
        "usersList" : true,
        "changecompany" : true,
        "surveys" : true,
        "actions" : true,
        "timeline" : true,
        "_lastVisit" : ISODate("2019-11-26T13:54:13.800Z"),
        "changeCompany" : true,
        "position" : "",
        "age" : "",
        "contents" : true,
        "batches" : true,
        "viewed" : [ ]
    },
    {
        "_id" : "SbrnSZxPBEjx7ZwZs",
        "createdAt" : ISODate("2019-11-27T16:43:05.521Z"),
        "services" : {
            "password" : {
                "bcrypt" : "$2a$10$ZeS3K75Tu9ASAUEPcg7PNOT4xhYlUGLvG1q9II/5GPZ9pG/0/RSvq"
            }
        },
        "emails" : [
            {
                "address" : "admin@example.com3",
                "verified" : true
            }
        ],
        "tempPassword" : "smGwtp2k4kF6aFnEe",
        "img" : "385SeRYoSAyv2FKq9",
        "name" : "Mark",
        "name2" : "Rufallo",
        "age" : "20",
        "company" : "mf7JjKdGGnxNkXSYD",
        "status" : "active",
        "dashboard" : true,
        "usersList" : true,
        "changecompany" : true,
        "surveys" : true,
        "content" : true,
        "actions" : true,
        "timeline" : true,
        "batches" : true,
        "viewed" : [ ]
    },
    {
        "_id" : "YNEFRsJ2jZHyWN2vH",
        "createdAt" : ISODate("2019-11-27T17:29:54.009Z"),
        "services" : {
            "password" : {
                "bcrypt" : "$2a$10$LcOK1ZuCTRKNVB9dEjfsZOR5QQwFGoNBt78oB38FubqZGbz7sCFKu"
            }
        },
        "emails" : [
            {
                "address" : "dfbfdbdfbfdbdfb@sdfv.ru",
                "verified" : true
            }
        ],
        "company" : "mf7JjKdGGnxNkXSYD",
        "tempPassword" : "smGwtp2k4kF6aFnEe",
        "img" : "SfLDFk3MuesJcGKnE",
        "name" : "BLOCKCHEIN TECHNOLOGIES, LLC",
        "name2" : "Gorban",
        "position" : "admin.",
        "age" : "25",
        "phone" : "8966191000845",
        "status" : "архивный",
        "dashboard" : true,
        "usersList" : true,
        "changeCompany" : true,
        "surveys" : true,
        "content" : true,
        "actions" : true,
        "timeline" : true,
        "batches" : true,
        "viewed" : [ ]
    },
    {
        "_id" : "zou8uvA28bgBmBZSC",
        "createdAt" : ISODate("2020-01-24T13:43:47.770Z"),
        "services" : {
            "password" : {
                "bcrypt" : "$2b$10$Yj67/z0dwBj8cid1IHwADuRvzIytS67ybnqdeM8b0vK2VpodIw97q"
            }
        },
        "emails" : [
            {
                "address" : "randomexample@example.com",
                "verified" : true
            }
        ],
        "company" : "mf7JjKdGGnxNkXSYD",
        "tempPassword" : "ivFQy4nmaSxzYrZzq",
        "img" : "54LrFnoTod2wfQGAa",
        "name" : "man",
        "name2" : null,
        "position" : null,
        "age" : "20",
        "phone" : "111-123-4567",
        "status" : "active",
        "dashboard" : true,
        "usersList" : true,
        "changeCompany" : true,
        "surveys" : true,
        "content" : true,
        "actions" : true,
        "timeline" : true,
        "batches" : true,
        "viewed" : [ ]
    }
]);