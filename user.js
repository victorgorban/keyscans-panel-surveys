db.users.insertOne({
    "_id": "f9unuxzcaczizwZos",
    "createdAt": ISODate("2019-11-26T12:18:59.425Z"),
    "services": {
        "password": {
            "bcrypt": "$2b$10$N2a6W5Z.EdiYqxkyyvSYPeVKujDvJk4Cnvvf6d0Y9b.7Fh1mU4BdS"
        },
        "resume": {
            "loginTokens": [
                {
                    "when": ISODate("2019-12-07T17:51:55.977Z"),
                    "hashedToken": "3L/7Fmv5d5arzIBGH8rMyKmYLwW3zbbXE3HOGysn6dw="
                },
            ]
        }
    },
    "emails": [
        {
            "address": "Stuurgurs@yandex.ua",
            "verified": true
        }
    ],
    "name": "Юрий",
    "name2": "Петров",
    "phone": "89661910008",
    "password": "G6YSWDRcSJpQL8cP3",
    "img": "oWHdrqDzn64JWBQcM",
    "company": "mf7JjKdGGnxNkXSYD",
    "status": "active",
    "role": "full",
    "dashboard": true,
    "usersList": true,
    "changecompany": true,
    "surveys": true,
    "actions": true,
    "timeline": true,
    "_lastVisit": ISODate("2019-11-26T13:54:13.800Z"),
    "changeCompany": true,
    "position": "",
    "age": "",
    "contents": true,
    "batches": true,
    "viewed": []
});