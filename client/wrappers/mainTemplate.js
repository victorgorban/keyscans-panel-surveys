Template.maintemplate.onCreated(function () {
    window.scrollTo(0,0);
    var data2, instance;
    this.load = new ReactiveVar(false);
    data2 = this.subscribe('company.data');
    instance = this;
    return this.autorun(() => {
        if (data2.ready()) {
            return instance.load.set(true);
        }
    });
});

Template.maintemplate.helpers({
    loaded: function () {
        return Template.instance().load.get();
    }
});
