import {showSuccess, showError} from "../../notifications";

function getNumber(number) {
    if (!number)
        return 0;
    return +number;
}

function getText(node, first = false) {
    let and = ' И ';
    if (first) {
        and = ''
    }
    if (node.category)
        return ` ТО ${node.category}`;
    else
        return `${and}${node.attribute}${node.predicateName}${node.pivot}`
}


function nodeToString(node, str, first = false) {
    if (!str) {
        str = 'ЕСЛИ ';
    }

    str += getText(node, first);
    if (node.category) {
        strings.push(str);
        return;
    }

    if (node.match) {
        let newStr = str + ' -ДА';
        nodeToString(node.match, newStr);
    }
    if (node.notMatch) {
        let newStr = str + ' -НЕТ';
        nodeToString(node.notMatch, newStr);
    }
}


Template.viewTestResult.onCreated(function () {
    window.scrollTo(0, 0);
    Session.set('thisSurveyId', FlowRouter.getParam('id'));
    this.subscribe('actions.type', 'survey');
    return this.subscribe('test.one', FlowRouter.getParam('id'));
});

Template.viewTestResult.helpers({
    date(date) {
        return moment(date).format('DD-MM-YYYY');
    },
    getNumber(number) {
        return getNumber(number);
    },
    test() {
        return Surveys.findOne(FlowRouter.getParam('id'));
    },
    isTest(test) {
        if (test)
            return test.test;
    },
    actions() {
        return Actions.find();
    },
    testChecked(test) {
        return test.isTest ? 'checked' : '';
    },

});

Template.viewTestResult.events({
    'change .test-field'(e) {
        let item = $(e.currentTarget);
        let field = item.data('name');
        let data = item.val();

        if (field == 'title' && !data) {
            showError('Необходимо ввести название опроса');
            return;
        }

        if (['radio', 'checkbox'].includes(item.attr('type'))) {
            data = item.is(":checked");
        }


        Meteor.call('updateSurveyData', Surveys.findOne()._id, field, data, (err, res) => {
            if (err) {
                showError(err.message);
                return;
            }
            if (res) {
                showSuccess('Изменения сохранены');
                return;
            }
        });
    },
    'click #remove'(e) {
        let id = Surveys.findOne()._id;
        Meteor.call('removeSurvey', id, (err, res) => {
            if (err) {
                showError(err.message);
                return;
            }
            if (res) {
                showSuccess('Опрос удален');
                FlowRouter.go('testsList');
                return;
            }
        })
    }

})


