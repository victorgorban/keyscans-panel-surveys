import {showSuccess, showError} from "../../notifications";

function getNumber(number) {
    if (!number)
        return 0;
    return +number;
}

Template.viewSurveyResult.onCreated(function () {
    window.scrollTo(0, 0);
    Session.set('thisSurveyId', FlowRouter.getParam('id'));
    this.subscribe('actions.type', 'survey');
    return this.subscribe('survey.one', FlowRouter.getParam('id'));
});

Template.viewSurveyResult.helpers({
    date(date) {
        return moment(date).format('DD-MM-YYYY');
    },
    getNumber(number) {
        return getNumber(number);
    },
    survey() {
        return Surveys.findOne(FlowRouter.getParam('id'));
    },
    isTest(survey) {
        if (survey)
            return survey.test;
    },
    actions() {
        return Actions.find();
    },
    testChecked(survey) {
        return survey.isTest ? 'checked' : '';
    },

});

Template.viewSurveyResult.events({
    'change .survey-field'(e) {
        let item = $(e.currentTarget);
        let field = item.data('name');
        let data = item.val();

        if (field == 'title' && !data) {
            showError('Необходимо ввести название опроса');
            return;
        }

        if (['radio', 'checkbox'].includes(item.attr('type'))) {
            data = item.is(":checked");
        }


        Meteor.call('updateSurveyData', Surveys.findOne()._id, field, data, (err, res) => {
            if (err) {
                showError(err.message);
                return;
            }
            if (res) {
                showSuccess('Изменения сохранены');
                return;
            }
        });
    },
    'click #remove'(e) {
        let id = Surveys.findOne()._id;
        Meteor.call('removeSurvey', id, (err, res) => {
            if (err) {
                showError(err.message);
                return;
            }
            if (res) {
                showSuccess('Опрос удален');
                FlowRouter.go('surveysList');
                return;
            }
        })
    }

})


