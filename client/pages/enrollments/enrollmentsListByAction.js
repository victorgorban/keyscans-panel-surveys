Template.enrollmentsListByAction.onCreated(function () {
    window.scrollTo(0,0);
    this.subscribe('company.enrollmentsListByAction', Meteor.user().company, FlowRouter.getParam('id'));
    return this.loaded = new ReactiveVar(true);
});

//  console.log(Actions.find({},{sort: {_createdAt: -1}}).fetch());
Template.enrollmentsListByAction.helpers({
    loadervar: function () {
        return Template.instance().loaded.get();
    },
    enrollments: function () {
        // console.log('loadedvar: '+Template.instance().loaded.get());
        // console.log(Enrollments.find().count());
        // return Enrollments.find().fetch(); // реактивная подписка не сразу обновляет/удаляет сопутствующие. Поэтому нужен фильтр.

        return Enrollments.find({action: FlowRouter.getParam('id')}).fetch();
    },
    userName: function (id) {
        let user = Meteor.users.findOne(id);
        if (user){
            return user.name;
        }
    },
    actionName: function (id) {
        let action = Actions.findOne(id);
        if (action) return action.name;
    },
    created: function (date) {
        return moment(date).format('DD-MM-YYYY hh:mm:ss');
    },

});

//Template.enrollmentsList.events
