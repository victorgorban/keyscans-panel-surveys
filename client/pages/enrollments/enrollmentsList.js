Template.enrollmentsList.onCreated(function () {
    window.scrollTo(0,0);
    this.filter = new ReactiveDict();
    Tracker.autorun(() => {
        let filter = this.filter;

        this.subscribe('company.enrollments', {
            user: filter.get('user'),
            action: filter.get('action')
        });
    });

    return this.loaded = new ReactiveVar(true);
});

Template.enrollmentsList.helpers({
    pageTitle: function () {
        let filter = Template.instance().filter;
        let user = filter.get('user'), action = filter.get('action');
        if (!user && !action) return 'Все зачисления';
        if (user) return 'Зачисления по пользователю ' + Meteor.users.findOne(user).name;
        if (action) return 'Зачисления по акции ' + Actions.findOne(action).name;
    },
    enrollments: function () {
        // return Enrollments.find().fetch(); // реактивная подписка не сразу обновляет/удаляет сопутствующие. Поэтому нужен фильтр.
        let filter = Template.instance().filter;
        // console.log('in enrollments helper');
        // console.log(filter);
        let opts = {user: filter.get('user'), action: filter.get('action')};
        if (!opts.user) {
            delete opts.user;
        }
        if (!opts.action) {
            delete opts.action;
        }
        return Enrollments.find(opts).fetch();
    },
    userName: function (id) {
        return Meteor.users.findOne(id).name || '';
    },
    actionName: function (id) {
        let action = Actions.findOne(id);
        if (action) return action.name;
    },
    created: function (date) {
        return moment(date).format('DD-MM-YYYY hh:mm:ss');
    },

    
});

Template.enrollmentsList.events({
    'click .enrollments-user': function (e, t) {
        e.preventDefault();
        let id = e.currentTarget.dataset.id;
        t.filter.set('user', id);
        t.filter.set('action', undefined); // ну вот так и хочется полноценный фильтр сделать
    },
    'click .enrollments-action': function (e, t) {
        e.preventDefault();
        let id = e.currentTarget.dataset.id;
        t.filter.set('action', id);
        t.filter.set('user', undefined); // ну вот так и хочется полноценный фильтр сделать
    }
});
