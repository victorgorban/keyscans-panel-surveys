import {showError, showSuccess} from "../../notifications";

Template.addVideo.onCreated(function () {
    this.subscribe('actions.type', 'video');
    this.subscribe('company.surveysTitles');
    return this.loaded = new ReactiveVar(true);
});

Template.addVideo.events({
    'change #videoFile': function (e, t) {
        var f, fileExt, reader, sender, uploader, validExts;
        sender = e.currentTarget;
        validExts = ['.mp4', '.webm', '.mov', '.avi'];
        fileExt = sender.value;
        fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
        if (!validExts.includes(fileExt)) {
            sender.value = '';
            showError('Неверное расширение файла');
        }
        if (e.currentTarget.files && e.currentTarget.files[0]) {
            reader = new FileReader();
        }
        f = e.currentTarget.files[0];
        reader.onload = function (e) {
        };
        reader.readAsDataURL(f);
        uploader = Videos.insert({
            file: e.currentTarget.files[0],
            streams: 'dynamic',
            chunkSize: 'dynamic',
            meta: {
                company: Meteor.user().company,
                action: $('#action').val(),
                name: $('#name').val(),
                _createdAt: new Date()
            }
        }, false);
        uploader.on('uploaded', function (error, fileObj) {
            if (fileObj) {
                console.log(fileObj._id);
                $('#fileId').val(fileObj._id);
                showSuccess('Видео загружено');
            }
        });
        uploader.on('error', function (error, fileObj) {
            showError(error.error);
        });

        let oldId = $('#fileId').val();
        if (oldId) {
            Videos.remove({_id: $('#fileId').val()});
        }

        return uploader.start();
    },
    'click #create': function (e, t) {
        e.preventDefault();
        var object;
        object = {};
        object.name = $('#name').val().trim();
        if (!object.name) {
            showError('Сначала введите название!');
            return;
        }

        object.action = $('#action').val();
        object.survey = $('#survey').val();
        object.expire = $('#expire').val() || new Date();
        if ($('#fileId').val()) {
            return Meteor.call('addVideo', $('#fileId').val(), object, function (err, res) {
                if (err) {
                    showError(err.message);
                    console.log(err);
                }
                if (res) {
                    showSuccess('Создано');
                    FlowRouter.go('videosList');
                }
            });
        } else {
            showError('Сначала выберите файл!');
        }
    }
});

Template.addVideo.helpers({
    loadervar: function () {
        return Template.instance().loaded.get();
    },
    actions: function () {
        return Actions.find().fetch();
    },
    surveys: function () {
        return Surveys.find().fetch();
    },
});
