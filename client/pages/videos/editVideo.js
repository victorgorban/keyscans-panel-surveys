import {showError, showSuccess} from "../../notifications";

Template.editVideo.onCreated(function() {
  this.subscribe('actions.type', 'video');
  this.subscribe('company.surveysTitles');
  this.subscribe('video.one', FlowRouter.getParam('id'));
  window.scrollTo(0, 0);
  return this.loaded = new ReactiveVar(true);
});

Template.editVideo.events({
  'change .video': function(e, t) {
    if (!e.currentTarget.value){
        showError('Введите название видео!');
        return;
    }

    return Meteor.call('updateVideoData', FlowRouter.getParam('id'), e.currentTarget.dataset.field, e.currentTarget.value, function(err, res) {
      if (err) {
        showError(err.message); console.log(err); console.log(err);
      }
      if (res) {
        showSuccess('Изменения сохранены');
      }
    });
  },
  'click #activate': function(e, t) {
    if ($('#name').val()) {
      return Meteor.call('setActiveVideo', FlowRouter.getParam('id'), function(err, res) {
        if (err) {
          showError(err.message); console.log(err);
        }
        if (res) {
          showSuccess('Пост с видео создан');
          return FlowRouter.go('contentsList');
        }
      });
    }
  },
  'click #remove': function(e, t) {
    return Meteor.call('removeVideo', FlowRouter.getParam('id'), function(err, res) {
      if (err) {
        showError(err.message); console.log(err);
      }
      if (res) {
        showSuccess('Удалено');
        return FlowRouter.go('videosList');
      }
    });
  }
});

Template.editVideo.helpers({
  loadervar() {
    return Template.instance().loaded.get();
  },
  video() {
    return Videos.findOne(FlowRouter.getParam('id'));
  },
  actions() {
    return Actions.find().fetch();
  },
  surveys() {
    return Surveys.find().fetch();
  },
  date(date) {
    return moment(date).format('DD-MM-YYYY HH:mm');
  },
});
