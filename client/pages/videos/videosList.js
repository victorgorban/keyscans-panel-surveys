Template.videosList.onCreated(function () {
    window.scrollTo(0,0);
    var instance;
    instance = this;
    instance.subscribe('company.videos');
    return this.loaded = new ReactiveVar(true);
});

Template.videosList.onRendered(function () {
    var instance;
    instance = this;
    return instance.loaded.set(false);
});

//  Meteor.setTimeout ->
//#    $('#datatable').dataTable({"pageLength": 50, "order": []})
//    instance.loaded.set false
//  , 800
Template.videosList.events;

Template.videosList.helpers({
    loadervar: function () {
        return Template.instance().loaded.get();
    },
    videos: function () {
        return Videos.find({}, {sort: {_createdAt: -1}}).fetch();
    },
    created: function (date) {
        return moment(date).format('DD-MM-YYYY');
    },
    username: function (id) {
        if (Meteor.users.findOne(id))
            return Meteor.users.findOne(id).name;
    },
    username2: function (id) {
        if (Meteor.users.findOne(id))
            return Meteor.users.findOne(id).name2;
    },
    isAction(id){
        return !!Actions.findOne(id);
    },
    actionName: function (id) {
        if (Actions.findOne(id))
            return Actions.findOne(id).name;
    },
    surveyTitle(id) {
        let survey = Surveys.findOne(id);
        if (survey) {
            return survey.title;
        } /*else return 'Не указан'*/
    },
    isSurvey(id) {
        let survey = Surveys.findOne(id);
        return !!survey;
    },
    def: function (size) {
        return (size / (1024 * 1024)).toFixed(1) + 'MB';
    },
    russianActive(val) {
        if (val)
            return 'Да';
        else return 'Нет';
    }

});
