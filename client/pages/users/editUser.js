import {showError, showSuccess} from "../../notifications";

Template.editUser.onCreated(function () {
    window.scrollTo(0,0);
    return this.subscribe('user.one', FlowRouter.getParam('id'));
});

Template.editUser.onRendered(() => {
    // bootstrap-validator
    $('#form').validator({
        delay: 0,
        disable: false
    });
});


Template.editUser.helpers({
    isDisabled: function () {
        if (Meteor.user()._id === FlowRouter.getParam('id')) {
            return 'disabled';
        } else {
            return '';
        }
    },
    formatPhone(phone) {
        // убираем () и - из строки
        phone = phone.split(/[-() ]/).join('');
        let
            first = phone.substr(0, 3),
            second = phone.substr(3, 3),
            third = phone.substr(6, 4);
        return `${first}-${second}-${third}`;
    },
    created: function (date) {
        return moment(date).format('DD-MM-YYYY hh:mm');
    },
    lang: function () {
        return TAPi18n.getLanguage();
    },
    user: function () {
        return Meteor.users.findOne(FlowRouter.getParam('id'));
    },
    linkx: function (id) {
        var url;
        if (Images.findOne(id)) {
            url = Images.findOne(id).link('thumbnail');
            return url;
        } else {
            return 'not found in Images id: ' + id;
        }
    }
});

Template.editUser.events({
    'change #mainImg': function (e, t) {
        var f, reader, uploader;
        if (e.currentTarget.files && e.currentTarget.files[0]) {
            reader = new FileReader();
            f = e.currentTarget.files[0];
            reader.onload = function (e) {
                return $('#mainim').attr("src", e.currentTarget.result); // этот e - это к reader.onload
            };
            reader.readAsDataURL(f);
            uploader = Images.insert({
                file: e.currentTarget.files[0],
                streams: 'dynamic',
                chunkSize: 'dynamic'
            }, false);
            uploader.on('uploaded', function (error, fileObj) {
                if (fileObj) {
                    //          Meteor.call 'removeOldImage', FlowRouter.getParam('id') (err, res) ->
                    return Meteor.call('changeUserData', 'img', FlowRouter.getParam('id'), fileObj._id, function (err, res) {
                        if (err) {
                            showError(err);
                        }
                        if (res) {
                            showSuccess('Изображение загружено');
                        }

                    });
                }
            });
            uploader.on('error', function (error, fileObj) {
                showError(error);
            });
            return uploader.start();
        }
    },
    'change .form-control': function (e, t) {
        let isValid = $(e.currentTarget).is(':valid'); // надеюсь, bootstrap-validator проставляет этот псевдокласс правильно
        console.log(isValid);
        if (!isValid)
            return;

        return Meteor.call('changeUserData', e.currentTarget.id, FlowRouter.getParam('id'), e.currentTarget.value, function (err, res) {
            var userLang;
            if (err) {
                showError(err.message); console.log(err);
            }
            if (res) {
                if (e.currentTarget.id === 'lang') {
                    userLang = e.currentTarget.value;
                    TAPi18n.setLanguage(userLang);
                    Session.set('lang', userLang);
                }
                showSuccess('Сохранено');
            }
        });
    },
    'click #remove': function (e, t) {
        var userid;
        userid = FlowRouter.getParam('id');
        return Meteor.call('removeUser', userid, function (err, res) {
            if (err)
                showError(err.message); console.log(err);

            if (res) {
                showSuccess();
                return FlowRouter.go('usersList');
            }
        });
    },
    'click #resetpass': function (e, t) {
        if ($('#email').val()) {
            return Meteor.call('sendResetPassword', $('#email').val(), function (err, res) {
                if (err)
                    showError(err.message); console.log(err);

                if (res) {
                    showSuccess(`Ссылка со сбросом пароля отправлена на email ${$('#email').val()}`);
                }
            });
        }
    },
    'click #resetbracelet': function (e, t) {
        return Meteor.call('resetbracelet', FlowRouter.getParam('id'), function (err, res) {
            if (err)
                showError(err.message); console.log(err);

            if (res) {
                showSuccess();
            }
        });
    }
});

