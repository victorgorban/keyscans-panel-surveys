import {showError, showSuccess} from "../../notifications";

function submitForm(e){
    e.preventDefault();

    object = {};
    object.img = $('#fileId').val();
    object.name = $('#name').val();
    object.name2 = $('#name2').val();
    object.age = $('#age').val();
    object.phone = $('#phone').val();
    object.position = $('#position').val();
    object.email = $('#email').val();
    object.tempPassword = $('#tempPassword').val();
    object.access = $('#access').val();
    if (object.email && object.tempPassword) {
        return Meteor.call('createNewUser', object, function (err, res) {
            if (err) {
                showError(err.message); console.log(err);
                return;
            }
            if (res) {
                showSuccess('Пользователь создан');
                return FlowRouter.go('usersList');
            }

        });
    } else {
        showError('Введите email и пароль');
    }
}

Template.addUser.onCreated(function () {
    window.scrollTo(0,0);
    var instance;
    return instance = this;
});

Template.addUser.onRendered(function() {
    let instance = this;
    // bootstrap-validator
    $('#form').validator({
        delay: 0,
        disable: false
    }).on('submit', function (e) {
        if (e.isDefaultPrevented()) {
            // showError('form invalid');
        } else {
            submitForm(e);
        }
    })
});

//  this.fetchComp = new ReactiveVar []
Template.addUser.events({
    /*'click #create': function (e, t) {
        var object, ok;
        e.preventDefault();

        object = {};
        object.img = $('#fileId').val();
        object.name = $('#name').val();
        object.name2 = $('#name2').val();
        object.age = $('#age').val();
        object.phone = $('#phone').val();
        object.position = $('#position').val();
        object.email = $('#email').val();
        object.tempPassword = $('#tempPassword').val();
        object.access = $('#access').val();
        if (object.email && object.tempPassword) {
            return Meteor.call('createNewUser', object, function (err, res) {
                if (err) {
                    showError(err.error);
                    return;
                }
                if (res) {
                    showSuccess('Пользователь создан');
                    return FlowRouter.go('usersList');
                }

            });
        } else {
            showError('Введите email и пароль');
        }
    },*/

    'change #mainImg': function (e, t) {
        var f, reader, uploader;
        if (e.currentTarget.files && e.currentTarget.files[0]) {
            reader = new FileReader();
            f = e.currentTarget.files[0];
            reader.onload = function (e) {
                return $('#img').attr("src", e.currentTarget.result);
            };
            reader.readAsDataURL(f);
            uploader = Images.insert({
                file: e.currentTarget.files[0],
                streams: 'dynamic',
                chunkSize: 'dynamic'
            }, false);
            uploader.on('uploaded', function (error, fileObj) {
                if (fileObj) {
                    //          Meteor.call 'removeOldImage', FlowRouter.getParam('id') (err, res) ->
                    $('#fileId').val(fileObj._id);
                    showSuccess('Изображение загружено');
                }
            });
            uploader.on('error', function (error, fileObj) {
                showError(error);
            });
            return uploader.start();
        }
    }
});

Template.addUser.helpers({
    loadervar: function () {
        return Template.instance().loaded.get();
    },
    pass: function () {
        return Random.id();
    }
});

//  companys: () ->
//    return Template.instance().fetchComp.get()
