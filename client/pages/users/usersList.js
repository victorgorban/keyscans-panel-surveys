import {showError, showSuccess} from "../../notifications";

Template.usersList.onCreated(function () {
    window.scrollTo(0,0);
    this.subscribe('company.users', Meteor.user().company);
    this.subscribe('invited');
    return this.loaded = new ReactiveVar(true);
});

Template.usersList.helpers({
    loadervar: function () {
        return Template.instance().loaded.get();
    },
    company: function () {
        return Companys.findOne();
    },
    users: function () {
        return Meteor.users.find().fetch();
    },
    invited: function () {
        var comp;
        comp = Companys.findOne();
        return Invited.find({
            company: comp._id
        }).fetch();
    },
    isAdmin: function () {
        if (Companys.findOne().users.indexOf(Meteor.userId()) !== -1) {
            return 1;
        } else {
            return 0;
        }
    },
    created: function (date) {
        return moment(date).format('DD-MM-YYYY');
    },
    linkx: function (id) {
        var url;
        if (Images.findOne(id)) {
            url = Images.findOne(id).link('thumbnail');
            return url;
        }
    }
});

Template.usersList.events({
    'click .remove': function (e, t) {
        return Meteor.call('removeUser', e.currentTarget.id, function (err, res) {
            if (err)
                showError(err.message); console.log(err);

            if (res) {
                showSuccess('Пользователь удален');
            }
        });
    }
});
