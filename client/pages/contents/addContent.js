import {showError} from "../../notifications";

Template.addContent.onCreated(function () {
    window.scrollTo(0,0);

    Meteor.call('createNewContent', (err, res) => {
        if (err) {
            showError(err.message); console.log(err);
            console.log(err);
            return;
        }
        if (res) {
            FlowRouter.go('editContent', {id: res});
        }
    })
});

