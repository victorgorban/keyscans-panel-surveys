import {showError, showSuccess} from "../../notifications";

function getImagesIdsFromNode(node) {
    node = $(node);
    let idsNodes = node.find('.file-id');
    let ids = idsNodes.map((index, element) => $(element).text()).toArray();

    return ids;
}

function findImagesAndUpdateOnServer(container) {
    // Обновляем список ответов.
    let images = getImagesIdsFromNode(container);
    Meteor.call('updateContentData', FlowRouter.getParam('id'), 'images', images, (err, res) => {
        if (err) {
            showError(err.message);
            return;
        }
        if (res) {
            showSuccess('Изменения сохранены');
        }
    })

}

function initImagesSortable() {
    let nodes = $('.images-sortable');
    for (let i = 0; i < nodes.length; i++) {
        let elem = nodes[i];
        new Sortable(elem, {
            animation: 150,
            dataIdAttr: 'data-sort-id',
            scroll: true,
            scrollSensitivity: 60,
            scrollSpeed: 30,
            ghostClass: 'opacity-half',
            filter: '.not-sortable',
            /*events*/
            onMove: function (evt) {
                if ($(evt.related).hasClass('not-sortable')) {
                    return false; // prevent dropping on .not-sortable
                }
            },
            onEnd(e) {
                findImagesAndUpdateOnServer($(e.item).closest('.images-sortable'));
            }

        });
    }
}


Template.editContent.onCreated(function () {
    window.scrollTo(0, 0);
    this.subscribe('company.surveysTitles');
    return this.subscribe('content.one', FlowRouter.getParam('id'));
});
Template.editContent.onRendered(function () {
    initImagesSortable();
});

Template.editContent.helpers({
    content() {
        return Contents.findOne();
    },
    surveys() {
        return Surveys.find();
    },
    linkx: function (id) {

        let image = Images.findOne(id);
        if (image) return image.link('thumbnail');
    },
});

Template.editContent.events({
    'change .content-image': function (e, t) {
        let container = $(e.currentTarget).closest('.image.view');
        let image = container.find('.image-source');
        let fileId = container.find('.file-id');
        let oldId = fileId.text();

        var f, reader, uploader;
        if (e.currentTarget.files && e.currentTarget.files[0]) {
            reader = new FileReader();
            f = e.currentTarget.files[0];
            reader.onload = function (e) {
                console.log(e.currentTarget);
                // return image.attr("src", e.currentTarget.result); // этот e - это к reader.onload
            };
            reader.readAsDataURL(f);
            uploader = Images.insert({
                file: e.currentTarget.files[0],
                streams: 'dynamic',
                chunkSize: 'dynamic'
            }, false);


            if (oldId) {
                Images.remove({_id: oldId});
                console.log('Изображение должно было удалиться');
            }
            uploader.on('uploaded', function (error, fileObj) {
                if (fileObj) {
                    // собираем новый images из старых + заменяем старый id на новый, затем обновляем ContentData
                    let images = Contents.findOne().images;
                    fileId.text(fileObj._id);
                    let imIndex = images.findIndex(im => im == oldId);
                    images.splice(imIndex, 1, fileObj._id);

                    return Meteor.call('updateContentData', FlowRouter.getParam('id'), 'images', images, function (err, res) {
                        if (err) {
                            showError(err.message);
                            console.log(err);
                        }
                        if (res) {
                            showSuccess('Изображение обновлено');
                        }
                    });
                }
            });
            uploader.on('error', function (error, fileObj) {
                showError(error);
            });
            return uploader.start();
        }
    },
    'change .add-image': function (e, t) {

        var f, reader, uploader;
        if (e.currentTarget.files && e.currentTarget.files[0]) {
            reader = new FileReader();
            f = e.currentTarget.files[0];
            reader.onload = function (e) {
                console.log(e.currentTarget);
                // return image.attr("src", e.currentTarget.result); // этот e - это к reader.onload
            };
            reader.readAsDataURL(f);
            uploader = Images.insert({
                file: e.currentTarget.files[0],
                streams: 'dynamic',
                chunkSize: 'dynamic'
            }, false);
            uploader.on('uploaded', function (error, fileObj) {
                if (fileObj) {
                    // собираем новый images из старых + добавляем id, затем обновляем ContentData
                    let images = Contents.findOne().images;
                    images.push(fileObj._id);

                    return Meteor.call('updateContentData', FlowRouter.getParam('id'), 'images', images, function (err, res) {
                        if (err) {
                            showError(err.message);
                            console.log(err);
                        }
                        if (res) {
                            showSuccess('Изображение добавлено');
                        }
                    });
                }
            });
            uploader.on('error', function (error, fileObj) {
                showError(error);
            });
            return uploader.start();
        }
    },
    'click .remove-image': function (e, t) {
        let container = $(e.currentTarget).closest('.image.view');
        let image = container.find('.image-source');
        let fileId = container.find('.file-id');
        let oldId = fileId.text();


        // собираем новый images из старых + заменяем старый id на новый, затем обновляем ContentData
        let images = Contents.findOne().images;
        let imIndex = images.findIndex(im => im == oldId);
        images.splice(imIndex, 1);

        return Meteor.call('updateContentData', FlowRouter.getParam('id'), 'images', images, function (err, res) {
            if (err) {
                showError(err.message);
                console.log(err);
            }
            if (res) {
                showSuccess('Изображение удалено');
            }

        });
    },

    'change .form-control': function (e, t) {
        let id = FlowRouter.getParam('id');
        let field = e.currentTarget.dataset.field;
        let value = e.currentTarget.value;
        // зачем три строчки? Для удобного тестирования. И понятных параметров метода
        return Meteor.call('updateContentData', id, field, value, function (err, res) {
            if (err) {
                showError(err.message);
                console.log(err);
            }
            if (res) {
                showSuccess('Данные обновлены');
            }
        });
    },
    'click #remove': function (e, t) {
        let id = FlowRouter.getParam('id');
        return Meteor.call('removeContent', id, function (err, res) {
            if (err) {
                showError(err.message);
                console.log(err);
            }
            if (res) {
                showSuccess('Удалено');
                FlowRouter.go('contentsList');
            }
        });
    },
});



