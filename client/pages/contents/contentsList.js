Template.contentsList.onCreated(function () {
    window.scrollTo(0,0);
    this.subscribe('company.contents');
    return this.loaded = new ReactiveVar(true);
});

Template.contentsList.helpers({
    loadervar: function () {
        return Template.instance().loaded.get();
    },
    contents: function () {
        return Contents.find({},{sort: {_createdAt: -1}}).fetch();
    },
    surveyTitle(id) {
        let survey = Surveys.findOne(id);
        if (survey) {
            return survey.title;
        }
    },
    isSurvey(id) {
        let survey = Surveys.findOne(id);
        return !!survey;
    },
    created: function (date) {
        return moment(date).format('DD-MM-YYYY');
    },
    linkx: function (id) {
        var url;
        if (Images.findOne(id)) {
            url = Images.findOne(id).link('thumbnail');
            return url;
        }
    }
});

Template.contentsList.events({
    'click .remove': function (e, t) {
        return Meteor.call('removeContent', e.currentTarget.id, function (err, res) {

        });
    }
});
