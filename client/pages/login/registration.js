import {showError, showSuccess} from "../../notifications";

Template.registration.onCreated(function () {
    window.scrollTo(0,0);
    return this.accept = new ReactiveVar(false);
});

Template.registration.helpers({
    valemail: function () {
        if (FlowRouter.getQueryParam('email')) {
            return FlowRouter.getQueryParam('email');
        }
    },
    accept: function () {
        return Template.instance().accept.get();
    }
});

Template.registration.events({
    'click #accept': function (e, t) {
        e.preventDefault();
        t.accept.set(true);
        return window.scrollTo(0, 0);
    },
    'submit #reg': function (e, t) {
        var createUser;
        e.preventDefault();
        if ($('#email').val() === '' || $('#password').val() === '' || $('#password2').val() === '') {
            return new PNotify({
                title: 'Ошибка',
                text: 'Заполните все обязательные поля',
                type: 'error',
                styling: 'bootstrap3'
            });
        } else {
            if ($('#password').val() === $('#password2').val()) {
                createUser = {};
                createUser.tempPassword = $('#password').val();
                createUser.email = $('#email').val();
                Meteor.call('checkAndRegisterUser', createUser, function (err, res) {
                    if (!err) {
                        Meteor.call('sendVerificationEmail', function (err, res) {
                            if (!err) {
                                FlowRouter.go('login');
                                showSuccess('Регистрация прошла успешно', 'Регистрация');
                            }
                        });
                    }
                    if (err) {
                        showError(err.message); console.log(err);
                    }
                });
                return Meteor.loginWithPassword($('#email').val(), $('#password').val(), function (err, res) {
                    if (err) {
                        showError(err.message); console.log(err);
                    }
                });
            } else {
                showError('Пароли не совпадают');
            }
        }
    }
});
