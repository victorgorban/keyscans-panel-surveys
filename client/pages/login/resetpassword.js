import {showError, showSuccess} from "../../notifications";

Template.resetpas.events({
    'submit #entereset': function (e, t) {
        var newPassword, token;
        e.preventDefault();
        if ($('#pass').val() && FlowRouter.getParam('token')) {
            newPassword = $('#pass').val();
            token = FlowRouter.getParam('token');
            return Accounts.resetPassword(token, newPassword, function (err) {
                if (err) {
                    showError(err);
                }
                if (!err) {
                    showSuccess('Пароль обновлен');
                    return FlowRouter.go('dashboard');
                }
            });
        } else {
            showError('Введите пароль');
        }
    }
});
