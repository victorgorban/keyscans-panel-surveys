import {showError, showSuccess} from "../../notifications";

Template.loginForm.onCreated(function () {
    window.scrollTo(0,0);
    var instance;
    instance = this;
    this.statevar = new ReactiveVar('login');
    return this.autorun(() => {
        if (Meteor.user() && Meteor.user().emails[0].verified === false) {
            return instance.statevar.set('verify');
        }
    });
});

Template.loginForm.helpers({
    state: function () {
        return Template.instance().statevar.get();
    }
});

Template.loginForm.events({
    'click #forget': function (e, t) {
        e.preventDefault();
        return t.statevar.set('forget');
    },
    'click #login': function (e, t) {
        e.preventDefault();
        return t.statevar.set('login');
    },
    'click #regist': function (e, t) {
        e.preventDefault();
        return t.statevar.set('regist');
    },
    'submit #enter': function (e, t) {
        e.preventDefault();
        return Meteor.loginWithPassword($('#loginlogin').val(), $('#loginpassword').val(), function (error) {
            if (!error) {
                FlowRouter.go('dashboard');
            }
            if (error) {
                showError(error);
                console.log(error);
            }
        });
    },
    //        console.log('login is '+$('#loginlogin').val() + ' password is '+$('#loginpassword').val())
    'submit #resent': function (e, t) {
        e.preventDefault();
        Meteor.call('sendVerificationEmail', (err, res) => {
            if (err) {
                showError(err.message); console.log(err);
            }
            if (res) {
                showSuccess('Письмо с подтверждением email отправлено');
            }
        });
    },
    'submit #enterforget': function (e, t) {
        e.preventDefault();
        if ($('#emailforget').val()) {
            return Meteor.call('sendResetPassword', $('#emailforget').val(), function (err, res) {
                if (err) {
                    showError(err.message); console.log(err);
                }
                if (res) {
                    showSuccess('Письмо с подтверждением email отправлено');
                }
            });
        } else {
            showError('Введите пароль');
        }
    }
});
