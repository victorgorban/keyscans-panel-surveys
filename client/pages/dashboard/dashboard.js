Template.dashboard.onCreated(function () {
    window.scrollTo(0,0);

    var instance = this;
    this.datas = new ReactiveVar([]);
    this.start = new ReactiveVar(moment().subtract(1, 'month'));
    this.end = new ReactiveVar(moment());
    // this.subscribe('tracks.company2');
    this.theme = {
        color: [
            '#26B99A', '#34495E', '#BDC3C7', '#3498DB',
            '#9B59B6', '#8abb6f', '#759c6a', '#bfd3b7',
        ],

        title: {
            itemGap: 8,
            textStyle: {
                fontWeight: 'normal',
                color: '#408829',
            },
        },

        dataRange: {
            color: ['#1f610a', '#97b58d'],
        },

        toolbox: {
            color: ['#408829', '#408829', '#408829', '#408829'],
        },

        tooltip: {
            backgroundColor: 'rgba(0,0,0,0.5)',
            axisPointer: {
                type: 'line',
                lineStyle: {
                    color: '#408829',
                    type: 'dashed',
                },
                crossStyle: {
                    color: '#408829',
                },
                shadowStyle: {
                    color: 'rgba(200,200,200,0.3)',
                },
            },
        },

        dataZoom: {
            dataBackgroundColor: '#eee',
            fillerColor: 'rgba(64,136,41,0.2)',
            handleColor: '#408829',
        },
        grid: {
            borderWidth: 0,
        },

        categoryAxis: {
            axisLine: {
                lineStyle: {
                    color: '#408829',
                },
            },
            splitLine: {
                lineStyle: {
                    color: ['#eee'],
                },
            },
        },

        valueAxis: {
            axisLine: {
                lineStyle: {
                    color: '#408829',
                },
            },
            splitArea: {
                show: true,
                areaStyle: {
                    color: ['rgba(250,250,250,0.1)', 'rgba(200,200,200,0.1)'],
                },
            },
            splitLine: {
                lineStyle: {
                    color: ['#eee'],
                },
            },
        },
        timeline: {
            lineStyle: {
                color: '#408829',
            },
            controlStyle: {
                normal: {color: '#408829'},
                emphasis: {color: '#408829'},
            },
        },

        k: {
            itemStyle: {
                normal: {
                    color: '#68a54a',
                    color0: '#a9cba2',
                    lineStyle: {
                        width: 1,
                        color: '#408829',
                        color0: '#86b379',
                    },
                },
            },
        },
        map: {
            itemStyle: {
                normal: {
                    areaStyle: {
                        color: '#ddd',
                    },
                    label: {
                        textStyle: {
                            color: '#c12e34',
                        },
                    },
                },
                emphasis: {
                    areaStyle: {
                        color: '#99d2dd',
                    },
                    label: {
                        textStyle: {
                            color: '#c12e34',
                        },
                    },
                },
            },
        },
        force: {
            itemStyle: {
                normal: {
                    linkStyle: {
                        strokeColor: '#408829',
                    },
                },
            },
        },
        chord: {
            padding: 4,
            itemStyle: {
                normal: {
                    lineStyle: {
                        width: 1,
                        color: 'rgba(128, 128, 128, 0.5)',
                    },
                    chordStyle: {
                        lineStyle: {
                            width: 1,
                            color: 'rgba(128, 128, 128, 0.5)',
                        },
                    },
                },
                emphasis: {
                    lineStyle: {
                        width: 1,
                        color: 'rgba(128, 128, 128, 0.5)',
                    },
                    chordStyle: {
                        lineStyle: {
                            width: 1,
                            color: 'rgba(128, 128, 128, 0.5)',
                        },
                    },
                },
            },
        },
        gauge: {
            startAngle: 225,
            endAngle: -45,
            axisLine: {
                show: true,
                lineStyle: {
                    color: [[0.2, '#86b379'], [0.8, '#68a54a'], [1, '#408829']],
                    width: 8,
                },
            },
            axisTick: {
                splitNumber: 10,
                length: 12,
                lineStyle: {
                    color: 'auto',
                },
            },
            axisLabel: {
                textStyle: {
                    color: 'auto',
                },
            },
            splitLine: {
                length: 18,
                lineStyle: {
                    color: 'auto',
                },
            },
            pointer: {
                length: '90%',
                color: 'auto',
            },
            title: {
                textStyle: {
                    color: '#333',
                },
            },
            detail: {
                textStyle: {
                    color: 'auto',
                },
            },
        },
        textStyle: {
            fontFamily: 'Arial, Verdana, sans-serif',
        },
    };
    this.time = new ReactiveVar(new Date());

    // instance.subscription = this.subscribe('tracks.range', instance.start.get()._d, instance.end.get()._d);
    // instance.subscription2 = instance.subscribe('tracks.range.custom', instance.start.get()._d, instance.end.get()._d);

    // instance.subBuble = this.subscribe("tracks.live");

    this.autorun(function () {
        // instance.subscription = instance.subscribe('tracks.range', instance.start.get()._d, instance.end.get()._d);

        //Подтягиание данных при выборе произвользого диапазона дат за рамками изначального 1 месяца
        // if( instance.start.get()._d < moment().subtract(1, 'month').subtract(1, 'days')._d  ) {
        //   instance.subscription = instance.subscribe('tracks.range', instance.start.get()._d, instance.end.get()._d);
        // }

        /*if( instance.subBuble.ready()  ){
         // var item = Tracks.find({user:{$exists: true}}).fetch();

         }
         if( instance.subscription.ready()  ){
         // var array = Tracks.find({$and: [{_createdAt: {$gte: instance.start.get()._d}, lat:{$gt: 0}}, {_createdAt: {$lt: instance.end.get()._d}, lat:{$gt: 0}} ] }).fetch();
         //Передача данных и реактивная перерисовка карты
         // instance.datas.set(array);
         }*/
    });

});

Template.dashboard.onRendered(function () {
    //Step 1: initialize communication with the platform
    var platform = new H.service.Platform({
        app_id: 'D0jbS3gy6jPR7sWFDfYa',
        app_code: 'eC9BZosfWXJIPUi8C3H8jw',
        useHTTPS: true,
    });
    var pixelRatio = window.devicePixelRatio || 1;
    var defaultLayers = platform.createDefaultLayers({
        tileSize: pixelRatio === 1 ? 256 : 512,
        ppi: pixelRatio === 1 ? undefined : 320,
    });
    //Step 2: initialize a map - this map is centered over Europe
    var map = new H.Map(document.getElementById('map'),
        defaultLayers.normal.map, {
            center: {lat: 50, lng: 5},
            zoom: 3,
            pixelRatio: pixelRatio,
        });

    //Step 3: make the map interactive
// MapEvents enables the event system
// Behavior implements default interactions for pan/zoom (also on mobile touch environments)
    var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

// Create the default UI components
    var ui = H.ui.UI.createDefault(map, defaultLayers);

    var buble = new H.ui.InfoBubble({lat: 50, lng: 5}, {});


    var instance = this;
    var content = '';
    var user = {};

    //Инициализация ввода дат, подготовка
    var optionSet1 = {
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: Companys.findOne()._createdAt,
        maxDate: moment(),
        dateLimit: {
            days: 60,
        },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
        },
        opens: 'right',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'MM/DD/YYYY',
        separator: ' to ',
        locale: {
            applyLabel: 'Отправить',
            cancelLabel: 'Очистить',
            fromLabel: 'От',
            toLabel: 'В',
            customRangeLabel: 'Свое',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: [
                'January',
                'February',
                'March',
                'April',
                'May',
                'June',
                'July',
                'August',
                'September',
                'October',
                'November',
                'December',
            ],
            firstDay: 1,
        },
    };

    instance.uspadeRange = function (start, end, label) {
        $('#reportrange_right2 span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    };


    //Динамическая Отрисовка карты
    this.autorun(function () {

        Meteor.call('getSessions', instance.start.get()._d, instance.end.get()._d, function (err, data) {
            if (data) {
                var sessionsLine = echarts.init(document.getElementById('sessions_line'), instance.theme);
                sessionsLine.setOption({
                    tooltip: {
                        trigger: 'axis',
                    },
                    calculable: true,
                    xAxis: [
                        {
                            type: 'category',
                            boundaryGap: false,
                            data: data.days,
                        },
                    ],
                    yAxis: [
                        {
                            type: 'value',
                        },
                    ],
                    series: [
                        {
                            name: 'Deal',
                            type: 'line',
                            smooth: true,
                            itemStyle: {
                                normal: {
                                    areaStyle: {
                                        type: 'default',
                                    },
                                },
                            },
                            data: data.counts,
                        },
                    ],
                });
            }
        });


        map.removeObjects(map.getObjects());
        var array = instance.datas.get();
        // for(var i=0; i < array.length; i++ ){
        //   var marker = new H.map.Marker({lat:array[i].lat, lng:array[i].lon});
        //   map.addObject(marker);
        // }

        // First we need to create an array of DataPoint objects,
        // for the ClusterProvider
        var dataPoints = array.map(function (item) {
            return new H.clustering.DataPoint(item.lat, item.lon);
        });

        // Create a clustering provider with custom options for clusterizing the input
        var clusteredDataProvider = new H.clustering.Provider(dataPoints, {
            clusteringOptions: {
                // Maximum radius of the neighbourhood
                eps: 32,
                // minimum weight of points required to form a cluster
                minWeight: 2,
            },
        });

        // Create a layer tha will consume objects from our clustering provider
        var clusteringLayer = new H.map.layer.ObjectLayer(clusteredDataProvider);

        // To make objects from clustering provder visible,
        // we need to add our layer to the map
        map.addLayer(clusteringLayer);

    });

    //Инициализация ввода дат после начально отрисовки шаблона
    Meteor.setTimeout(function () {
        $('#reportrange_right2').daterangepicker(optionSet1, instance.uspadeRange(moment().subtract(29, 'days'), moment()));

        $('#reportrange_right2 span').html(moment().subtract(1, 'month').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

        // $('#reportrange_right2').daterangepicker(optionSet1, cb);
        //
        // $('#reportrange_right2').on('show.daterangepicker', function() {
        //   // console.log("show event fired");
        // });
        // $('#reportrange_right2').on('hide.daterangepicker', function() {
        //   // console.log("hide event fired");
        // });
        $('#reportrange_right2').on('apply.daterangepicker', function (ev, picker) {
            instance.start.set(moment(picker.startDate));
            instance.end.set(moment(picker.endDate));
            instance.uspadeRange(moment(picker.startDate), moment(picker.endDate));
            // console.log(picker);
            // console.log(moment(picker.startDate));
            // console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
        });
        $('#reportrange_right2').on('cancel.daterangepicker', function (ev, picker) {
            // console.log("cancel event fired");
        });
        $('#destroy').click(function () {
            $('#reportrange_right2').data('daterangepicker').remove();
        });

    }, 1000);

});

Template.dashboard.helpers({
    Company() {
        return Companys.findOne()
    },
    /*itemViewsPersent(){
     var persent = 0;
     var array = Tracks.find({_createdAt: {$lt: moment().subtract(7, 'days')._d}} ).fetch();
     var lastWeek = array.length;
     if(lastWeek) {
     var all = Companys.findOne().counters.itemviews;
     persent = all * 100 / lastWeek;
     return Math.round(persent);
     }
     },*/
    /*addBatchesPersent(){
     var persent = 0;
     var array = Batches.find({_createdAt: {$lt: moment().subtract(7, 'days')._d}} ).fetch();
     var lastWeek = array.length;
     if(lastWeek) {
     var all = Companys.findOne().counters.batches;
     persent = all * 100 / lastWeek;
     return Math.round(persent);
     }
     },*/
    /*addUsersPersent(){
     var persent = 0;
     var array = Meteor.users.find({createdAt: {$lt: moment().subtract(7, 'days')._d}} ).fetch();
     var lastWeek = array.length;
     if(lastWeek) {
     var all = Companys.findOne().counters.users;
     persent = all * 100 / lastWeek;
     return Math.round(persent);
     }
     },*/
    /*limitItemsPersent(){
     var percent = 0;
     var current = Companys.findOne().counters.items;
     if(current) {
     var all = Companys.findOne().counters.itemslimit;
     percent = 100 * current / all;
     return Math.round(percent);
     }
     },*/
    /*limitProductsPersent(){
     var percent = 0;
     var current = Companys.findOne().counters.products;
     if(current) {
     var all = Companys.findOne().counters.productslimit;
     percent = 100 * current / all;
     return Math.round(percent);
     }
     },*/
    /*limitViewsPersent(){
     var persent = 0;
     var current = Companys.findOne().counters.itemviews;
     if(current) {
     var all = Companys.findOne().counters.viewslimit;
     persent = 100 * current / all;
     return Math.round(persent);
     }
     }*/

});
