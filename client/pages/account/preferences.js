import {showError, showSuccess} from "../../notifications";

Template.preferences.onRendered(() => {
    // bootstrap-validator
    $('#form').validator({
        delay: 0,
        disable: false
    });
});

Template.preferences.helpers({
    Company: function () {
        return Companys.findOne();
    },
    linkx: function (id) {
        if (Images.findOne(id)) {
            return Images.findOne(id).link('thumbnail');
        }
    },
    formatPhone(phone) {
        phone = phone.split(/[-() ]/).join('');
        let
            first = phone.substr(0, 3),
            second = phone.substr(3, 3),
            third = phone.substr(6, 4);
        return `${first}-${second}-${third}`;
    }
});

Template.preferences.events({
    'change #mainImg': function (e, t) {
        var f, reader, uploader;
        if (e.currentTarget.files && e.currentTarget.files[0]) {
            reader = new FileReader();
            f = e.currentTarget.files[0];
            reader.onload = function (e) {
                return $('#mainim').attr("src", e.currentTarget.result);
            };
            reader.readAsDataURL(f);
            uploader = Images.insert({
                file: e.currentTarget.files[0],
                streams: 'dynamic',
                chunkSize: 'dynamic'
            }, false);
            uploader.on('uploaded', function (error, fileObj) {
                if (fileObj) {
                    //          Meteor.call 'removeOldImage', FlowRouter.getParam('id') (err, res) ->
                    return Meteor.call('changeCompanyData', 'img', fileObj._id, function (err, res) {
                        if (err) {
                            showError(err.message); console.log(err);
                        }
                        if (res) {
                            showSuccess('Изображение загружено');
                        }
                    });
                }
            });
            uploader.on('error', function (error, fileObj) {
                showError(error);
            });
            return uploader.start();
        }
    },
    'change .company': function (e, t) {
        var value;
        value = e.currentTarget.value;

        // if (!Template.instance().valid)
        //     return ;

        let isValid = $(e.currentTarget).is(':valid'); // надеюсь, bootstrap-validator проставляет этот псевдокласс правильно
        if (!isValid)
            return;

        Meteor.call('changeCompanyData', e.currentTarget.dataset.field, value, function (err, res) {
            if (err) {
                showError(err.message); console.log(err);
            }
            if (res) {
                showSuccess('Данные обновлены');
            }
        });
    }
});
