import {showError, showInfo, showSuccess} from "../../notifications";

Template.changeLog.onCreated(function () {
    window.scrollTo(0,0);
    var instance;
    instance = this;
    this.limit = new ReactiveVar(30);
    this.loaded = new ReactiveVar(true);
    instance.sub = this.subscribe('company.change.log', instance.limit.get());
    window.scrollTo(0, 0);

    return this.autorun(() => {
        if (instance.sub.ready()) {
            instance.loaded.set(false);
        }
        return instance.sub = this.subscribe('company.change.log', instance.limit.get());
    });
});

Template.changeLog.helpers({
    loadervar: function () {
        return Template.instance().loaded.get();
    },
    timeline: function () {
        return Timeline.find({}, {
            sort: {
                _createdAt: -1
            }
        }).fetch();
    },
    tusername: function (id) {
        return Meteor.users.findOne(id).name;
    },
    tusername2: function (id) {
        return Meteor.users.findOne(id).name2;
    },
    created: function (date) {
        return moment(new Date(date)).format('DD-MM-YYYY HH:mm');
    },
    russianAction(action){
      switch (action) {
          case 'login': return 'Логин';
          case 'createNewUser': return 'Создание пользователя';
          case 'removeUser': return 'Удаление пользователя';
          case 'createNewSurvey': return 'Создание опроса';
          case 'removeSurvey': return 'Удаление опроса';
          case 'createNewTest': return 'Создание теста';
          case 'removeTest': return 'Удаление теста';
          case 'createNewAction': return 'Создание акции';
          case 'removeAction': return 'Удаление акции';
          default: return action;
      }
    },
});

Template.changeLog.events({
    'click #showMore': function (e, t) {
        return t.limit.set(Number(t.limit.get()) + 30);
    },

    'click .del': function (e, t) {
        return Meteor.call('removeTimeline', e.currentTarget.id, function (err, res) {
            if (err) {
                showError(err.message); console.log(err);
            }
            if (res) {
                showSuccess('Удалено');
            }
        });
    },
    'click .revert': function (e, t) {
        return Meteor.call('revertTimeline', e.currentTarget.id, function (err, res) {
            if (err) {
                showError(err.message);
            }
            if (res) {
                return showSuccess(TAPi18n.__('action_reverted'));
            }
        });
    },

});
