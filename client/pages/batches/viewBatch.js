import {showError, showInfo, showSuccess} from "../../notifications";

Template.viewBatch.onCreated(function () {
    var instance;
    instance = this;
    this.subscribe('batch.one', FlowRouter.getParam('id'));
    return this.adddom = function (map, lat, lon, color, id, city) {
        var bearsMarker, changeOpacity, domIcon, innerElement, outerElement;
        outerElement = document.createElement('div');
        innerElement = document.createElement('div');
        outerElement.style.cursor = 'pointer';
        innerElement.style.color = '#fff';
        innerElement.style.backgroundColor = color;
        innerElement.style.borderRadius = '50%';
        innerElement.style.border = '1px solid #fff';
        innerElement.style.font = 'bold 14px arial';
        innerElement.style.lineHeight = '15px';
        innerElement.style.textAlign = 'center';
        innerElement.style.width = '15px';
        innerElement.style.height = '15px';
        outerElement.appendChild(innerElement);
        innerElement.title = id + ' - ' + city;
        changeOpacity = function (evt) {
            return FlowRouter.go('salePoint', {
                id: evt.currentTarget.id
            });
        };
        domIcon = new H.map.DomIcon(outerElement, {
            onAttach: function (clonedElement, domIcon, domMarker) {
            },
            //      clonedElement.addEventListener 'click', changeOpacity
            onDetach: function (clonedElement, domIcon, domMarker) {
            }
        });
        bearsMarker = new H.map.DomMarker({
            lat: lat,
            lng: lon
        }, {
            icon: domIcon
        });
        return map.addObject(bearsMarker);
    };
});

Template.viewBatch.onRendered(function () {
    var behavior, defaultLayers, instance, map, pixelRatio, platform;
    instance = this;
    platform = new H.service.Platform({
        app_id: 'D0jbS3gy6jPR7sWFDfYa',
        app_code: 'eC9BZosfWXJIPUi8C3H8jw',
        useHTTPS: true
    });
    pixelRatio = window.devicePixelRatio || 1;
    defaultLayers = platform.createDefaultLayers({
        tileSize: pixelRatio === 1 ? 256 : 512,
        ppi: pixelRatio === 1 ? void 0 : 320
    });
    map = new H.Map(document.getElementById('map'), defaultLayers.normal.map, {
        center: {
            lat: 50,
            lng: 5
        },
        zoom: 3,
        pixelRatio: pixelRatio
    });
    behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));
    behavior.disable(H.mapevents.Behavior.WHEELZOOM);
    return this.autorun(() => {
        return Tracks.find({
            bid: FlowRouter.getParam('id'),
            lat: {
                $exists: true
            },
            lon: {
                $exists: true
            }
        }).forEach((track) => {
            return instance.adddom(map, track.lat, track.lon, '#26b99a', track.bid, track.city);
        });
    });
});

Template.viewBatch.events({
    'click .download': function (e, t) {
        showInfo('Скачивание начнётся через несколько секунд', 'Выполнено');
        return Meteor.call('downloadSingleQRcode', e.currentTarget.id, function (err, res) {
            var blob, elem;
            if (err) {
                showError(err.message);
                console.log(err);
            }
            if (res) {
                blob = new Blob([res], {
                    type: "application/zip"
                });
                elem = window.document.createElement('a');
                elem.href = window.URL.createObjectURL(blob);
                elem.download = 'Batch ' + e.currentTarget.id + '.zip';
                document.body.appendChild(elem);
                elem.click();
                return document.body.removeChild(elem);
            }
        });
    }
});

Template.viewBatch.helpers({
    batch: function () {
        return Batches.findOne();
    },
    Action() {
        return Actions.findOne();
    },
    /*items: function () {
        return Items.find({}, {
            sort: {
                active: -1
            }
        }).fetch();
    },*/
    itemsActive: function () {
        return Items.find({active: true}).fetch();
    },
    itemsInactive: function () {
        return Items.find({active: false}).fetch();
    },
    isToday(date) {
        console.log(moment(date).isSame(moment(), 'day'));
        return moment(date).isSame(moment(), 'day');
    },
    actionName(id) {
        let action = Actions.findOne(id);
        if (action) {
            return action.name;
        }
    },
    surveyTitle(id) {
        let survey = Surveys.findOne(id);
        if (survey) {
            return survey.title;
        }
    },
    linkx: function (id) {
        var url;
        console.log(`image id: ${id}`);
        if (Images.findOne({_id: id})) { /*Даже если undefined, все равно находит. (потому что если undefined, то оно работает как findOne без аргументов)*/

            url = Images.findOne(id).link('thumbnail');
            return url;
        }
    },
    imageOrDefault(base64) {
        if (base64) {
            return 'Картинка';
        } else return 'Картинка не найдена';
    }
});
