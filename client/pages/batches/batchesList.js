import {showError, showInfo} from "../../notifications";

Template.batchesList.onCreated(function () {
    window.scrollTo(0, 0);
    this.subscribe('company.batches');
    return this.loaded = new ReactiveVar(true);
});

Template.batchesList.onRendered(function () {
    var instance;
    instance = this;
    return instance.loaded.set(false);
});

//  Meteor.setTimeout ->
//#    $('#datatable').dataTable({"pageLength": 50, "order": []})
//    instance.loaded.set false
//  , 800
Template.batchesList.events({
    'click .download': function (e, t) {
        showInfo('Скачивание начнётся через несколько секунд', 'Выполнено');
        $(e.currentTarget).addClass('disabled');
        return Meteor.call('downloadSingleQRcode', e.currentTarget.id, function (err, res) {
            var blob, elem;
            if (err) {
                showError(err.message);
                console.log(err);
            }
            if (res) {
                blob = new Blob([res], {
                    type: "application/zip"
                });
                elem = window.document.createElement('a');
                elem.href = window.URL.createObjectURL(blob);
                elem.download = 'Batch ' + e.currentTarget.id + '.zip';
                document.body.appendChild(elem);
                elem.click();
                $(e.currentTarget).removeClass('disabled');
                return document.body.removeChild(elem);
            }
        });
    }
});

Template.batchesList.helpers({
    loadervar() {
        return Template.instance().loaded.get();
    },
    batches() {
        return Batches.find().fetch();
    },
    created(date) {
        return moment(new Date(date)).format('DD-MM-YYYY');
    },
    username(id) {
        let user = Meteor.users.findOne(id);
        if (user)
            return user.name;
    },
    username2(id) {
        let user = Meteor.users.findOne(id);
        if (user)
            return user.name2;
    },
    isAction(id){
        let action = Actions.findOne(id);
        return !!action;
    },
    actionName(id) {
        let action = Actions.findOne(id);
        if (action) {
            return action.name;
        }
    },
    surveyTitle(id) {
        let survey = Surveys.findOne(id);
        if (survey) {
            return survey.title;
        } /*else return 'Не указан'*/
    },
    isSurvey(id) {
        let survey = Surveys.findOne(id);
        return !!survey;
    },
    noFeature(alias) {
        if (Companys.findOne().features.indexOf(alias) !== -1) {
            return true;
        }
    }
})
;
