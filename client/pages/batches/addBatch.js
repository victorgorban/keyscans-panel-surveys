import {showError, showInfo, showSuccess} from "../../notifications";

Template.addBatch.onCreated(function () {
    this.subscribe('actions.type', 'item');
    this.subscribe('company.surveysTitles');
    return this.loaded = new ReactiveVar(true);
});

function handleForm() {
    var object;
    object = {};
    object.name = $('#name').val().trim();
    object.amount = Number($('#amount').val());
    object.action = $('#action').val();
    object.survey = $('#survey').val();
    object.expire = $('#expire').val();
    return Meteor.call('generateBatch', object, function (err, res) {
        if (err) {
            showError(err.message);
            console.log(err);
        }
        if (res) {
            showSuccess('Партия создана');
            return FlowRouter.go('batchesList');
        }
    });

}

Template.addBatch.onRendered(function () {
    let instance = this;
    // bootstrap-validator
    $('#form').validator({
        delay: 0,
        disable: false
    }).on('submit', function (e) {
        if (e.isDefaultPrevented()) {
            // showError('form invalid');
        } else {
            e.preventDefault();
            handleForm();
        }
    })
});

Template.addBatch.events({
    /*'click #generate'(e, t) { // go to form.validator.submit
    },*/
    'change #excelFile'(e) {
        if (!$('#name').val())
            return showError('Сначала введите название');

        let f, reader, uploader;
        reader = new FileReader();

        let fnode = document.getElementById('excelFile');
        let files = fnode.files;
        console.log(files);
        f = files[0];
        if (!f)
            return showError('Вы не выбрали файл');
        if (!f.size)
            return showError('Выберите не пустой файл');
        // return true;
        reader.readAsDataURL(f);
        uploader = Docs.insert({
            file: f,
            streams: 'dynamic',
            chunkSize: 'dynamic',
        }, false);
        uploader.on('error', function (errorMsg, fileObj) {
            return error(errorMsg);
        });
        uploader.on('uploaded', function (errorMsg, fileObj) {
            if (fileObj) {
                var batch = {};
                batch.name = $('#name').val().trim();
                batch.action = $('#action').val();
                batch.survey = $('#survey').val();
                batch.expire = $('#expire').val();
                batch._id = Random.id();
                // Meteor.call('generateBatch', batch);

                Meteor.call('importBatchItemsFromFile', batch, fileObj._id, (err, res) => {

                    if (err) {
                        console.log(err);
                        if (err.reason == 'duplicate key') {
                            return showError('Найден дубликат id, импорт отменен');
                        }
                        return showError(err.message);
                    } else {
                        showSuccess('Партия успешно импортирована из excel файла');
                        FlowRouter.go('batchesList');
                    }


                });

            }
        });

        uploader.start();

    }
});

Template.addBatch.helpers({
    loadervar: function () {
        return Template.instance().loaded.get();
    },
    actions: function () {
        return Actions.find().fetch();
    },
    surveys: function () {
        return Surveys.find().fetch();
    }
});
