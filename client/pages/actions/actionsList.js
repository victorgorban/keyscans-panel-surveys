import { showError, showSuccess } from '../../notifications';

Template.actionsList.onCreated(function() {
    window.scrollTo(0, 0);
    let instance = this;
    instance.loaded = new ReactiveVar(false);
    this.sub = this.subscribe('company.actions', function() {
        instance.loaded.set(true);
    });


    /*return Tracker.autorun(() => {
        if (instance.sub.ready()) {
            instance.loaded.set(false);
        }
        instance.sub = this.subscribe('company.actions', function () {
            instance.loaded.set(true);
        }); // слабо полезная фишка. Но, возможно, имеет смысл при слабом интернете

    });*/
});

//  console.log(Actions.find({},{sort: {_createdAt: -1}}).fetch());
Template.actionsList.helpers({
    loadervar() {
        // какой смысл? Все равно оно показывается только при F5. А т.к. на заднем фоне и так значок загрузки, это только мешает.
        return Template.instance().loaded.get();
    },
    company() {
        return Companys.findOne();
    },
    actions() {
        return Actions.find(
            {},
            {
                sort: {
                    _createdAt: -1
                }
            }
        ).fetch();
    },
    itemName(id) {
        var obj;
        //    console.log(id)
        obj = Actions.findOne(id);
        switch (obj.type) {
            case 'товар':
                return Products.findOne(obj.item).name;
            case 'опрос':
                return Surveys.findOne(obj.item).title;
            case 'новостной пост':
                return Contents.findOne(obj.item).title;
            case 'видео':
                return 'video';
            default:
                return void 0;
        }
    },
    created(date) {
        return moment(date).format('DD-MM-YYYY');
    },
    linkx(id) {
        var url;
        if (Images.findOne(id)) {
            url = Images.findOne(id).link('thumbnail');
            return url;
        }
    },
    russianType(en) {
        switch (en) {
            case 'item':
                return 'Товар';
            case 'video':
                return 'Видео';
            case 'survey':
                return 'Опрос';
            default:
                return 'Не указан';
        }
    },
    russianStatus(en) {
        switch (en) {
            case 'active':
                return 'Активная';
            case 'disabled':
                return 'Архивная';
            default:
                return 'Не указан';
        }
    }
});

Template.actionsList.events({
    'click .trash': function(e, t) {
        Meteor.call('removeAction', e.currentTarget.id, function(err, res) {
            if (err) {
                showError(err.message);
                console.log(err);
                return;
            }
            if (res) {
                showSuccess();
            }
        });
    }
});
