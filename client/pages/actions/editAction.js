import {showError, showSuccess} from "../../notifications";

Template.editAction.onCreated(function () {
    window.scrollTo(0,0);
    this.subscribe('action.one', FlowRouter.getParam('id'));
});

Template.editAction.onRendered(() => {
    // bootstrap-validator
    $('#form').validator({
        delay: 0,
        disable: false
    });
});


Template.editAction.helpers({
    datepicker: function (date) {
        return moment(date).format('YYYY-MM-DD');
    },
    lang: function () {
        return TAPi18n.getLanguage();
    },
    action: function () {
        return Actions.findOne(FlowRouter.getParam('id'));
    },
    product: function () {
        return Products.findOne(Actions.findOne(FlowRouter.getParam('id')).item);
    },
    video: function () {
        return Images.findOne(Actions.findOne(FlowRouter.getParam('id')).item);
    },
    linkx: function (id) {
        var url;
        url = Images.findOne(id).link();
        return url;
    },
});

Template.editAction.events({
    'change .form-control': function (e, t) {
        var field, value;
        field = e.currentTarget.id;
        value = e.currentTarget.value;

        let isValid = $(e.currentTarget).is(':valid'); // надеюсь, bootstrap-validator проставляет этот псевдокласс правильно
        if (!isValid)
            return;

        return Meteor.call('updateActionData', field, FlowRouter.getParam('id'), value, function (err, res) {
            if (err) {
                showError(err.message); console.log(err);
            }
            if (res) {
                showSuccess('Сохранено');
            }
        });
    },
    'click #remove': function (e, t) {
        let id;
        id = FlowRouter.getParam('id');
        return Meteor.call('removeAction', id, function (err, res) {
            if (err) {
                showError(err.message); console.log(err);
            }
            if (res) {
                showSuccess('Удалено');
                FlowRouter.go('actionsList');
            }
        });
    },
});
