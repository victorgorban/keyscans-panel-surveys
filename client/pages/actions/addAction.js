import {showError, showSuccess} from "../../notifications";




function submitForm(e){
    e.preventDefault();
    var object = {};
    object.name = $('#name').val().trim();
    object.budget = Number($('#budget').val());
    object.type = $('#type').val();
    object.minPoints = Number($('#minPoints').val());
    object.maxPoints = Number($('#maxPoints').val());
    object.expire = $('#expire').val();

    if (object.name && object.budget) {
        Meteor.call('createNewAction', object, function(err, res) {
            if (err) {
                showError(err.message); console.log(err);
            }
            if (res) {
                showSuccess();
                FlowRouter.go('actionsList');
            }
        });
    } else {
        showError('Введите название акции и бюджет');
    }

}

Template.addAction.onRendered(function() {
    let instance = this;
    // bootstrap-validator
    $('#form').validator({
        delay: 0,
        disable: false
    }).on('submit', function (e) {
        if (e.isDefaultPrevented()) {
            // showError('form invalid');
        } else {
            submitForm(e);
        }
    })
})

Template.addAction.events({

    'change #excelFile': function (e, t) {
        let sender = e.currentTarget;
        const validExts = ['.xlsx', '.xls', '.csv'];
        let fileExt = sender.value;
        fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
        // console.log(fileExt);
        if (!validExts.includes(fileExt)) {
            sender.value = '';
            showError(`Неверное расширение файла, файл должен быть ${validExts.toString()}.`);
        }
    },

    'change #videoFile': function (e, t) {
        let sender = e.currentTarget;
        const validExts = ['.mp4', '.webm', '.mov', '.avi'];
        let fileExt = sender.value;
        fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
        // console.log(fileExt);
        if (!validExts.includes(fileExt)) {
            sender.value = '';
            showError(`Неверное расширение файла, файл должен быть ${validExts.toString()}.`);
        }
    },

});

Template.addAction.helpers({
    loadervar: function () {
        return Template.instance().loaded.get();
    },
    pass: function () {
        return Random.id();
    },

});
