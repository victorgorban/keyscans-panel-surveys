import _ from 'lodash'

function getNumber(number) {
    if (!number) {
        return 0;
    }
    return +number;
}

function initMapResults() {
    let results = Results.find().fetch();
    let lastIndex = results.length - 1;
    for (let [i, value] of results.entries()) {
        let marker = L.marker([value.latitude, value.longitude]).addTo(map);
        marker.bindPopup(`<p>Ответили с координатами ${value.latitude}, ${value.longitude}</p>`);
        if (i == lastIndex) {
            marker.openPopup();
            map.flyTo([value.latitude, value.longitude])
        }
    }
}

function initQuestionsResults() { // orUpdate - все равно полностью перерисовывать, так что большого оверхеда тут нет.
    let survey = Surveys.findOne(FlowRouter.getParam('id'));
    let total = survey.answered;
    let qs = Surveys.findOne(FlowRouter.getParam('id')).questions;
    window.answeredCharts = [];

    $('.select-answers-stats').each(function (index, node) {
        var ctx = node.getContext('2d');
        let qid = node.dataset['questionId'];
        let question = qs.find(el => el._id == qid);
        let qanswered = question.answered;

        let labels = [];
        let data = [];
        question.answers.forEach(function (a) {
            labels.push(a.text);
            data.push(a.answered || 0);
        })


        let chart = new Chart(ctx, {
            type: 'horizontalBar',
            data: {
                labels: labels,
                datasets: [{
                    data: data,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                legend: {display: false},
                title: {
                    display: true,
                    text: 'Кол-во ответов'
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
        window.answeredCharts.push({
            qid,
            type: 'selectAnswersAnswered',
            chart
        })
    })
}

// график результатов теста (сколько правильных ответов)
function initTestResults() {
    let points = Results.find().fetch().map(r => r.totalPoints || 0)
    let minPoints = _.min(points);
    let maxPoints = _.max(points);
    let ranges = [];
    let labels = [];
    let data = [];
    let gap = ((maxPoints - minPoints) / 5) // + остаток
    if (gap < 5) {
        gap = ((maxPoints - minPoints) / 2);
    }
    gap = Math.round(gap);
    for (let i = 0; i <= 5; i++) { //<= - чтобы захватить остаток
        let min = i * gap;
        let max = (i + 1) * gap;
        ranges.push({
            min,
            max,
        })
        labels.push(`${min}-${max}`)
        let count = _.countBy(points, p => p >= min && p < max)
        data.push(count.true || 0)
    }

    let node = document.getElementById('test-correct-stats');
    var ctx = node.getContext('2d');

    let chart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: labels,
            datasets: [{
                data: data,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            legend: {display: false},
            title: {
                display: true,
                text: 'Кол-во баллов'
            },
        }
    });
    window.correctStatsChart = chart;
}

function updateQuestionsResults() { // orUpdate - все равно полностью перерисовывать, так что большого оверхеда тут нет.
    let survey = Surveys.findOne(FlowRouter.getParam('id'));
    let total = survey.answered;
    let qs = Surveys.findOne(FlowRouter.getParam('id')).questions;

    $('.select-answers-stats').each(function (index, node) {
        var ctx = node.getContext('2d');
        let qid = node.dataset['questionId'];
        let question = qs.find(el => el._id == qid);
        let qanswered = question.answered;

        let labels = [];
        let data = [];
        question.answers.forEach(function (a) {
            labels.push(a.text);
            data.push(a.answered || 0);
        })

        let chart = window.answeredCharts.find(c => c.type == 'selectAnswersAnswered' && c.qid == qid).chart;
        /*change chart*/
        chart.data.labels = labels;
        chart.data.datasets[0].data = data;

        chart.update(); // работает просто по-царски
    })

}


function updateTestResults() {
    let points = Results.find().fetch().map(r => r.totalPoints || 0)
    let minPoints = _.min(points);
    let maxPoints = _.max(points);
    let ranges = [];
    let labels = [];
    let data = [];
    let gap = ((maxPoints - minPoints) / 5) // + остаток
    if (gap < 5) {
        gap = ((maxPoints - minPoints) / 2);
    }
    gap = Math.round(gap);

    for (let i = 0; i <= 5; i++) { //<= - чтобы захватить остаток
        let min = i * gap;
        let max = (i + 1) * gap;
        ranges.push({
            min,
            max,
        })
        labels.push(`${min}-${max}`)
        let count = _.countBy(points, p => p >= min && p < max)
        data.push(count.true || 0)
    }

    window.correctStatsChart.data.labels = labels;
    window.correctStatsChart.data.datasets[0].data = data;
    window.correctStatsChart.update()
}

Template.testStats.onCreated(function () {
    window.scrollTo(0, 0);
    let self = this;
    self.surveySub = self.subscribe('survey.stats', FlowRouter.getParam('id'));

    setTimeout(function () {
        let map = L.map('map').setView([51.505, -0.09], 13);
        window.map = map;

        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1,
            accessToken: 'pk.eyJ1IjoidmljdG9yZ29yYmFuMiIsImEiOiJja2FiZmVvNTMxNjBwMzJxd2dsaWdmemZzIn0.Jl_2wJlWZXPnvAmozUXqMA'
        }).addTo(map);


        // let marker = L.marker([51.5, -0.09]).addTo(map);
        // marker.bindPopup("<b>Hello world!</b><br>I am a popup.");

        initMapResults();

        initTestResults();

        initQuestionsResults();


        /*По изменению опроса, обновляем графики вопросов (если изменились поля вопросов)*/
        Surveys.find().observeChanges({
            changed(id, fields) {
                console.log('survey changed', fields)
                // если fields содержит answered, пересчитываем. И все.
                if (!fields['answered']) {
                    return;
                }
                updateQuestionsResults();
            }
        })

        setTimeout(function () {
            /*По добавлению результатов, добавляем маркер на карту*/
            Results.find().observeChanges({
                added(id, fields) {
                    console.log('results added')
                    let marker = L.marker([fields.latitude, fields.longitude]).addTo(map);
                    marker.bindPopup(`<p>Ответили с координатами ${fields.latitude}, ${fields.longitude}</p>`);
                    marker.openPopup();
                    map.flyTo([fields.latitude, fields.longitude])

                    updateTestResults()
                }
            })
        }, 500)

    }, 500) // looks like html is not ready without this timeout

});

Template.testStats.onRendered(function () {
})

Template.testStats.helpers({
    date(date) {
        return moment(date).format('DD-MM-YYYY');
    },
    linkx: function (id) {
        if (Images.findOne(id)) {
            return Images.findOne(id).link('thumbnail');
        }
    },
    getNumber(number) {
        return getNumber(number);
    },
    survey() {
        return Surveys.findOne(FlowRouter.getParam('id'));
    },
    results() {
        return Results.find().fetch();
    },
    testChecked(survey) {
        return survey.isTest ? 'checked' : '';
    },
});

Template.testStats.events({})


