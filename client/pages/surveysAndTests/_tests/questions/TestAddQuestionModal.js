import {showError, showSuccess} from "../../../../notifications";


function getQuestionFromNode(node) {
    node = $(node);
    let question = {};
    question.type = node.data('questiontype');
    question.required = node.data('required') || false;
    question.multiple = node.data('multiple') || false;
    question.customAnswer = node.data('customanswer') || false;
    question.customAnswer = node.data('customanswer') || false;

    question.answers = node.find('.answers-preview').find('.question-field-answer').map((index, element) => {
        return {
            _id: Random.id(),
            text: element.value
        }
    }).toArray();
    if (question.answers.length == 0) // скорее всего, не тот тип вопроса. Если тот, то явно ошибка, все норм.
        delete question.answers;

    return question;
}

Template.TestAddQuestionModal.helpers({
    survey(){
        return Surveys.findOne(FlowRouter.getParam('id'));
    },
});

Template.TestAddQuestionModal.events({
    'click #addQuestionModalButton'(e) {
        let qnode = $('#addQuestionModal').find('.preview').find('.tab-pane.active').find('.question-preview');
        let question = getQuestionFromNode(qnode);
        // нужен минимум 1 правильный ответ
        question.answers[0].correct = true;
        console.log(question);

        let insertOptions = Session.get('insertQuestionOptions');
        Meteor.call('insertSurveyQuestion', Surveys.findOne(FlowRouter.getParam('id'))._id, question, insertOptions.position, "" + insertOptions.id, (err, res) => {
            if (err) {
                showError(err.message);
                return;
            }
            if (res) {
                showSuccess('Изменения сохранены');
                return;
            }
        });
    }
})