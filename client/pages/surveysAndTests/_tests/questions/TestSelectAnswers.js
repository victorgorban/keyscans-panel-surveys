import {showError, showSuccess} from "../../../../notifications";

function getNumber(number) {
    if (!number) {
        return 0;
    }
    return +number;
}

/**
 * Получить список ответов из DOM узла
 * @param node - DOM node
 */
function getAnswersIdsFromNode(node) {
    node = $(node);
    let answersNodes = node.find('.question-answer-option');
    let ids = answersNodes.map((index, element) => $(element).data('id')).toArray();

    return ids;
}


/**
 *
 * @param node - DOM node that contains .question-field-answer nodes.
 * @param onSuccess - function that will be called when server succeeds
 */
function findAnswersAndUpdateOnServer(node, onSuccess) {
    // Обновляем список ответов.
    let ids = getAnswersIdsFromNode(node);
    let qid = "" + Session.get('thisQuestionId');
    let survey = Surveys.findOne();
    let question = survey.questions.find((q) => q._id == qid);
    let answers = question.answers;

    let orderedAnswers = [];
    for (id of ids) { // лучше не придумал
        let answer = answers.find((a) => a._id == id);
        orderedAnswers.push(answer);
    }


    Meteor.call('updateSurveyQuestionData', Surveys.findOne()._id, qid, 'answers', orderedAnswers, (err, res) => {
        if (err) {
            showError(err.message);
            return;
        }
        if (res) {
            if (onSuccess) {
                onSuccess();
            }
            showSuccess('Изменения сохранены');
            return;
        }
    })
}

function initAnswersSortable(nodes) {
    for (let i = 0; i < nodes.length; i++) {
        let elem = nodes[i];
        new Sortable(elem, {
            animation: 150,
            dataIdAttr: 'data-sort-id',
            scroll: true,
            scrollSensitivity: 60,
            scrollSpeed: 30,
            ghostClass: 'opacity-half',
            // draggable: ':not(.custom-answer)',

            onEnd(e) {
                findAnswersAndUpdateOnServer($(e.item).closest('.answers-sortable'));
            }

        });
    }
}

function getQuestion() {
    let question = "" + Session.get('thisQuestionId');

    // console.log(`finding question: ${typeof survey}-${typeof question}`);
    // console.log('found question: ', Surveys.findOne(survey).questions.find((val)=>val._id===question)); //.find((val)=>val._id===question)._id
    let survey = Surveys.findOne();
    if (survey) {
        return survey.questions.find((val) => val._id === question);
    }
}

Template.TestSelectAnswers.helpers({
    checked(bool) {
        console.log('checked', bool, typeof bool, bool ? 'checked' : '')
        return bool ? 'checked' : '';
    },
    survey() {
        return Surveys.findOne();
    },
    getPercent(what, of) {
        what = getNumber(what);
        of = getNumber(of);

        console.log(what, of);

        if (what != 0 && of != 0) {
            return (what / of * 100).toFixed(2);
        } else {
            return 0;
        }
    },
    isTest() {
        let survey = Surveys.findOne();
        if (!survey) {
            return;
        }
        return survey.test === true;
    },
    isCorrectChecked(answer) {
        let survey = Surveys.findOne();
        if (!survey) {
            return;
        }
        if (survey.test && answer.correct) {
            return 'checked'
        }
    },
    successClass(answer) {
        if (answer.correct === true) {
            return 'success';
        }
    },
    isCustomAnswerPossible(question) {
        return false;
        // return !Surveys.findOne().test && question.type == 'select';
    },
    customAnswerDisabled(question) {
        console.log('question.customAnswer is', question.customAnswer);

        if (!question.customAnswer) {
            return 'disabled';
        } else {
            return '';
        }

        // return question.customAnswer === true ? 'disabled' : "";
    },
    customAnswerChecked(question) {
        return question.customAnswer === true ? 'checked' : "";
    },
    radioOrCheckbox(question) {
        return question.multiple ? "checkbox" : 'radio';
    },
    randomId() {
        return Random.id();
    }

});


Template.TestSelectAnswers.onRendered(() => {
    // answers options moving
    let answersLists = $('.answers-sortable');
    initAnswersSortable(answersLists);
})

Template.TestSelectAnswers.events({
    'change .correct-answer-checkbox'(e) {
        /*todo  НЕ РАБОТАЕТ!!*/
        // /*срочно нужен новый механизм: Получить существующий список ответов, а затем сравнить id. И ВСЕ! И это не толко в этом методе.*/
        /*вроде все норм*/

        console.log('in change .correct-answer-checkbox');
        let isCorrect = $(e.currentTarget).is(":checked");
        let answerId = $(e.currentTarget).closest('.question-answer-option').data('id');
        let question = getQuestion();


        Meteor.call('testSetAnswerCorrect', {
            surveyId: Surveys.findOne()._id,
            questionId: question._id,
            answerId: answerId,
            isCorrect
        }, (err, res) => {
            if (err) {
                showError(err.message);
                return;
            }
            if (res) {
                showSuccess('Изменения сохранены');
                return;
            }
        })
    },

    'click .answers-sortable .delete-answer-option'(e) {
        let node = $(e.currentTarget).closest('.answers-sortable');
        let answerId = $(e.currentTarget).closest('.question-answer-option').data('id');
        let qid = "" + Session.get('thisQuestionId');

        let survey = Surveys.findOne();
        let question = survey.questions.find((q) => q._id == qid);
        let answers = question.answers;
        let index = answers.findIndex(a => a._id == answerId);
        if (index < 0) {
            showError('Ответ не найден');
            return;
        }

        answers.splice(index, 1);

        Meteor.call('updateSurveyQuestionData', survey._id, qid, 'answers', answers, (err, res) => {
            if (err) {
                showError(err.message);
                return;
            }
            if (res) {
                showSuccess('Изменения сохранены');
                return;
            }
        })

    },
    'click .add-answer-option'(e) {

        let question = "" + Session.get('thisQuestionId'); // number. Получение из data-атрибутов делает type conversion???
        question = Surveys.findOne().questions.find(q => q._id == question);

        console.log(question);

        let answers = question.answers;
        console.log(answers);


        answers.push({
            text: 'Новый ответ',
            _id: Random.id()
        });

        console.log(answers);


        // return ;

        let qid = "" + question._id;
        Meteor.call('updateSurveyQuestionData', Surveys.findOne()._id, qid, 'answers', answers, (err, res) => {
            if (err) {
                showError(err.message);
                return;
            }
            if (res) {
                showSuccess('Изменения сохранены');
                return;
            }
        })

        // findAnswersAndUpdateOnServer(answers);
        //    синхронизировать с сервером необходимо, иначе 1) неочевидно 2) костыльно в UI, когда приходит обновление

    },

    'change .question-field-answer'(e) {
        let answerId = $(e.currentTarget).closest('.question-answer-option').data('id');
        let qid = "" + Session.get('thisQuestionId');

        let survey = Surveys.findOne();
        let question = survey.questions.find((q) => q._id == qid);
        let answers = question.answers;
        let index = answers.findIndex(a => a._id == answerId);
        if (index < 0) {
            showError('Ответ не найден');
            return;
        }

        answers[index].text = e.currentTarget.value;

        Meteor.call('updateSurveyQuestionData', survey._id, qid, 'answers', answers, (err, res) => {
            if (err) {
                showError(err.message);
                return;
            }
            if (res) {
                showSuccess('Изменения сохранены');
                return;

            }
        })


    }

});