import {showError} from "../../../notifications";

Template.addTest.onCreated(function () {
    window.scrollTo(0,0);

    Meteor.call('createNewTest', (err, res) => {
        console.log(err, res);
        if (err) {
            showError(err.message); console.log(err);
            return;
        }
        if (res) {

            FlowRouter.go('editTest', {id: res});

        }
    })
});

Template.addTest.events({});

Template.addTest.helpers({});

