import {showError, showSuccess} from "../../../notifications";

function getNumber(number) {
    if (!number)
        return 0;
    return +number;
}

Template.editSurvey.onCreated(function () {
    window.scrollTo(0, 0);
    Session.set('thisSurveyId', FlowRouter.getParam('id'));
    this.subscribe('actions.type', 'survey');
    return this.subscribe('survey.one', FlowRouter.getParam('id'));
});

Template.editSurvey.onCreated(function(){
    setTimeout(function () {
        $('[data-toggle="popover"]').popover({
            container: 'body',
            html: true,
            trigger: 'focus',
            template: '<div style="padding: 0 5px;" class="popover" role="tooltip"><div class="arrow"></div><h4 class="popover-header">Вы уверены?</h4><div class="popover-body" style="display: flex; justify-content: center"><button id="remove" class="remove btn btn-sm btn-danger">Да</button></div></div>'
        });


    }, 250);
})

Template.editSurvey.helpers({
    date(date) {
        return moment(date).format('DD-MM-YYYY');
    },
    linkx: function (id) {
        if (Images.findOne(id)) {
            return Images.findOne(id).link('thumbnail');
        }
    },
    getNumber(number) {
        return getNumber(number);
    },
    survey() {
        return Surveys.findOne(FlowRouter.getParam('id'));
    },
    isTest(survey) {
        if (survey)
            return survey.test;
    },
    actions() {
        return Actions.find();
    },
    testChecked(survey) {
        return survey.isTest ? 'checked' : '';
    },

});

Template.editSurvey.events({
    'focus .remove-show-popover'(e, t) {
        $('.remove').off('click');
        console.log('click .remove-show-popover')
        /*В списках как-то обходимся без этого. Здесь meteor event (click .remove) не ставится.*/
        setTimeout(function () {
            $('.remove').click(function () {
                let id = FlowRouter.getParam('id');
                return Meteor.call('removeSurvey', id, function (err, res) {
                    if (err) {
                        showError(err.message);
                        return;
                    }
                    if (res) {
                        showSuccess('Опрос удален');
                        FlowRouter.go('surveysList');
                        return;
                    }
                });
            })
        }, 0)
    },

    'change #mainImg'(e, t) {
        var f, reader, uploader;
        if (e.currentTarget.files && e.currentTarget.files[0]) {
            reader = new FileReader();
            f = e.currentTarget.files[0];
            reader.onload = function (e) {
                return $('#mainim').attr("src", e.currentTarget.result);
            };
            reader.readAsDataURL(f);
            uploader = Images.insert({
                file: e.currentTarget.files[0],
                streams: 'dynamic',
                chunkSize: 'dynamic'
            }, false);
            uploader.on('uploaded', function (error, fileObj) {
                if (fileObj) {
                    return Meteor.call('updateSurveyData', Surveys.findOne()._id, 'img', fileObj._id, function (err, res) {
                        if (err) {
                            showError(err.message); console.log(err);
                        }
                        if (res) {
                            showSuccess('Изображение загружено');
                        }
                    });
                }
            });
            uploader.on('error', function (error, fileObj) {
                showError(error);
            });
            return uploader.start();
        }
    },

    'change .survey-field'(e) {
        let item = $(e.currentTarget);
        let field = item.data('name');
        let data = item.val();

        if (field == 'title' && !data) {
            showError('Необходимо ввести название опроса');
            return;
        }

        if (['radio', 'checkbox'].includes(item.attr('type'))) {
            data = item.is(":checked");
        }


        Meteor.call('updateSurveyData', Surveys.findOne()._id, field, data, (err, res) => {
            if (err) {
                showError(err.message);
                return;
            }
            if (res) {
                showSuccess('Изменения сохранены');
                return;
            }
        });
    },
    /*'click #remove'(e) {
        let id = Surveys.findOne()._id;
        Meteor.call('removeSurvey', id, (err, res) => {
            if (err) {
                showError(err.message);
                return;
            }
            if (res) {
                showSuccess('Опрос удален');
                FlowRouter.go('surveysList');
                return;
            }
        })
    },*/
    'click #clone'(e) {
        let id = Surveys.findOne()._id;
        Meteor.call('cloneSurvey', id, (err, res) => {
            if (err) {
                showError(err.message);
                return;
            }
            if (res) {
                showSuccess('Опрос дублирован');
                FlowRouter.go('surveysList');
                return;
            }
        })
    },

})


