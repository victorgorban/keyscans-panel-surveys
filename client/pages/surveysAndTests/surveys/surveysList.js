import {showError, showSuccess} from "../../../notifications";
import _ from 'lodash'


function updateSurveys(instance, limit, search) {
    if (!limit)
        limit = instance.limit.get();
    if (!search)
        search = instance.search.get();

    Meteor.call('getSurveysList', limit, search, function (err, res) {
        if (err) {
            return showError(err.message);
        }
        if (res) {
            instance.surveys.set(res);
            instance.loaded.set(true)
        }
    });
}

Template.surveysList.onCreated(function () {
    let instance = this;
    instance.limit = new ReactiveVar(20);
    instance.search = new ReactiveVar('');
    instance.surveys = new ReactiveVar([]);
    instance.sub = this.subscribe('company.surveys'); // разница с this и Meteor в том, что this сам останавливает и очищает публикацию.
    instance.loaded = new ReactiveVar(false);
    instance.notReactive = new ReactiveVar(false);

    instance.autorun(() => {
        if (instance.sub.ready()) {
            return instance.loaded.set(true);
        } else {
            return instance.loaded.set(false);
        }
    })

    instance.autorun(() => {
        updateSurveys(instance, instance.limit.get(), instance.search.get());
    });
});

Template.surveysList.helpers({
    showRefresh() {
        let t = Template.instance();
        return t.notReactive.get() === true;
    },
    showReset() {
        let t = Template.instance();
        return t.limit.get() != 20 || t.search.get() != '' ? true : false;
    },
    limit() {
        return Template.instance().limit.get();
    },
    search() {
        return Template.instance().search.get();
    },
    loaded() {
        return Template.instance().loaded.get();
    },
    surveys() {
        // notReactive: мы не останавливаем подписку, к ней можно вернуться по reset, например
        let surveys = [];
        let instance = Template.instance();
        if (instance.notReactive.get() === true) {
            surveys = Template.instance().surveys.get().sort((a, b) => b._createdAt - a._createdAt);
        } else {
            surveys = Surveys.find({test: {$ne: true}}).fetch().sort((a, b) => b._createdAt - a._createdAt);
        }
        console.log('surveys got', 'notReactive is', instance.notReactive.get(), surveys.map(u => u._createdAt))
        return surveys;
    },
    isAction(id) {
        return !!Actions.findOne(id);
    },
    actionName(id) {
        let action = Actions.findOne(id);
        if (action)
            return action.name;
    },
    created(date) {
        return moment(date).format('DD-MM-YYYY');
    },
    russianStatus(en) {
        switch (en) {
            case 'active':
                return 'Активный';
            case 'disabled':
                return 'Архивный';
            default:
                return 'Не указан'
        }
    }

});

Template.surveysList.events({
    'click #resetLimitSearch'(e, t) {
        t.notReactive.set(false);
        t.loaded.set(true)
        t.limit.set(20)
        t.search.set('')
    },
    'change #limit'(e, t) {
        t.notReactive.set(true);

        let value = e.currentTarget.value;
        t.limit.set(value);
    },
    'input #search': _.debounce((e, t) => {
        let value = e.currentTarget.value;
        if (value.length >= 3) {
            t.notReactive.set(true);
        } else {
            t.notReactive.set(false); // допустим, юзер стер поиск. Длина 0 (или 1). Т.к. поиск еще (или уже) не пошел, то юзер ожидает что список реактивный.
        }
        t.search.set(value);
    }, 300),
    'click .remove': function (e, t) {
        return Meteor.call('removeSurvey', e.currentTarget.id, function (err, res) {
            if (err) {
                showError(err.message);
                console.log(err);
            }
            if (res) {
                showSuccess('Удалено');
                FlowRouter.go('surveysList');
            }
        });
    },
    'click #refresh'(e, t) {
        updateSurveys(t)
    }
});
