import {showError} from "../../../notifications";

Template.addSurvey.onCreated(function () {
    window.scrollTo(0,0);

    Meteor.call('createNewSurvey', (err, res) => {
        console.log(err, res);
        if (err) {
            showError(err.message); console.log(err);
            return;
        }
        if (res) {

            FlowRouter.go('editSurvey', {id: res});

        }
    })
});

Template.addSurvey.events({});

Template.addSurvey.helpers({});

