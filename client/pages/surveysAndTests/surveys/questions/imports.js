import './SurveyDateAnswers.html'
import './SurveyDateAnswers'
import './SurveyTextAnswers.html'
import './SurveyTextAnswers'
import './SurveySelectAnswers.html'
import './SurveySelectAnswers'

import './SurveyAddQuestionModal.html'
import './SurveyAddQuestionModal'

import './SurveyQuestionsList.html'
import './SurveyQuestionsList'

