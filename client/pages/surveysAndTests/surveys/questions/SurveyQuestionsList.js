import {showError, showSuccess} from "../../../../notifications";

function getNumber(number) {
    if (!number)
        return 0;
    return +number;
}

/**
 * Получить ids вопросов в том порядке, как они есть в DOM.
 * @param node - wrapper для вопросов
 */
function getQuestionsIdsFromNode(node) {
    node = $(node);

    let qnodes = node.find('.question');
    let ids = qnodes.map((index, node) => "" + $(node).data('question-id')).toArray();

    return ids;
}

function initSortableQuestions() {

    //questions copying
    let lastCopied = moment('2010-10-08 10:29:23'); // давно
    /*sortable('.questions.sortable')[0].addEventListener('sortstop', function (e) {
        //  Вызывается дважды. Плохо. Именно поэтому тут lastCopied
        let now = new Date();
        if (moment(now).diff(lastCopied, 'seconds', true) < 0.5)  // нереально скопировать два раза за 0.5s. Если да, то это этот баг.
            return;

        lastCopied = now;
        console.log('.questions.sortable sortstop'); // тут мы только копируем. Поэтому я вызываю insertSurveyQuestion

        let item = $(e.detail.item);
        let prev = item.prev();
        let insertPos = null;
        let neighborId = null;
        if (prev.length == 0) {
            insertPos = 'first';
            neighborId = null;
            console.log('Вставка в самое начало. first');
        } else {
            insertPos = 'before';
            neighborId = prev.find('.question').data('question-id');

            console.log('Вставка перед <id>');
        }

        let qnode = item.find('.question')[0];
        let q = getQuestionFromNode(qnode);


        return;

        Meteor.call('insertSurveyQuestion', Surveys.findOne(FlowRouter.getParam('id'))._id, q, insertPos, neighborId);

        if (['select'].includes(item.find('.question').data('type'))) {
            // // если это select, то придется еще и переинициализировать Sortable для answers
            let elem = item.find('.answers-sortable')[0];
            elem = $(elem);
            initSortableQuestions(elem);
        }

        console.log(prev);
        let qid = prev.find('.question').data('question-id');
        console.log(qid);

    });*/


    // questions moving
    new Sortable($('.questions.sortable')[0], {
        handle: '.move-handle', // handle's class
        animation: 150,
        dataIdAttr: 'data-sort-id',
        scroll: true,
        scrollSensitivity: 60,
        scrollSpeed: 30,
        ghostClass: 'opacity-half', // когда есть sortable(), это не работает. Ужас.
        onEnd(e) {
            let wrapper = $('.questions.sortable');
            let ids = getQuestionsIdsFromNode(wrapper);

            console.log(ids);

            Meteor.call('updateSurveyQuestionsOrder', Surveys.findOne(FlowRouter.getParam('id'))._id, ids, (err, res) => {
                if (err) {
                    showError(err.message);
                    console.log(err);
                    return;
                }
                if (res) {
                    showSuccess('Изменения сохранены');
                    return;
                }
            })
        }
    });

}

Template.SurveyQuestionsList.onRendered(() => {
    initSortableQuestions();
});

Template.SurveyQuestionsList.helpers({
    randomId() {
        return Random.id();
    },
    survey() {
        Surveys.findOne(FlowRouter.getParam('id'));
    },
    surveyAnswered() {
        return Surveys.findOne(FlowRouter.getParam('id')).answered // по какой-то причине, это работает даже тогда, когда у findOne() нет answered
    },
    getPercent(what, of) {
        console.log(what, of);
        what = getNumber(what);
        // of = getNumber(of);
        of = getNumber(of);
        console.log(what, of);
        if (what != 0 && of != 0) {
            return (what / of * 100).toFixed(2);
        } else return 0;
    },
    surveyId() {
        return Surveys.findOne(FlowRouter.getParam('id'))._id;
    },
    isTest() {
        let survey = Surveys.findOne(FlowRouter.getParam('id'));
        return survey.test === true;
    },
    questions() {
        // let id = Session.get('thisSurveyId');
        return Surveys.findOne(FlowRouter.getParam('id')).questions;
    },
    requiredChecked(question) {
        return question.required === true ? 'checked' : "";
    },
    isMultiplePossible(question) {
        return question.type == 'select';
    },
    multipleChecked(question) {
        return question.multiple === true ? 'checked' : "";
    },
    isRequired: function (question) {
        if (question.required) {
            return 'true';
        } else return 'false';
    },
    isMultiple: function (question) {
        if (question.multiple) {
            return 'true';
        } else return 'false';
    },
    isCustomAnswer: function (question) {
        if (question.customAnswer) {
            return 'true';
        } else return 'false';
    },
    questionType: function (question) {
        return question.type;
    },
    hasDescription: function (question) {
        return !!question.description;
    }

});


Template.SurveyQuestionsList.events({
    'click .question-delete.delete-button'(e) {
        e.stopPropagation();
        let button = e.currentTarget;
        let parent = $(button).closest('.question-wrapper');
        let qnode = parent.find('.question');
        let qid = "" + qnode.data('question-id');
        console.log(qid);

        let surveyId = Surveys.findOne(FlowRouter.getParam('id'))._id;

        parent.slideUp(400, 'swing', () => {
            parent.remove();
            Meteor.call('removeSurveyQuestion', surveyId, qid, (err, res) => {
                if (err) {
                    showError(err.message);
                    console.log(err);
                    return;
                }
                if (res) {
                    showSuccess('Изменения сохранены');
                }
            });
        })


    },
    'click .question-overlay.open-settings'(e, t) {
        // скрываем question-overlay, чтобы не мешал
        $(e.currentTarget).addClass('hidden');

        let question = $(e.currentTarget).siblings('.question');
        Session.set('thisQuestionId', question.data('question-id'));
        // поднимаем question над overlay
        question.addClass('active-question');
        //    overlay
        // console.log($('#questionsListOverlay')[0]);
        $('#questionsListOverlay').removeClass('visuallyAndPointerHidden');

        //    инициализируем question settings
        let id = question.data('question-id');
        // открываем question settings
        // openQuestionSettings();
    },
    'click .close-settings'(e, t) {
        $('#questionsListOverlay').addClass('visuallyAndPointerHidden');

        // закрываем боковое меню
        // возвращаем question на место
        let surveyId = Session.get('thisSurveyId');
        let questionId = Session.get('thisQuestionId');
        let question = $(`#question-${surveyId}-${questionId}`).removeClass('active-question');

        //    возвращаем question-overlay на место
        question.siblings('.question-overlay').removeClass('hidden');
    },
    'click .add-question'(e) {
        e.stopPropagation(); // не дает событию пройти вниз по z-index
        // alert('stop!');
        let el = $(e.currentTarget);
        let insertPosition = el.data().position;
        let qNode = el.closest('.question-wrapper').find('.question');
        let qid = qNode.data('question-id');

        Session.set('insertQuestionOptions', {
            position: insertPosition,
            id: qid
        });


        $('#addQuestionModal').modal();
    },
    'click .push-question'(e) {
        e.preventDefault();
        Session.set('insertQuestionOptions', {position: 'last'});


        $('#addQuestionModal').modal();
    },
    'click .display-logic'(e) {
        e.stopPropagation();
        console.log('click .display-logic')
    },

    'change .question-field'(e) {
        // answers тут не должны быть
        let item = $(e.currentTarget);
        let field = item.data('name'); // camelCase есть
        let data = item.val();
        // console.log(data);

        if (['radio', 'checkbox'].includes(item.attr('type'))) {
            data = item.is(":checked");
        }

        console.log(data);

        let qid = Session.get('thisQuestionId') + "";

        // return;

        Meteor.call('updateSurveyQuestionData', Surveys.findOne(FlowRouter.getParam('id'))._id, qid, field, data, (err, res) => {
            if (err) {
                showError(err.message);
                console.log(err);
                console.log(err);
                return;
            }
            if (res) {
                showSuccess('Изменения сохранены');
                return;
            }
        });
    }
});