import './questions/imports'

import './addSurvey.html'
import './addSurvey'
import './editSurvey.html'
import './editSurvey'
import './surveyStats.html'
import './surveyStats'
import './surveysList.html'
import './surveysList'