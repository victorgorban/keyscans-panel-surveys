Actions = new Mongo.Collection('actions');
Batches = new Mongo.Collection('batches');
Companys = new Mongo.Collection('companys');
Contents = new Mongo.Collection('contents');
Enrollments = new Mongo.Collection('enrollments');
Items = new Mongo.Collection('items');

Tracks = new Mongo.Collection('tracks');
Surveys = new Mongo.Collection('surveys');
Results = new Mongo.Collection('results')

Timeline = new Mongo.Collection('timeline');

Images = new FilesCollection({
    collectionName: 'images',
    storagePath: '/home/stuurgurs/uploads/Images',
});
Videos = new FilesCollection({
    collectionName: 'videos',
    storagePath: '/home/stuurgurs/uploads/videos',
});

Docs = new FilesCollection({
    collectionName: 'docs',
    storagePath: '/home/stuurgurs/uploads/documents',
});
