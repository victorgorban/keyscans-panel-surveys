var stack_bottomright = {"dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25};


export function showError(text, title) {
    new PNotify({
        title: title || 'Ошибка',
        text: text || '',
        type: 'error',
        styling: 'bootstrap3',
        delay: 3000,
        addclass: "stack-bottomright", // This is one of the included default classes.
        stack: stack_bottomright
    });
}

export function showInfo(text, title) {
    new PNotify({
        title: title || 'Уведомление',
        text: text || '',
        type: 'info',
        styling: 'bootstrap3',
        delay: 3000,
        addclass: "stack-bottomright", // This is one of the included default classes.
        stack: stack_bottomright
    });
}

export function showSuccess(text, title) {
    new PNotify({
        title: title || 'Успешно',
        text: text || '',
        type: 'success',
        styling: 'bootstrap3',
        delay: 3000,
        addclass: "stack-bottomright", // This is one of the included default classes.
        stack: stack_bottomright
    });
}

