Template.leftCol.onRendered(function () {

    Meteor.setTimeout(function () {
        var CURRENT_URL = window.location.href.split('#')[0].split('?')[0],
            $BODY = $('body'),
            $MENU_TOGGLE = $('#menu_toggle'),
            $SIDEBAR_MENU = $('#sidebar-menu');

        $SIDEBAR_MENU.find('a').on('click', function (ev) {
            // console.log('clicked - sidebar_menu');
            var $li = $(this).parent();

            if ($li.is('.active')) {
                $li.removeClass('active active-sm');
                $('ul:first', $li).slideUp();
            } else {
                // prevent closing menu if we are on child menu
                if (!$li.parent().is('.child_menu')) {
                    $SIDEBAR_MENU.find('li').removeClass('active active-sm');
                    $SIDEBAR_MENU.find('li ul').slideUp();
                } else {
                    if ($BODY.is('.nav-sm')) {
                        $li.parent().find('li').removeClass('active active-sm');
                        $($li.parent()[0]).slideUp();
                    }
                }
                $li.addClass('active');

                $('ul:first', $li).slideDown();
            }
        });

        // toggle small or large menu
        $MENU_TOGGLE.on('click', function () {
            console.log('clicked - menu toggle');

            if ($BODY.hasClass('nav-md')) {
                $SIDEBAR_MENU.find('li.active ul').hide();
                $SIDEBAR_MENU.find('li.active').addClass('active-sm').removeClass('active');
            } else {
                $SIDEBAR_MENU.find('li.active-sm ul').show();
                $SIDEBAR_MENU.find('li.active-sm').addClass('active').removeClass('active-sm');
            }

            $BODY.toggleClass('nav-md nav-sm');


            $('.dataTable').each(function () {
                $(this).dataTable().fnDraw();
            });
        });

        // check active menu
        $SIDEBAR_MENU.find('a[href="' + CURRENT_URL + '"]');

        $SIDEBAR_MENU.find('a').filter(function () {
            return this.href == CURRENT_URL;
        }).parent('li').parents('ul').slideDown(function () {
        }).parent().addClass('active');


        // fixed sidebar
        if ($.fn.mCustomScrollbar) {
            $('.menu_fixed').mCustomScrollbar({
                autoHideScrollbar: true,
                theme: 'minimal',
                mouseWheel: {preventDefault: true},
            });
        }

    }, 1000)
});


Template.leftCol.events({
    'click .left_col__toggler'(e) {
        let _class1 = 'left_col-hide';
        let _class2 = 'left_col-show';
        // console.log($(e.currentTarget).position().left);
        if ($(e.currentTarget).position().left < 50) { // не помню точно сколько. Малое значение - панель уже свернута.
            _class1 = 'left_col-show';
            _class2 = 'left_col-hide';
        }
        $('.left_col').addClass(_class1).removeClass(_class2);
        $('.left_col__toggler').addClass(_class1).removeClass(_class2);
        $('.right_col').addClass(_class1).removeClass(_class2);
    },
    'click #profile'(e, t) {
        Session.set('sidebar', 'usersList');
        FlowRouter.go('editUser', {id: Meteor.userId()});
    },
    'click #logout'(e, t) {
        Meteor.logout();
        FlowRouter.go('login');
    },
    'click #company'(e, t) {
        Session.set('sidebar', 'preferences');
        FlowRouter.go('preferences')
    },
});

Template.leftCol.helpers({
    Company() {
        if (Companys.findOne()) {
            return Companys.findOne()
        }
    },

    linkx(id) {
        if (Images.findOne(id)) {
            var url = Images.findOne(id).link('thumbnail');
        } else return 'Not found';
        return url.replace('https://185.251.89.179:3000/', 'https://185.251.89.179:3000/');
    },

});
