Blaze.registerHelper('pathFor', function (path, kw) {
    return FlowRouter.path(path, kw.hash);
});

FlowRouter.triggers.enter([
    () => {
        window.scrollTo(0, 0);
    },
]);

FlowRouter.route('/', {
    name: 'surveysList1',
    action() {
        Session.set('sidebar', 'surveysList');
        BlazeLayout.render('maintemplate', {
            wrapper: 'pageContent',
            pageContent: 'accessWrapper',
            accessWrapperContent: 'surveysList',
            access: (Meteor.user() && Meteor.user().surveys ),
        });
    },
});

FlowRouter.route('/dashboard', {
    name: 'dashboard',
    action() {
        Session.set('sidebar', 'dashboard');
        // console.log("in / route");
        BlazeLayout.render('maintemplate', {
            wrapper: 'pageContent',
            pageContent: 'dashboard',
        });
    },
});

FlowRouter.route('/resetpassword/:token', {
    name: 'resetpassword',
    action(params) {
        BlazeLayout.render('pageContentEmpty', {
            wrapper: 'pageContent',
            pageContent: 'resetpas',
            params: params,
        });
    },
});


FlowRouter.route('/login', {
    name: 'login',
    action() {
        if (!Meteor.userId()) {
            BlazeLayout.render('pageContentEmpty', {
                wrapper: 'pageContent',
                pageContent: 'loginForm',
            });
        } else {
            Session.set('sidebar', 'dashboard');
            BlazeLayout.render('maintemplate', {
                wrapper: 'pageContent',
                pageContent: 'dashboard',
            });
        }
    },
});

FlowRouter.route('/preferences', {
    name: 'preferences',
    action() {
        Session.set('sidebar', 'preferences');
        BlazeLayout.render('maintemplate', {
            wrapper: 'pageContent',
            pageContent: 'accessWrapper',
            accessWrapperContent: 'preferences',
            access: (Meteor.user() && Meteor.user().changeCompany ),
        });
    },
});


FlowRouter.route('/changeLog', {
    name: 'changeLog',
    action() {
        Session.set('sidebar', 'changeLog');
        BlazeLayout.render('maintemplate', {
            wrapper: 'pageContent',
            pageContent: 'accessWrapper',
            accessWrapperContent: 'changeLog',
            access: (Meteor.user() && Meteor.user().timeline ),
        });
    },
});


FlowRouter.route('/usersList', {
    name: 'usersList',
    action() {
        Session.set('sidebar', 'usersList');
        BlazeLayout.render('maintemplate', {
            wrapper: 'pageContent',
            pageContent: 'accessWrapper',
            accessWrapperContent: 'usersList',
            access: (Meteor.user() && Meteor.user().usersList ),
        });
    },
});

FlowRouter.route('/usersList/:id', {
    name: 'editUser',
    action(params) {
        Session.set('sidebar', 'usersList');
        BlazeLayout.render('maintemplate', {
            wrapper: 'pageContent',
            pageContent: 'accessWrapper',
            accessWrapperContent: 'editUser',
            access: (Meteor.user() && Meteor.user().usersList ),
            params: params,
        });
    },
});

FlowRouter.route('/addUser', {
    name: 'addUser',
    action() {
        Session.set('sidebar', 'addUser');
        BlazeLayout.render('maintemplate', {
            wrapper: 'pageContent',
            pageContent: 'accessWrapper',
            accessWrapperContent: 'addUser',
            access: (Meteor.user() && Meteor.user().usersList ),
        });
    },
});

FlowRouter.route('/surveysList', {
    name: 'surveysList',
    action() {
        Session.set('sidebar', 'surveysList');
        BlazeLayout.render('maintemplate', {
            wrapper: 'pageContent',
            pageContent: 'accessWrapper',
            accessWrapperContent: 'surveysList',
            access: (Meteor.user() && Meteor.user().surveys ),
        });
    },
});

FlowRouter.route('/surveysList/:id', {
    name: 'editSurvey',
    action(params) {
        Session.set('sidebar', 'surveysList');
        BlazeLayout.render('maintemplate', {
            wrapper: 'pageContent',
            pageContent: 'accessWrapper',
            accessWrapperContent: 'editSurvey',
            access: (Meteor.user() && Meteor.user().surveys ),
            params: params,
        });
    },
});

FlowRouter.route('/surveyStats/:id', {
    name: 'surveyStats',
    action(params) {
        Session.set('sidebar', 'surveysList');
        BlazeLayout.render('maintemplate', {
            wrapper: 'pageContent',
            pageContent: 'accessWrapper',
            accessWrapperContent: 'surveyStats',
            access: (Meteor.user() && Meteor.user().surveys ),
            params: params,
        });
    },
})

FlowRouter.route('/addSurvey', {
    name: 'addSurvey',
    action() {
        Session.set('sidebar', 'addSurvey');
        BlazeLayout.render('maintemplate', {
            wrapper: 'pageContent',
            pageContent: 'accessWrapper',
            accessWrapperContent: 'addSurvey',
            access: (Meteor.user() && Meteor.user().surveys ),
        });
    },
});

FlowRouter.route('/testsList', {
    name: 'testsList',
    action() {
        Session.set('sidebar', 'testsList');
        BlazeLayout.render('maintemplate', {
            wrapper: 'pageContent',
            pageContent: 'accessWrapper',
            accessWrapperContent: 'testsList',
            access: (Meteor.user() && Meteor.user().surveys ),
        });
    },
});

FlowRouter.route('/testsList/:id', {
    name: 'editTest',
    action(params) {
        Session.set('sidebar', 'testsList');
        BlazeLayout.render('maintemplate', {
            wrapper: 'pageContent',
            pageContent: 'accessWrapper',
            accessWrapperContent: 'editTest',
            access: (Meteor.user() && Meteor.user().surveys ),
            params: params,
        });
    },
});

FlowRouter.route('/testStats/:id', {
    name: 'testStats',
    action(params) {
        Session.set('sidebar', 'testsList');
        BlazeLayout.render('maintemplate', {
            wrapper: 'pageContent',
            pageContent: 'accessWrapper',
            accessWrapperContent: 'testStats',
            access: (Meteor.user() && Meteor.user().surveys ),
            params: params,
        });
    },
})

FlowRouter.route('/addTest', {
    name: 'addTest',
    action() {
        Session.set('sidebar', 'addTest');
        BlazeLayout.render('maintemplate', {
            wrapper: 'pageContent',
            pageContent: 'accessWrapper',
            accessWrapperContent: 'addTest',
            access: (Meteor.user() && Meteor.user().surveys ),
        });
    },
});


FlowRouter.route('/contentsList', {
    name: 'contentsList',
    action() {
        Session.set('sidebar', 'contentsList');
        BlazeLayout.render('maintemplate', {
            wrapper: 'pageContent',
            pageContent: 'accessWrapper',
            accessWrapperContent: 'contentsList',
            access: (Meteor.user() && Meteor.user().contents ),
        });
    },
});

FlowRouter.route('/contentsList/:id', {
    name: 'editContent',
    action(params) {
        Session.set('sidebar', 'contentsList');
        BlazeLayout.render('maintemplate', {
            wrapper: 'pageContent',
            pageContent: 'accessWrapper',
            accessWrapperContent: 'editContent',
            access: (Meteor.user() && Meteor.user().contents ),
            params: params,
        });
    },
});

FlowRouter.route('/addContent', {
    name: 'addContent',
    action() {
        Session.set('sidebar', 'addContent');
        BlazeLayout.render('maintemplate', {
            wrapper: 'pageContent',
            pageContent: 'accessWrapper',
            accessWrapperContent: 'addContent',
            access: (Meteor.user() && Meteor.user().contents ),
        });
    },
});

FlowRouter.route('/actionsList', {
    name: 'actionsList',
    action() {
        Session.set('sidebar', 'actionsList');
        BlazeLayout.render('maintemplate', {
            wrapper: 'pageContent',
            pageContent: 'accessWrapper',
            accessWrapperContent: 'actionsList',
            access: (Meteor.user() && Meteor.user().actions ),
        });
    },
});

FlowRouter.route('/actionsList/:id', {
    name: 'editAction',
    action(params) {
        BlazeLayout.render('maintemplate', {
            wrapper: 'pageContent',
            pageContent: 'accessWrapper',
            accessWrapperContent: 'editAction',
            access: (Meteor.user() && Meteor.user().actions ),
            params: params,
        });
    },
});

FlowRouter.route('/addAction', {
    name: 'addAction',
    action() {
        Session.set('sidebar', 'addAction');
        BlazeLayout.render('maintemplate', {
            wrapper: 'pageContent',
            pageContent: 'accessWrapper',
            accessWrapperContent: 'addAction',
            access: (Meteor.user() && Meteor.user().actions ),
        });
    },
});

FlowRouter.route('/enrollmentsList', {
    name: 'enrollmentsList',
    action() {
        Session.set('sidebar', 'enrollmentsList');
        BlazeLayout.render('maintemplate', {
            wrapper: 'pageContent',
            pageContent: 'accessWrapper',
            accessWrapperContent: 'enrollmentsList',
            access: (Meteor.user() && Meteor.user().changeCompany ),
        });
    },
});

/*
FlowRouter.route('/enrollmentsList/byAction/:id', {
    name: 'enrollmentsListByAction',
    action() {
        Session.set('sidebar', 'enrollmentsList');
        BlazeLayout.render('maintemplate', {
            wrapper: 'pageContent',
            pageContent: 'accessWrapper',
            accessWrapperContent: 'enrollmentsListByAction',
            access: (Meteor.user() && Meteor.user().changeCompany ),
        });
    },
});

FlowRouter.route('/enrollmentsList/byUser/:id', {
    name: 'enrollmentsListByUser',
    action() {
        Session.set('sidebar', 'enrollmentsList');
        BlazeLayout.render('maintemplate', {
            wrapper: 'pageContent',
            pageContent: 'accessWrapper',
            accessWrapperContent: 'enrollmentsListByUser',
            access: (Meteor.user() && Meteor.user().changeCompany ),
        });
    },
});
*/


FlowRouter.route('/batchesList', {
    name: 'batchesList',
    action() {
        Session.set('sidebar', 'batchesList');
        BlazeLayout.render('maintemplate', {wrapper: 'pageContent', pageContent: 'batchesList' });
    }
});

FlowRouter.route('/batchesList/:id', {
    name: 'viewBatch',
    action(params) {
        Session.set('sidebar', 'batchesList');
        BlazeLayout.render('maintemplate', {wrapper: 'pageContent', pageContent: 'viewBatch', params: params });
    }
});

FlowRouter.route('/addBatch', {
    name: 'addBatch',
    action() {
        Session.set('sidebar', 'addBatch');
        BlazeLayout.render('maintemplate', {wrapper: 'pageContent', pageContent: 'addBatch' });
    }
});

FlowRouter.route('/editBatch/:id', {
    name: 'editBatch',
    action: function(params) {
        Session.set('sidebar', 'batchesList');
        BlazeLayout.render('maintemplate', {wrapper: 'pageContent', pageContent: 'editBatch', params: params });
    }
});

FlowRouter.route('/videosList', {
    name: 'videosList',
    action() {
        Session.set('sidebar', 'videosList');
        BlazeLayout.render('maintemplate', {wrapper: 'pageContent', pageContent: 'videosList' });
    }
});

FlowRouter.route('/addVideo', {
    name: 'addVideo',
    action() {
        Session.set('sidebar', 'addVideo');
        BlazeLayout.render('maintemplate', {wrapper: 'pageContent', pageContent: 'addVideo' });
    }
});

FlowRouter.route('/editVideo/:id', {
    name: 'editVideo',
    action: function(params) {
        Session.set('sidebar', 'videosList');
        BlazeLayout.render('maintemplate', {wrapper: 'pageContent', pageContent: 'editVideo', params: params });
    }
});


