import 'bootstrap-validator'; // Не знаю где лучше. Здесь импортировать логично, т.к. либа используется почти везде в приложении

import {Meteor} from 'meteor/meteor';
import './router.js'
import './main.html';

import './collections.js'
import '/lib/common.js';

import './parts/imports';

import './pages/imports'

import './wrappers/imports'
FlowRouter.wait();


// BlazeLayout.setRoot('#root');

Meteor.startup(function () {

    // company и так загружается в mainTemplate
    Meteor.subscribe('userData', {
            onReady: function () {
                FlowRouter.initialize();
            },
            onError: function () {
            },
        },
    );


});




