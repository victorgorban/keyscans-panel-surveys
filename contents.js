db.contents.insertMany([[
    {
        "_id" : "5dcad625408f9d23cb9e3c84",
        "title" : "Новостной пост 3",
        "company" : "mf7JjKdGGnxNkXSYD",
        "survey" : "5dcad0ec408f9d23cb9e3c5b",
        "images" : [
            "xaNKQ3h2PAYQgekvd",
            "R4tc88cxBaPoe5MKn",
            "7cfHNYCmRSour5ys5",
            "MoYq4p79wMnFJpibi",
            "mkzQ6eD6yimqqTYdp"
        ],
        "rewardPoints" : 28,
        "status" : "active",
        "text" : "Текст \nпоста"
    },
    {
        "_id" : "5dcad625408f9d23cb9e3c86",
        "title" : "News title2",
        "company" : "mf7JjKdGGnxNkXSYD",
        "survey" : "5dcad0ec408f9d23cb9e3c5c",
        "images" : [
            "NHdZkLZqsMDot5mXf",
            "JkwDq2M23gEQWNxKC"
        ],
        "rewardPoints" : 65,
        "status" : "active"
    },
    {
        "_id" : "5dcad625408f9d23cb9e3c8e",
        "title" : "News title3",
        "company" : "mf7JjKdGGnxNkXSYD",
        "survey" : "5dcad0ec408f9d23cb9e3c6d",
        "images" : [
            "NHdZkLZqsMDot5mXf",
            "JkwDq2M23gEQWNxKC"
        ],
        "rewardPoints" : 31,
        "status" : "active"
    },
    {
        "_id" : "wtP7iXKv6H9icmFwS",
        "_createdAt" : ISODate("2019-12-04T11:42:16.206Z"),
        "title" : " Post with video ",
        "images" : [ ],
        "company" : "mf7JjKdGGnxNkXSYD",
        "action" : "2RuBgh67GfxkjSipu",
        "status" : "active"
    },
    {
        "_id" : "swYN7iDtMmfqBTnK6",
        "_createdAt" : ISODate("2019-12-04T11:42:22.780Z"),
        "title" : " Post with video ",
        "images" : [ ],
        "company" : "mf7JjKdGGnxNkXSYD",
        "action" : "F8KsqfFprY3DhuQkx",
        "status" : "active"
    },
    {
        "_id" : "yE45LCNSX6dwN4NDz",
        "_createdAt" : ISODate("2019-12-04T11:43:50.402Z"),
        "title" : " Post with video ",
        "images" : [ ],
        "company" : "mf7JjKdGGnxNkXSYD",
        "action" : "yKAEGYmrrbL5apLRR",
        "status" : "active"
    },
    {
        "_id" : "5SiKeLRaL5hrYg9DL",
        "_createdAt" : ISODate("2019-12-04T11:46:24.377Z"),
        "title" : " Post with video ",
        "images" : [ ],
        "company" : "mf7JjKdGGnxNkXSYD",
        "action" : "bj3sKR58GAMfHySKd",
        "status" : "active"
    },
    {
        "_id" : "EvR49QfNtaKhmji2D",
        "_createdAt" : ISODate("2019-12-04T11:54:33.447Z"),
        "title" : " Post with video ",
        "images" : [ ],
        "company" : "mf7JjKdGGnxNkXSYD",
        "action" : "GvnwdgZcFa5AXbhAM",
        "status" : "active"
    },
    {
        "_id" : "NcFdrWZoWSSLbWvdt",
        "_createdAt" : ISODate("2019-12-04T11:56:30.586Z"),
        "title" : " Post with video ",
        "images" : [ ],
        "company" : "mf7JjKdGGnxNkXSYD",
        "action" : "xhmRkY4KuwzqBsWCn",
        "status" : "active"
    },
    {
        "_id" : "5RX5qq67xQNGhXpxy",
        "_createdAt" : ISODate("2019-12-04T11:57:09.837Z"),
        "title" : " Post with video ",
        "images" : [ ],
        "company" : "mf7JjKdGGnxNkXSYD",
        "action" : "6oRig9cfZieeBZBQY",
        "status" : "active"
    },
    {
        "_id" : "zuBaerZWSBcMwMTsN",
        "_createdAt" : ISODate("2019-12-04T11:57:10.386Z"),
        "title" : " Post with video ",
        "images" : [ ],
        "company" : "mf7JjKdGGnxNkXSYD",
        "action" : "icPn33uPAfyyanoNN",
        "status" : "active"
    },
    {
        "_id" : "jQwpicvKuFAvrrkrt",
        "_createdAt" : ISODate("2019-12-04T11:59:47.444Z"),
        "title" : " Post with video ",
        "images" : [ ],
        "company" : "mf7JjKdGGnxNkXSYD",
        "action" : "6zXzsQnCve5kuSitr",
        "status" : "active"
    },
    {
        "_id" : "8XZRkrpoTGqoQGwq2",
        "_createdAt" : ISODate("2019-12-04T11:59:51.464Z"),
        "title" : " Post with video ",
        "images" : [ ],
        "company" : "mf7JjKdGGnxNkXSYD",
        "action" : "W8ZnKa4PCcWpywixt",
        "status" : "active"
    },
    {
        "_id" : "8jLKfyaNEFL9QBu7E",
        "_createdAt" : ISODate("2019-12-04T12:00:30.999Z"),
        "title" : " Post with video ",
        "images" : [ ],
        "company" : "mf7JjKdGGnxNkXSYD",
        "action" : "FsGGzXERoixeknjuB",
        "status" : "active"
    },
    {
        "_id" : "2nEm8onRoAs4BbZf4",
        "_createdAt" : ISODate("2019-12-04T12:02:22.902Z"),
        "title" : " Post with video ",
        "images" : [ ],
        "company" : "mf7JjKdGGnxNkXSYD",
        "action" : "zLMXoNbBgGara92xr",
        "status" : "active"
    },
    {
        "_id" : "YJ3gESbKu4rhFRa9S",
        "_createdAt" : ISODate("2019-12-04T12:04:19.677Z"),
        "title" : " Post with video ",
        "images" : [ ],
        "company" : "mf7JjKdGGnxNkXSYD",
        "action" : "54CNFdWsiyaAYpRcD",
        "status" : "active"
    },
    {
        "_id" : "wTqPsiJhro6bkYEG3",
        "_createdAt" : ISODate("2019-12-04T12:23:05.530Z"),
        "title" : " Post with video ",
        "images" : [ ],
        "company" : "mf7JjKdGGnxNkXSYD",
        "action" : "eEkQFz6wJ2sfsHib4",
        "status" : "active"
    },
    {
        "_id" : "Bg2zqqrzwTeQb9tQa",
        "_createdAt" : ISODate("2019-12-04T12:23:19.089Z"),
        "title" : " Post with video ",
        "images" : [ ],
        "company" : "mf7JjKdGGnxNkXSYD",
        "action" : "rxifCfrJShFkJkNSc",
        "status" : "active"
    },
    {
        "_id" : "DcHnNDBhsrkFLvtb7",
        "_createdAt" : ISODate("2019-12-04T12:26:36.458Z"),
        "title" : " Post with video ",
        "images" : [ ],
        "company" : "mf7JjKdGGnxNkXSYD",
        "action" : "swaZyg3XJnz4hii8R",
        "status" : "active"
    },
    {
        "_id" : "KkHvFzsXxAq2yD5sC",
        "_createdAt" : ISODate("2019-12-04T14:48:53.895Z"),
        "title" : " Post with video ",
        "images" : [ ],
        "company" : "mf7JjKdGGnxNkXSYD",
        "action" : "3SamG8iqAMJtyCtYj",
        "status" : "active"
    },
    {
        "_id" : "9D4xtcwFcFy6qr3fq",
        "_createdAt" : ISODate("2019-12-05T21:35:14.503Z"),
        "title" : "sldfvnlsvnsldvnjohubqarenfvspvbq\\a ;  (that was Chuck Norris)",
        "images" : [ ],
        "company" : "mf7JjKdGGnxNkXSYD",
        "action" : "oWqFfnvDCEzG67abJ",
        "status" : "active"
    },
    {
        "_id" : "sXSLNX5eLyXsrmGig",
        "_createdAt" : ISODate("2020-01-27T10:09:59.415Z"),
        "company" : "mf7JjKdGGnxNkXSYD",
        "title" : "Новый контент",
        "status" : "active",
        "images" : [ ]
    },
    {
        "_id" : "7DG8xG3rvyRyEdGYJ",
        "_createdAt" : ISODate("2020-01-27T10:10:05.414Z"),
        "company" : "mf7JjKdGGnxNkXSYD",
        "title" : "Новый контент",
        "status" : "active",
        "images" : [ ]
    },
    {
        "_id" : "6TXoMb48zd824vtQu",
        "_createdAt" : ISODate("2020-01-27T10:10:07.201Z"),
        "company" : "mf7JjKdGGnxNkXSYD",
        "title" : "Новый контент",
        "status" : "active",
        "images" : [ ]
    },
    {
        "_id" : "aNPY7rK7T88oYiaZx",
        "_createdAt" : ISODate("2020-01-27T10:10:57.556Z"),
        "company" : "mf7JjKdGGnxNkXSYD",
        "images" : [ ],
        "title" : "Новый контент",
        "status" : "active"
    },
    {
        "_id" : "jBFydQoAtBC828m65",
        "_createdAt" : ISODate("2020-01-27T10:15:14.613Z"),
        "company" : "mf7JjKdGGnxNkXSYD",
        "images" : [ ],
        "title" : "Новый контент",
        "status" : "active"
    },
    {
        "_id" : "9TErPaHYjrXkouzJi",
        "_createdAt" : ISODate("2020-01-27T10:16:08.006Z"),
        "company" : "mf7JjKdGGnxNkXSYD",
        "images" : [
            "4yKhn25hFQxLgpQiY",
            "uKNaP6TJQQhQo7TFi"
        ],
        "title" : "Новый контент",
        "status" : "active"
    },
    {
        "_id" : "Lnos45Y6z3MhoanKS",
        "title" : "ВИДЕО ПОСТ",
        "_createdAt" : ISODate("2020-01-28T13:38:52.851Z"),
        "company" : "mf7JjKdGGnxNkXSYD",
        "images" : [ ],
        "status" : "active"
    },
    {
        "_id" : "cYwPZoSkkf25DGxhL",
        "title" : "Видео1",
        "_createdAt" : ISODate("2020-01-30T11:05:07.103Z"),
        "company" : "mf7JjKdGGnxNkXSYD",
        "images" : [ ],
        "status" : "active"
    },
    {
        "_id" : "u5LqKsuNhSXFfXMbz",
        "_createdAt" : ISODate("2020-01-31T15:12:09.205Z"),
        "company" : "mf7JjKdGGnxNkXSYD",
        "images" : [
            "Hj5LLsCakXwzaPEGh",
            "d5GuFFHqeHYn8cxyy"
        ],
        "title" : "Новый контент",
        "status" : "active",
        "survey" : "13",
        "text" : "гыыыы"
    },
    {
        "_id" : "b6xsCFZyreA4GNQ9a",
        "title" : "BLOCKCHEIN TECHNOLOGIES, LLC",
        "_createdAt" : ISODate("2020-01-31T19:04:47.615Z"),
        "company" : "mf7JjKdGGnxNkXSYD",
        "images" : [
            "HLSuxpTdMfwAPdWJ3",
            "oGGbNbrjqncpie2pr",
            "HCiRRvrifbZ2jyQgx"
        ],
        "status" : "active",
        "survey" : "5xe5ByaKqJQRjax5r",
        "text" : "w4t"
    },
    {
        "_id" : "j2Eb4Zf7N62QtFXXR",
        "_createdAt" : ISODate("2020-02-10T06:20:40.413Z"),
        "company" : "mf7JjKdGGnxNkXSYD",
        "images" : [ ],
        "title" : "Новый контент",
        "status" : "active"
    }
]]);