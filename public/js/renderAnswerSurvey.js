/*ну зашибись, compile экранирует строки, оказывается. Зато .js файлы не экранирует.*/

function showError(text, title) {
    new PNotify({
        title: title || 'Ошибка',
        text: text || '',
        delay: 3000,
        type: 'error',
        icon: false,
        styling: 'bootstrap3'
    });
}

function showInfo(text, title) {
    new PNotify({
        title: title || 'Уведомление',
        text: text || '',
        delay: 3000,
        type: 'info',
        icon: false,
        styling: 'bootstrap3'
    });
}

function showSuccess(text, title) {
    new PNotify({
        title: title || 'Успешно',
        text: text || '',
        delay: 3000,
        icon: false,
        type: 'success',
        styling: 'bootstrap3'
    });
}

function checked(bool) {
    if (bool) {
        return "checked"
    } else {
        return "";
    }
}

async function answerSurvey() {
    console.log('in answerSurvey')
    try {
        let geo = {};
        try {
            geo = await new Promise((resolve, reject) => {
                navigator.geolocation.getCurrentPosition(function success(pos) {
                    let coords = pos.coords;
                    resolve({
                        latitude: coords.latitude,
                        longitude: coords.longitude
                    });
                }, function error(err) {
                    reject(err);
                }, { // options
                    enableHighAccuracy: true,
                    timeout: 10000,
                    maximumAge: 0
                })
            })
        } catch (e) {
            return showError('Cannot identify your position')
        }

        let surveyId = $('#survey_id').val();

        let questions = getQuestionsAnswers();

        let request = {
            result: JSON.stringify({
                _id: surveyId,
                questions
            }),
            ...geo
        };

        console.log(request);

        let response = await fetch('/api/answerSurveyAnonim', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(request)
        });


        let result = await response.json();
        console.log(result);
        if (result.status == "error") {
            new PNotify({
                title: 'Error',
                text: result.message,
                icon: false,
                delay: 3000,
                type: 'error',
                styling: 'bootstrap3'
            });
            return;
        }

        if (result.status == "success") {
            showSuccess('Ответ принят')

            console.log(result);
        }
    } catch (e) {
        return showError(e.message)
    }
}

function getQuestionsAnswers() {
    let questions = [];
    let qnodes = $('.question');
    qnodes.each(function () {
        let node = $(this);
        let id = node.data('question-id');

        let isMultiple = node.data('multiple') // нормальный bool
        let isRequired = node.data('required') // нормальный bool
        let title = node.data('title')
        let type = node.data('questiontype');

        if (type == 'select') {
            let answers = node.find('input:checked').map(function () {
                return this.id
            }).toArray()//.find(':checked');// .map(function(){return this.id}).toArray()

            if (isRequired && !answers.length) {
                throw new Error('Вы не ответили на обязательный вопрос: ' + title)
            }
            // console.log('answers.length', answers.length)

            questions.push({
                _id: id,
                answers: answers // только [0]
            })
        } else {
            let answer = node.find('input, textarea').val(); // input type text/date or textarea
            if (isRequired && !answer.length) {
                throw new Error('Вы не ответили на обязательный вопрос: ' + title)
            }
            questions.push({
                _id: id,
                answer: answer
            })
        }
    })
    console.log(questions)

    return questions;
}
