function validateUpdate(video){
    /* безопасность */
    if (!Meteor.user().company)
        throw new Meteor.Error('permissions','Недостаточно прав');
    if (video.meta.company != Meteor.user().company)
        throw new Meteor.Error('permissions','Недостаточно прав');
    if (Meteor.user().status == 'disabled') {
        throw new Meteor.Error('not allowed', 'Недостаточно прав');
    }
    if (Meteor.user().contents !== true)
        throw new Meteor.Error('permissions','Недостаточно прав');


    /*Схема meta*/
}

Meteor.methods({
    setActiveVideo(id) {

        check(id, String);
        let video = Videos.findOne(id);
        if (!video) {
            throw new Meteor.Error('not found', 'Видео не найдено');
        }

        validateUpdate(video);

        Videos.update({_id: id}, {$set: {'meta.active': true}});

        return true;
    }
});
