function validateUpdate(video){
    /* безопасность */
    if (!Meteor.user().company)
        throw new Meteor.Error('permissions','Недостаточно прав');
    if (video.meta.company != Meteor.user().company)
        throw new Meteor.Error('permissions','Недостаточно прав');
    if (Meteor.user().status == 'disabled') {
        throw new Meteor.Error('not allowed', 'Недостаточно прав');
    }
    if (Meteor.user().contents !== true)
        throw new Meteor.Error('permissions','Недостаточно прав');


    /*Схема meta*/
    /*может быть что угодно, тут без валидации*/
}


Meteor.methods({
    updateVideoData(id, field, value) {

        check(id, String);
        check(field, String);
        check(value, String);

        let video = Videos.findOne(id);
        if (!video) {
            throw new Meteor.Error('not found','Видео не найдено');
        }

        validateUpdate(video);

        // обновлять разрешено только meta
        let set = {};
        set[`meta.${field}`] = value;


        Videos.update(id, {
            $set: set
        });

        // console.log(result);
        return true;

    }
});
