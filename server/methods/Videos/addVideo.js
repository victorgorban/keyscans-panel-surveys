Meteor.methods({
    addVideo(id, options) {

        check(id, String);
        check(options, Object);

        if (!Videos.findOne(id)) {
            throw new Meteor.Error('not found', 'Видео не найдено');
        }

        let result = Videos.update(id, {
            $set: {
                'meta.user': Meteor.user()._id,
                'meta.name': options.name.trim(),
                'meta.views': 0,
                'meta.action': options.action,
                'meta.survey': options.survey,
                'meta.company': Meteor.user().company,
            }
        });

        // console.log(result);
        return true;

    }
});
