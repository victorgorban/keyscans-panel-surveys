function validateRemove(video){
    /* безопасность */
    if (!Meteor.user().company)
        throw new Meteor.Error('permissions','Недостаточно прав');
    if (video.meta.company != Meteor.user().company)
        throw new Meteor.Error('permissions','Недостаточно прав');
    if (Meteor.user().status == 'disabled') {
        throw new Meteor.Error('not allowed', 'Недостаточно прав');
    }
    if (Meteor.user().contents !== true)
        throw new Meteor.Error('permissions','Недостаточно прав');


}

Meteor.methods({
    removeVideo(id) {

        check(id, String);

        let video = Videos.findOne(id);
        if (!video) {
            throw new Meteor.Error('not found','Видео не найдено');
        }

        validateRemove(video);


        Videos.remove(id);

        // удаляем видео, и тогда ссылка на него будет неактивная? В принципе, в вк так и сделано.
        // Meteor.call('removeVideoContent', {videos: id, action});

        // console.log(result);
        return true;

    }
});
