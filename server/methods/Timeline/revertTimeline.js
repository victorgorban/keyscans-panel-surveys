/*
function revertLaunchContest(id) {
    Contests.update(id, {$set: {status: 'ready'}})
}

function revertFinishContest(id) {
    Contests.update(id, {$set: {status: 'launched'}})
}
*/

import {decrementCompanyUsers, incrementCompanyUsers} from "../counters";

function revertRemoveUser(timeline) {
    let object = timeline.object;
    let res = Users.insert(object);
    if(res)
        incrementCompanyUsers(object)
}

function revertRemoveAction(timeline) {
    let object = timeline.object;
    let res = Actions.insert(object);
    if(res)
        incrementCompanyActions(object)
}

function revertRemoveSurvey(timeline) {
    let object = timeline.object;
    let res = Surveys.insert(object);
    if(res)
        incrementCompanySurveys(object)
}

Meteor.methods({
    revertTimeline(id) {
        check(id, String);

        var timeline = Timeline.findOne(id);
        if (!timeline) {
            throw new Meteor.Error('Timeline item not found')
        }
        if(Meteor.user().company!=timeline.company){
            throw new Meteor.Error('error-not-allowed', TAPi18n.__('not_enough_permissions'));
        }
        if (Meteor.user().role != 'admin') {
            throw new Meteor.Error('error-not-allowed', TAPi18n.__('not_enough_permissions'));
        }


        switch (timeline.action) {
            case 'removeUser':
                revertRemoveUser(timeline);
                break;
            case 'removeAction':
                revertRemoveAction(timeline);
                break;
            case 'removeSurvey':
                revertRemoveSurvey(timeline);
                break;
            default:
                throw new Meteor.Error('Cannot revert this timeline item')
        }
        // var tid = Timeline.insert({_createdAt: new Date(), user: this.userId, action: 'updateCompanyImage', company: current._id });

        return Timeline.remove(id)
    },
});
