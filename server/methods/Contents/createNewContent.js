Meteor.methods({
    createNewContent(options={}) {
        check(options, Object);

        let content = Contents.initDefaults(options);

        content = Contents.validateInsert(options, {cleanup: true});


        Contents.registerInsert(content);

        return Contents.insert(content);

    },
});