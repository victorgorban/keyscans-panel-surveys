Meteor.methods({
    updateContentData(id, field, data) {
        check(id, String);
        check(field, String);
        // check(data, String); // может быть массив строк, строка. Можно отдать массив images и метод должен нормально все обновить.


        let content = Contents.findOne(id);
        if (!content)
            throw new Meteor.Error('not found', 'Контент не найден');

        let fields = {};
        fields[field] = data; // Так не возникает проблем с многострочниками. В случае с формированием строки, пришлось бы вручную экранировать кавычки и \n.

        content = Contents.validateUpdate(content, fields, {cleanup: false}); /*false - т.к. action может быть "", а cleanup сделает undefined*/

        fields[field] = content[field]; // cleanup может изменить это поле: trim, изменить тип (string=>number)


        // var query;
        // query = '{"' + field + '": "' + data + '"}';

        return Contents.update(id, {
            $set: fields,
        });
    },
});