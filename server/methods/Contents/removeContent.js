Meteor.methods({
    removeContent(id){
        check(id, String);

        let content = Contents.findOne(id);
        if (!content)
            throw new Meteor.Error('not found', 'Контент не найден');


        content = Contents.validateRemove(content);

        Contents.registerRemove(content);

        return Contents.remove(id)

    }
})