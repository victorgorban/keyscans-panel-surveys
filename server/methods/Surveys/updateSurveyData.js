// import _ from 'lodash'

function makeSurveyTest(survey) {
    return survey.questions = survey.questions.filter(q=>q.type=='select'); // omg, что бы я делал без filter
}

Meteor.methods({
    updateSurveyData(surveyId, field, data) {
        check(surveyId, String);
        check(field, String);
        // check(data, String); // это не точно


        // явно нужен initDefaults, чтобы set defaults
        let survey = Surveys.findOne(surveyId);
        if (!survey)
            throw new Meteor.Error('Опрос не найден');


        let fields = {};
        fields[field] = data;

        /*Если тест, то удаляем все questions которые !select*/
        if (field == 'test' && data) {
            makeSurveyTest(survey);
            fields['questions'] = survey.questions;
        }

        // false - из-за action, которое может быть пустым. Будут проблемы - перепишу.
        survey = Surveys.validateUpdate(survey, fields, {cleanup: false});

        fields[field] = survey[field]; // cleanup может изменить это поле: trim, изменить тип (string=>number)

        // var query;
        // query = '{"' + field + '": "' + data + '"}';

        return Surveys.update(surveyId, {
            $set: fields,
        });

    },
});