function validateRemoveSurvey(survey) {
    /* безопасность */
    if (survey.company != Meteor.user().company)
        throw new Meteor.Error('permissions', 'Недостаточно прав');
    if (Meteor.user().surveys !== true)
        throw new Meteor.Error('permissions', 'Недостаточно прав');

    /* целостность */


    return survey;
}

Meteor.methods({
    removeSurvey(surveyId) {
        check(surveyId, String);

        let survey = Surveys.findOne(surveyId);
        if (!survey) {
            throw  new Meteor.Error('Опрос не найден');
        }

        survey = validateRemoveSurvey(survey);

        let res = Surveys.remove(surveyId);
        if (res) {
            Surveys.registerRemove(survey);
        }
        return res;
    },
})
;