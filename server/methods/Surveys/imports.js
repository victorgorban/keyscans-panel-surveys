import './createNewSurvey'
import './createNewTest'
import './cloneSurvey'
import './cloneTest'


import './insertSurveyQuestion'
import './removeSurveyQuestion'
import './removeSurvey'
import './updateSurveyQuestionData'
import './testSetAnswerCorrect'
import './updateSurveyQuestionsOrder'
import './updateSurveyData'

import './getSurveysList'
import './getTestsList'