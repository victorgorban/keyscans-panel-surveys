
Meteor.methods({
    removeSurveyQuestion(surveyId, questionId) {
        check(surveyId, String);
        check(questionId, String);

        let survey = Surveys.findOne(surveyId);
        if (!survey) {
            throw  new Meteor.Error('Опрос не найден');
        }

        Surveys.validateAccess(survey);

        let questions = survey.questions;
        let question = questions.find((q) => q._id == questionId);
        let qindex = questions.findIndex((q) => q._id == questionId);
        if (!question) {
            throw new Meteor.Error('not found','Вопрос не найден');
        }

        questions.splice(qindex, 1); // удаление 1 элемента с qindex

        return Surveys.update(surveyId, {$set: {questions: questions}});
    },
})
;