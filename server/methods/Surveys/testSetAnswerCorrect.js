Meteor.methods({
    testSetAnswerCorrect({surveyId, questionId, answerId, isCorrect}) {
        if (!surveyId || !questionId || !answerId) {
            throw new Meteor.Error('Please set all params');
        }

        let survey = Surveys.findOne(surveyId);
        if (!survey) {
            throw new Meteor.Error('Опрос не найден');
        }

        Surveys.validateAccess(survey);

        let questions = survey.questions;
        let qind = questions.findIndex((q) => q._id == questionId);
        let question = questions.find((q) => q._id == questionId);
        if (!question) {
            throw new Meteor.Error('Вопрос не найден');
        }
        let answer = question.answers.find((a) => a._id == answerId);
        if(!answer)
            throw new Meteor.Error('Ответ не найден');

        if(!question.multiple){ // если ответ всего 1, то проходим по каждому ответу и делаем его unchecked
            for (let a of question.answers){
                a.correct = false;
            }
        }
        answer.correct = isCorrect;

        question = Surveys.Questions.validateSchema(question, {cleanup: true});

        questions[qind] = question;

        return Surveys.update(surveyId, {$set: {questions: questions}});
    },
});