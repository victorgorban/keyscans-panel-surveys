
Meteor.methods({
    /**
     *
     * @param surveyId - the id of survey currently being modified
     * @param question - the question to add
     * @param insertPos - one of ['before','after']
     * @param neighborId - insert the question before... or after this id.
     * @returns {any}
     */
    insertSurveyQuestion(surveyId, question, insertPos, neighborId) {
        check(surveyId, String);
        check(question, Object);
        check(insertPos, String);
        check(neighborId, Match.Maybe(String));

        let survey = Surveys.findOne(surveyId);
        if (!survey) {
            throw  new Meteor.Error('Опрос не найден');
        }
        question = Surveys.Questions.initDefaults(question); // Обязательно, т.к. id вопроса

        /*В тесте может быть только select*/
        if (survey.test && question.type!='select'){
            throw  new Meteor.Error('В тестах могут быть только вопросы типа select.');
        }

        Surveys.validateAccess(survey);
        Surveys.Questions.validateSchema(question, {cleanup: true});

        let questions = survey.questions;
        if (insertPos == 'first') {
            insertPos = 0;
            // splice(0,0,question)

        } else if (insertPos == 'last') {
            // splice(questions.length-1,0,question)
            insertPos = questions.length;

        } else {
            if (!neighborId) // string!
                throw new Meteor.Error('field required','Укажите все параметры!');
            let index = questions.findIndex((q) => q._id == neighborId);
            if (insertPos == 'before') {
                insertPos = index;
            } else if (insertPos == 'after')
                insertPos = index + 1;
        }


        questions.splice(insertPos, 0, question);


        return Surveys.update(surveyId, {$set: {questions: questions}});
    },
})
;