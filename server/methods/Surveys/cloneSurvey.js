import _ from 'lodash'

function validateCloneSurvey(survey) {
    /* безопасность */
    if (survey.company != Meteor.user().company)
        throw new Meteor.Error('permissions', 'Недостаточно прав');
    if (Meteor.user().surveys !== true)
        throw new Meteor.Error('permissions', 'Недостаточно прав');

    /* целостность */


    return survey;
}

Meteor.methods({
    cloneSurvey(surveyId) {
        check(surveyId, String);

        let survey = Surveys.findOne(surveyId);
        if (!survey) {
            throw  new Meteor.Error('Опрос не найден');
        }

        survey = validateCloneSurvey(survey);

        let clone = survey; // Т.к. я не сохраняю старый survey, а вставляю его, то все норм
        clone._createdAt = new Date();
        clone._id = Random.id();
        clone.answered = 0;
        for(let q in clone.questions){
            q.answered = 0;
            q.customAnswered = 0;
        }

        let res = Surveys.insert(clone);
        if (res) {
            Surveys.registerInsert(clone);
        }
        return res;
    },
})
;