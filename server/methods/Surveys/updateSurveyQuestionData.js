function validateUpdateData(questionId, field, data, survey) {
    let thisUser = Meteor.user();
    if (survey.company != thisUser.company)
        throw new Meteor.Error('permissions', 'Недостаточно прав');
    if (thisUser.surveys !== true)
        throw new Meteor.Error('permissions', 'Недостаточно прав');

    let allowedFields = ['title', 'description', 'required', 'multiple', 'customAnswer', 'answers'];

    if (!allowedFields.includes(field))
        throw new Meteor.Error('Недопустимое поле');


    let questions = survey.questions;
    let question = questions.find((q) => q._id == questionId);
    if (!question)
        throw new Meteor.Error('Вопрос не найден');

    if (field == 'answers') {
        function validateUpdateAnswers(question, answers) {
            check(answers, [Object]);
            let allowedQuestionTypes = ['select'];

            if (!allowedQuestionTypes.includes(question.type))
                throw new Meteor.Error('Неверный тип вопроса');

            if (answers.length == 0)
                throw new Meteor.Error('Список ответов не может быть пустым');
        }

        validateUpdateAnswers(question, data, survey);
    }
}

Meteor.methods({
    updateSurveyQuestionData(surveyId, questionId, field, data) {
        check(surveyId, String);
        check(questionId, String);
        check(field, String);
        // check(data, String); // не могу предсказать. Точно может быть string или [string]
        let survey = Surveys.findOne(surveyId);

        if (!survey)
            throw new Meteor.Error('Опрос не найден');

        Surveys.validateAccess(survey);

        let questions = survey.questions;
        let qind = questions.findIndex((q) => q._id == questionId);
        let question = questions.find((q) => q._id == questionId);

        question[field] = data;

        /*если мы сменили тип с multiple на single и у нас test, то должны убрать пометки correct со всех answers с целью избежания ошибок*/
        if (field == 'multiple' && !data && survey.test) { // multiple is falsey value
            question.answers.forEach(a => {
                a.correct = false;
            })
        }

        question = Surveys.Questions.validateSchema(question, {cleanup: true});

        questions[qind] = question;

        return Surveys.update(surveyId, {$set: {questions: questions}});
    },
});