import _ from 'lodash'

function validateCloneTest(test) {
    /* безопасность */
    if (test.company != Meteor.user().company)
        throw new Meteor.Error('permissions', 'Недостаточно прав');
    if (Meteor.user().surveys !== true)
        throw new Meteor.Error('permissions', 'Недостаточно прав');

    /* целостность */


    return test;
}

Meteor.methods({
    cloneTest(testId) {
        check(testId, String);

        let test = Surveys.findOne(testId);
        if (!test) {
            throw  new Meteor.Error('Тест не найден');
        }

        test = validateCloneTest(test);

        let clone = test; // Т.к. я не сохраняю старый test, а вставляю его, то все норм
        clone._createdAt = new Date();
        clone._id = Random.id();
        clone.answered = 0;
        for(let q in clone.questions){
            q.answered = 0;
            q.customAnswered = 0;
        }

        let res = Surveys.insert(clone);
        if (res) {
            Surveys.registerInsert(clone);
        }
        return res;
    },
})
;