Meteor.methods({
    getSurveysList(limit = 50, search = '') {
        limit = parseInt(limit)

        if (!search) {
            search = '';
        } else if (search.length >= 3) {
            // limit = undefined; // не обрезаем по limit
        } else if (search.length < 3) {
            search = ''; // не обрезаем по limit
        }

        let or = [
            {
                title: {
                    $regex: `${search}.*`,
                    $options: 'i'
                }
            },
        ];
        let fields = {
            _createdAt: 1,
            title: 1,
            status: 1,
        }

        let items = Surveys.find({
            test: {$ne: true},
            company: Meteor.user().company,
            $or: or
        }, {
            limit: limit,
            fields: fields,
            sort: {
                _createdAt: -1
            },
        }).fetch();

        return items;
    },
})