function validateUpdateQuestionsOrder(ids, survey) {
    /*Безопасность*/
    Surveys.validateAccess(survey);

    /*Целостность*/
    let questions = survey.questions;
    if (ids.length != questions.length)
        throw new Meteor.Error('Количество id не совпадает с количеством вопросов');

    for (let id of ids) {
        if (questions.findIndex((q) => q._id == id) < 0)
            throw new Meteor.Error('Вопрос не найден');
    }
}

Meteor.methods({
    /**
     *
     * @param surveyId survey to update
     * @param ids - {[String]} array of questions ids in desired order
     * @returns {any}
     */
    updateSurveyQuestionsOrder(surveyId, ids) {
        console.log(surveyId, ids);
        // return true;


        check(surveyId, String);
        console.log(typeof ids[0]);
        check(ids, [String]);
        // check(data, String); // не могу предсказать. Точно может быть string или [string]
        let survey = Surveys.findOne(surveyId);
        if (!survey)
            throw new Meteor.Error('Опрос не найден');

        // т.к. тут более сложно, чем просто checkAccess и schema, вынес это в отдельную ф-ию
        validateUpdateQuestionsOrder(ids, survey);

        let questions = survey.questions;
        let orderedQuestions = [];
        for (id of ids){ // лучше не придумал
            let question = questions.find((q)=>q._id==id);
            orderedQuestions.push(question);
        }

        return Surveys.update(surveyId, {$set: {questions: orderedQuestions}});
    },
});