Meteor.methods({
    createNewTest(/*Ничего*/){
        // явно нужен initDefaults, чтобы set defaults
        // let doc = Surveys.initDefaults({},); // тут set title, questions, company. (if not set)

        let doc = Surveys.initDefaults({test: true, title: 'Новый тест'}); // в Surveys это нужно, хотя бы для ссылок. Правда, webstorm посылает куда-то не туда...
        Surveys.registerInsert(doc);
        // Surveys.validateInsert(doc);// может выкинуть исключение

        return Surveys.insert(doc);
    },
});