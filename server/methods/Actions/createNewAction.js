// function cleanupAction(options) {
//   var possibleFields = ['', 'name', 'budget', 'condition', 'item', 'action', 'points'];
//   for (var key of Object.keys(options)) {
//     if (!possibleFields.includes(key)) {
//       delete options[key];
//     }
//   }
//
//   return [options];
// }


Meteor.methods({
    createNewAction({name, budget, type, minPoints, maxPoints, expire}) {
        // check
        let action = {name, budget, type, minPoints, maxPoints, expire};

        action = Actions.initDefaults(action);

        // console.log(action.company);

        action = Actions.cleanup(action, {});

        // console.log('action after cleanup');
        // console.log(action);

        Actions.validateInsert(action, {});

        // console.log('action after validate');
        // console.log(action);

        Actions.registerInsert(action);

        return Actions.insert(action);
    },
});
