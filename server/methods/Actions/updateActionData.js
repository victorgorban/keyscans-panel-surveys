
Meteor.methods({
    updateActionData(field, id, data) {

        check(field, String);
        check(id, String);
        check(data, String);

        let action = Actions.findOne(id);
        if(!action)
            throw new Meteor.Error('not found','Акция не найдена');

        let fields = {};
        fields[field] = data;

        action = Actions.validateUpdate(action, fields, {cleanup: true});

        fields[field] = action[field]; // cleanup может изменить это поле: trim, изменить тип (string=>number)


        // var query;
        // query = '{"' + field + '": "' + data + '"}';

        return Actions.update(id, {
            $set: fields,
        });

    },

});
