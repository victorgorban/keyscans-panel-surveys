
Meteor.methods({
                 removeAction(actionId) {

                   check(actionId, String);

                   let action = Actions.findOne(actionId);
                   if(!action)
                       throw new Meteor.Error('not found','Акция не найдена');

                   action = Actions.validateRemove(action);

                   Actions.registerRemove(action);

                   return Actions.remove(actionId)
                 },
               });
