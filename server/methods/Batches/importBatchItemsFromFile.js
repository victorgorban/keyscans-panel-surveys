Meteor.methods({
    importBatchItemsFromFile(batch, fileId) {
        batch = Batches.initDefaults(batch);

        let bid = batch._id;

        try {

            // console.log('in importProductItemsFromFile');
            let path = Docs.findOne(fileId).path;
            // console.log('path is');
            // console.log(path);

            var XLSX = require('xlsx');

            var workbook = XLSX.readFile(path);
            var sheet_name_list = workbook.SheetNames;

            var sh = workbook.Sheets[sheet_name_list[0]];

            var data = XLSX.utils.sheet_to_json(sh, {header: 'A'});

            /* get only ids from column A*/
            let dataIds = data.map(row => ('' + row.A).trim());
            dataIds = dataIds.filter(i => i); // remove all falsey values (empty strings)

            if (Items.find({_id: {$in: dataIds}}).count() > 0) {
                throw new Meteor.Error('E11000', 'duplicate key');
            }
            batch.amount = dataIds.length;
            if(!batch.amount){
                throw new Meteor.Error('no items', '0 товаров');
            }

            let items = [];
            for (var i of dataIds) {
                // console.log(data[i].A);
                // console.log(typeof data[i].A);

                var item = Items.initDefaults();
                item._id = i;
                item.batch = bid;

                var barcode = '';
                for (var j = 0; j < item._id.length; j++) {
                    barcode += String(item._id[j].charCodeAt())
                }
                item.barcode = barcode;
                // item.barcode = data[i].C;
                items.push(item);
            }

            try {
                /*Fibers не понимает нативных методов?.*/
                Items.batchInsert(items); // потому что запрашивать отдельно вставку даже 100 документов в 100 раз медленнее, чем один запрос к БД.
                // Items.insert(items); // надеюсь, он импортирует через batchInsert (нет. Да как так-то)
                // do other stuff here
            } catch(error) {
                throw new Meteor.Error('E11000', 'duplicate key');
            }

            batch = Batches.validateInsert(batch, {cleanup: true});
            Batches.insert(batch);

            let timeline = Timeline.initDefaults();
            Timeline.insert({
                ...timeline,
                action: 'importItems'
            });

            return true;

        } catch (e) {
            throw e;
        }
    },
});