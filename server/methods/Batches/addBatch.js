Meteor.methods({
    generateBatch(object) {

        console.log('in generate batch');

        check(object, Object);

        let batch = Batches.initDefaults(object);

        batch = Batches.validateInsert(batch, {cleanup: true});
        let bid = Batches.insert(batch);



        // console.log('start');

        let items = [];

        for (let i = 0; i < object.amount; i++) {

            let itemObject = Items.initDefaults();
            itemObject.action = object.action;
            itemObject.batch = bid;

            let idd = Random.id();
            itemObject._id = idd;

            let barcode = '';
            for(let j=0; j< idd.length; j++ ){
                barcode += String(idd[j].charCodeAt())
            }
            itemObject.barcode = barcode;
            items.push(itemObject);
            // Items.insert(itemObject);



            if (i == 0) Batches.update({_id: bid}, {$set: {item: idd}}); // зачем это?
        }
        try {
            // console.log('in insert items');
            /*Fibers не понимает нативных методов?.*/
            Items.batchInsert(items); // потому что запрашивать отдельно вставку даже 100 документов в 100 раз медленнее, чем один запрос к БД.

            // console.log('complete');
            // Items.insert(items); // надеюсь, он импортирует через batchInsert (нет. Да как так-то)
            // do other stuff here
        } catch(error) {
            throw new Meteor.Error('E11000', 'duplicate key');
        }


        Batches.registerInsert(batch);

        return bid

    }
});
