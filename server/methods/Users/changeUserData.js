var emailValidator = require('email-validator');

function changeUserDataChecks(field, id, data) {
  field = field ? field.trim() : field;
  data = data ? data.trim() : data;


  // validations
  var requiredFields = ['name', 'name2', 'email'];

  if (requiredFields.includes(field) && !data) {
    throw new Meteor.Error('field required', 'Заполните обязательные поля');
  }

  if (!Meteor.user().company) {
    throw new Meteor.Error('permissions','Недостаточно прав');
  }

  var user = Meteor.users.findOne({_id: id});// Meteor.users.find({_id: userId});

  if (Meteor.user().company != user.company) {
    throw new Meteor.Error('permissions','Недостаточно прав');
  }

  if (field == 'email' && !emailValidator.validate(data)) {
    throw new Meteor.Error('invalid email', 'Неверный Email', {method: 'changeUserData'});
  }

  if (field == 'email' && Meteor.users.findOne({'emails.0.address': data})) {
    throw new Meteor.Error('invalid email', 'Email уже в системе', {method: 'changeUserData'});
  }

  if (field == 'phonenumber') {
    data = data.split(/[-() ]/).join('');
  }

  if (field == 'phone' && !/^\+?(0|[1-9]\d*)$/.test(data)) {
    throw new Meteor.Error('invalid phone', 'Неверный телефон', {method: 'changeUserData'});
  }

  // end of validations

  // other checks
  var thisUser = Meteor.users.findOne(id);

  if (field == 'img' && thisUser.img != 'smGwtp2k4kF6aFnEe') {
    Images.remove(thisUser.img)
  }


  return [field, id, data];
}

Meteor.methods({
                 changeUserData(field, id, data) {

                   check(field, String);
                   check(id, String);
                   check(data, String);

                   [field, id, data] = changeUserDataChecks(field, id, data);

                   var query;
                   if (field == 'email') {
                     query = '{"' + 'emails.0.address' + '": "' + data + '"}';
                   } else {
                     query = '{"' + field + '": "' + data + '"}';
                   }

                   return Meteor.users.update(id, {
                     $set: JSON.parse(query),
                   });

                 },

               });
