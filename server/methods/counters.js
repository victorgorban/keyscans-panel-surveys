export function incrementCompanyUsers(user) {
    incrementUsers()
    let company = user.company;
    if (company) {
        let inc = {users: 1};
        Companys.update(company, {
            $inc: inc
        })
    }
}

export function decrementCompanyUsers(user) {
    decrementUsers();
    let company = user.company;
    if (company) {
        let dec = {users: -1};
        Companys.update(company, {
            $inc: dec
        })
    }
}

export function incrementUsers() {
    Counters.upsert({name: 'countUsers'}, {$inc: {value: 1}})
}

export function decrementUsers() {
    Counters.upsert({name: 'countUsers'}, {$inc: {value: -1}})
}

export function incrementCompanyActions(action) {
    incrementActions()
    let company = action.company;
    if (company) {
        let inc = {actions: 1};
        Companys.update(company, {
            $inc: inc
        })
    }
}

export function decrementCompanyActions(action) {
    decrementActions();
    let company = action.company;
    if (company) {
        let dec = {actions: -1};
        Companys.update(company, {
            $inc: dec
        })
    }
}

export function incrementActions() {
    Counters.upsert({name: 'countActions'}, {$inc: {value: 1}})
}

export function decrementActions() {
    Counters.upsert({name: 'countActions'}, {$inc: {value: -1}})
}

export function incrementCompanySurveys(survey) {
    incrementSurveys()
    let company = survey.company;
    if (company) {
        let inc = {surveys: 1};
        Companys.update(company, {
            $inc: inc
        })
    }
}

export function decrementCompanySurveys(survey) {
    decrementSurveys();
    let company = survey.company;
    if (company) {
        let dec = {surveys: -1};
        Companys.update(company, {
            $inc: dec
        })
    }
}

export function incrementSurveys() {
    Counters.upsert({name: 'countSurveys'}, {$inc: {value: 1}})
}

export function decrementSurveys() {
    Counters.upsert({name: 'countSurveys'}, {$inc: {value: -1}})
}