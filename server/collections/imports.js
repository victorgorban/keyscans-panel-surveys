/*В этих файлах - инит коллекций, валидация, симуляция триггеров.*/

import './Users'
import './Actions'
import './Batches'
import './Companys'
import './Contents'
import './Enrollments'
import './Items'
import './Surveys'
import './Results'
import './Timeline'

import './Images'
import './Videos'
import './Docs'
