import SimpleSchema from 'simpl-schema'
import {getErrorMessageFor} from "./commonFunctions";

Actions = new Mongo.Collection('actions');

Actions.initDefaults = function (doc = {}) {
    doc._id = doc._id || Random.id();
    doc._createdAt = doc._createdAt || Date.now();
    doc.company = doc.company || Meteor.user().company; // impossible for server-initiated operations
    doc.budget = doc.budget || 1900; // impossible for server-initiated operations
    doc.current = doc.budget;

    doc.status = doc.status || 'active';

    return doc;
};


Actions.schema = new SimpleSchema({
    _id: {
        type: String,
        optional: true
    },
    _createdAt: {
        type: Date,
        optional: true
    },
    name: String,
    company: String,
    budget: {
        type: SimpleSchema.Integer,
        min: 1
    },
    current: {
        type: SimpleSchema.Integer,
        min: 0,
        // max
        custom() {
            if (this.field("budget").value < this.value)
                return SimpleSchema.ErrorTypes.MAX_NUMBER;
        },
    },
    minPoints: {
        type: SimpleSchema.Integer,
        min: 0
    },
    maxPoints: {
        type: SimpleSchema.Integer,
        // min: 0,
        custom() {
            if (this.field("minPoints").value > this.value)
                return SimpleSchema.ErrorTypes.MIN_NUMBER;

            return undefined;
        },
    },
    type: {
        type: String,
        allowedValues: ['item', 'video', 'survey',]
    },
    expire: {
        type: Date,
        min: function () {
            return new Date();
        }
    },
    status: {
        type: String,
        allowedValues: ['active', 'disabled']
    },

}, {requiredByDefault: true});


/**
 *
 * @param action: doc to validate
 * @param options: options to be passed to SimpleSchema.validate function
 */
Actions.validateSchema = function (action, options = {}) {
    let context = Actions.schema.newContext();

    context.validate(action, options);
    if (!context.isValid()) {
        let error = context.validationErrors()[0];

        throw new Meteor.Error('schema', getErrorMessageFor(error, Actions.getLabelFor));
    }

    return action;
}

/**
 *
 * @param action: doc to validate
 * @returns {*}
 */
Actions.validateAccess = function (action) {
    /* безопасность */
    if (!Meteor.user().company)
        throw new Meteor.Error('permissions', 'Недостаточно прав');
    if (action.company != Meteor.user().company)
        throw new Meteor.Error('permissions', 'Недостаточно прав');
    if (Meteor.user().status == 'disabled') {
        throw new Meteor.Error('permissions', 'Недостаточно прав');
    }
    if (Meteor.user().actions !== true)
        throw new Meteor.Error('permissions', 'Недостаточно прав');

    return action;
}

/**
 * performing cleanup based on SimpleSchema.clean() function.
 * @param doc: action document to cleanup
 * @param options
 */
Actions.cleanup = function (doc, options = {}) {
    return Actions.schema.clean(doc, options);
};

Actions.getLabelFor = function (key) {
    switch (key) {
        case 'name':
            return 'Название';
        case 'company':
            return 'Компания';
        case 'budget':
            return 'Бюджет акции';
        case 'minPoints':
            return 'Мин. кол-во очков';
        case 'maxPoints':
            return 'Макс. кол-во очков';
        case 'type':
            return 'Тип акции';
        case 'expire':
            return 'Дата окончания';
        case 'status':
            return 'Статус акции';
        default:
            return key;
    }
}


/**
 *
 * @param action: doc to validate
 // * @param trimStrings: if true, trims all strings before validating (to avoid '  '==true)
 * @param cleanup: if true, then performing Actions.cleanup before validating.
 * @returns {{}}
 */
Actions.validateInsert = function (action, {cleanup = false}) {
    if (cleanup) {
        action = Actions.cleanup(action);
    }

    Actions.validateAccess(action);

    Actions.validateSchema(action);

    return action;
};

Actions.validateUpdate = function (action, setFields, {cleanup = false}) {
    // чтобы полноценно все проверить (например, мы изменили max. Нужно чтобы он был >=min)
    for (let key of Object.getOwnPropertyNames(setFields)) {
        action[key] = setFields[key];
    }

    if (cleanup) {
        action = Actions.cleanup(action);
    }

    Actions.validateAccess(action);

    Actions.validateSchema(action);

    return action;
};

Actions.validateRemove = function (action) {
    Actions.validateAccess(action);

    return action;
};

/**
 * register insert|update|delete in the system
 * @param actionName: {string} 'insert'|'update'|'delete'|custom
 * @param action: action document
 */
Actions.register = function (actionName, action) {
    let timeline = Timeline.initDefaults();
    timeline.item = action._id;
    if (actionName == 'removeAction') {
        timeline.revert = true;
        timeline.object = action;
    }
    timeline.action = actionName;
    Timeline.insert(timeline);
};

Actions.registerInsert = function (action) {
    Actions.register('createNewAction', action);
};

Actions.registerUpdate = function (action) {
    Actions.register('updateAction', action);
};

Actions.registerRemove = function (action) {
    Actions.register('removeAction', action);
};
