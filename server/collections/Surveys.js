import SimpleSchema from 'simpl-schema'
import {getErrorMessageFor} from './commonFunctions'

Surveys = new Mongo.Collection('surveys');


Surveys.Questions = {};
Surveys.Questions.Answers = {};
Surveys.Questions.Answers.schema = new SimpleSchema({
    _id: {
        type: String,
    },
    text: {
        type: String,
    },
    correct: {
        type: Boolean,
        optional: true
    },
    answered: {
        type: SimpleSchema.Integer,
        optional: true,
        defaultValue: 0
    }
});
Surveys.Questions.Answers.getLabelFor = function (key) {
    switch (key) {
        case 'text':
            return 'Текст ответа';
        case 'correct':
            return 'Это верный ответ';
        default:
            return key;
    }
};
Surveys.Questions.Answers.initDefaults = function (doc = {}) {
    doc._id = doc._id || Random.id();
    doc.text = doc.text || '';

    return doc;
};
Surveys.Questions.Answers.cleanup = function (doc, options = {}) {
    return Surveys.Questions.Answers.schema.clean(doc, options);
};
Surveys.Questions.Answers.validateSchema = function (answer = {}, options = {}) {
    let context = Surveys.Questions.Answers.schema.newContext();

    context.validate(answer, options);
    if (!context.isValid()) {
        let error = context.validationErrors()[0];

        throw new Meteor.Error('schema', getErrorMessageFor(error, Surveys.Questions.Answers.getLabelFor));
    }

    return answer;
};


Surveys.Questions.schema = new SimpleSchema({
    _id: {
        type: String,
        optional: true
    },
    points:{
       type: SimpleSchema.Integer,
       defaultValue: 0,
    },
    title: {
        type: String,
    },
    description: {
        type: String,
        optional: true,
    },
    required: {
        type: Boolean,
        defaultValue: true
    },
    multiple: {
        type: Boolean,
        defaultValue: false
    },
    answered: {
        type: SimpleSchema.Integer,
        defaultValue: 0,
    },
    customAnswered: {
        type: SimpleSchema.Integer,
        defaultValue: 0
    },
    customAnswer: Boolean,
    answers: {
        type: Array,
        optional: true
    },
    'answers.$': Surveys.Questions.Answers.schema,
    type: {
        type: String,
        allowedValues: ['select', 'text', 'date',]
    },
});
Surveys.Questions.getLabelFor = function (key) {
    switch (key) {
        case 'title':
            return 'Название';
        case 'description':
            return 'Описание';
        case 'required':
            return 'Обязательный вопрос';
        case 'multiple':
            return 'Много ответов';
        case 'customAnswer':
            return 'Свой ответ';
        case 'type':
            return 'Тип вопроса';
        case 'answers':
            return 'Ответы';
        default:
            return key;
    }
};
Surveys.Questions.validateSchema = function (question = {}, options = {}) {
    if (options.cleanup)
        question = Surveys.Questions.schema.clean(question);


    let context = Surveys.Questions.schema.newContext();

    context.validate(question, options);
    if (!context.isValid()) {
        let error = context.validationErrors()[0];

        throw new Meteor.Error('schema', getErrorMessageFor(error, Surveys.Questions.getLabelFor));
    }

    return question;
};
Surveys.Questions.initDefaults = function (doc = {}) {
    doc._id = doc._id || Random.id();

    doc.title = doc.title || 'Новый вопрос';
    doc.required = doc.required || true;

    return doc;
};
Surveys.Questions.cleanup = function (doc, options = {}) {
    return Surveys.Questions.schema.clean(doc, options);
};

Surveys.schema = new SimpleSchema({
    _id: {
        type: String,
        optional: true
    },
    totalPoints: {
        type: SimpleSchema.Integer,
        optional: true
    },
    _createdAt: {
        type: Date,
        optional: true
    },
    img:{
      type: String,
      optional: true,
    },
    title: {
        type: String,
    },
    answered: {
        type: SimpleSchema.Integer,
        defaultValue: 0,
        optional: true,
    },
    description: {
        type: String,
        optional: true
    },
    action: {
        type: String,
        optional: true
    },
    company: String,
    test: {
        type: Boolean,
        optional: true
    },
    questions: Array,
    'questions.$': {
        type: Object,
        blackbox: true
    },

    status: {
        type: String,
        allowedValues: ['active', 'disabled']
    },

}, {requiredByDefault: true});

Surveys.initDefaults = function (doc = {}) {
    doc._id = doc._id || Random.id();
    doc._createdAt = doc._createdAt || new Date();
    doc.company = doc.company || Meteor.user().company;
    doc.title = doc.title || 'Новый опрос';

    doc.questions = doc.questions || [];
    doc.status = doc.status || 'active';

    return doc;
};

/**
 *
 * @param survey: doc to validate
 * @param options: options to be passed to SimpleSchema.validate function
 */
Surveys.validateSchema = function (survey, options = {}) {
    let context = Surveys.schema.newContext();

    context.validate(survey, options);
    if (!context.isValid()) {
        let error = context.validationErrors()[0];

        throw new Meteor.Error('schema', getErrorMessageFor(error, Surveys.getLabelFor));
    }

    return survey;
}

/**
 *
 * @param survey: doc to validate
 * @returns {*}
 */
Surveys.validateAccess = function (survey) {
    /* безопасность */
    if (!Meteor.user().company)
        throw new Meteor.Error('permissions', 'Недостаточно прав');
    if (survey.company != Meteor.user().company)
        throw new Meteor.Error('permissions', 'Недостаточно прав');
    if (Meteor.user().status == 'disabled') {
        throw new Meteor.Error('permissions', 'Недостаточно прав');
    }
    if (Meteor.user().surveys !== true)
        throw new Meteor.Error('permissions', 'Недостаточно прав');

    return survey;
}

/**
 * performing cleanup based on SimpleSchema.clean() function.
 * @param doc: survey document to cleanup
 * @param options
 */
Surveys.cleanup = function (doc, options = {}) {
    return Surveys.schema.clean(doc, options);
};

Surveys.getLabelFor = function (key) {
    switch (key) {
        case 'title':
            return 'Название';
        case 'description':
            return 'Описание';
        case 'company':
            return 'Компания';
        case 'questions':
            return 'Вопросы';
        case 'test':
            return 'Это тест?';
        case 'status':
            return 'Статус';
        default:
            return key;
    }
}

/**
 *
 * @param survey: doc to validate
 // * @param trimStrings: if true, trims all strings before validating (to avoid '  '==true)
 * @param cleanup: if true, then performing Surveys.cleanup before validating.
 * @returns {{}}
 */
Surveys.validateInsert = function (survey, {cleanup = false}) {
    if (cleanup) {
        survey = Surveys.cleanup(survey);
    }

    Surveys.validateAccess(survey);

    Surveys.validateSchema(survey);

    return survey;
};

Surveys.validateUpdate = function (survey, setFields, {cleanup = false}) {
    // чтобы полноценно все проверить (например, мы изменили max. Нужно чтобы он был >=min)
    for (let key of Object.getOwnPropertyNames(setFields)) {
        survey[key] = setFields[key];
    }

    if (cleanup) {
        survey = Surveys.cleanup(survey);
    }

    Surveys.validateAccess(survey);

    Surveys.validateSchema(survey);

    return survey;
};

Surveys.validateRemove = function (survey) {
    Surveys.validateAccess(survey);

    return survey;
};

/**
 * register insert|update|delete in the system
 * @param actionName: {string} 'insert'|'update'|'delete'|custom
 * @param survey: survey document
 */
Surveys.register = function (actionName, survey) {
    let timeline = Timeline.initDefaults();
    timeline.item = survey._id;
    timeline.action = actionName;
    if (actionName == 'removeSurvey') {
        timeline.revert = true;
        timeline.object = survey;
    }
    Timeline.insert(timeline);
};

Surveys.registerInsert = function (survey) {
    Surveys.register('createNewSurvey', survey);
};

Surveys.registerUpdate = function (survey) {
    Surveys.register('updateSurvey', survey);
};

Surveys.registerRemove = function (survey) {
    Surveys.register('removeSurvey', survey);
};
