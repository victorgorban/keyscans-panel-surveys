Timeline = new Mongo.Collection('timeline');

Timeline.initDefaults = function(obj = {}){
    obj._createdAt = obj._createdAt || new Date();
    obj.company = obj.company || Meteor.user().company;
    obj.user = Meteor.user()._id;

    return obj;
};