import SimpleSchema from 'simpl-schema'
import {getErrorMessageFor} from "./commonFunctions";

Users = Meteor.users;

Users.initDefaults = function (doc = {}) {
    doc._id = doc._id || Random.id();
    doc._createdAt = doc._createdAt || Date.now();
    doc.company = doc.company || Meteor.user().company; // impossible for server-initiated operations

    doc.status = doc.status || 'active';

    return doc;
};


/*Users.schema = new SimpleSchema({
    _id: {
        type: String,
        optional: true
    },
    _createdAt: {
        type: Date,
        optional: true
    },
    name: String,
    company: String,
    budget: {
        type: SimpleSchema.Integer,
        min: 1
    },
    current: {
        type: SimpleSchema.Integer,
        min: 0,
        // max
        custom() {
            if (this.field("budget").value < this.value)
                return SimpleSchema.ErrorTypes.MAX_NUMBER;
        },
    },
    minPoints: {
        type: SimpleSchema.Integer,
        min: 0
    },
    maxPoints: {
        type: SimpleSchema.Integer,
        // min: 0,
        custom() {
            if (this.field("minPoints").value > this.value)
                return SimpleSchema.ErrorTypes.MIN_NUMBER;

            return undefined;
        },
    },
    type: {
        type: String,
        allowedValues: ['item', 'video', 'survey',]
    },
    expire: {
        type: Date,
        min: function () {
            return new Date();
        }
    },
    status: {
        type: String,
        allowedValues: ['active', 'disabled']
    },

}, {requiredByDefault: true});*/


/**
 *
 * @param user: doc to validate
 * @param options: options to be passed to SimpleSchema.validate function
 */
Users.validateSchema = function (user, options = {}) {
    return user;
    let context = Users.schema.newContext();

    context.validate(user, options);
    if (!context.isValid()) {
        let error = context.validationErrors()[0];

        throw new Meteor.Error('schema', getErrorMessageFor(error, Users.getLabelFor));
    }

    return user;
}

/**
 *
 * @param user: doc to validate
 * @returns {*}
 */
Users.validateAccess = function (user) {
    /* безопасность */
    if (!Meteor.user().company)
        throw new Meteor.Error('permissions', 'Недостаточно прав');
    if (user.company != Meteor.user().company)
        throw new Meteor.Error('permissions', 'Недостаточно прав');
    if (Meteor.user().status == 'disabled') {
        throw new Meteor.Error('permissions', 'Недостаточно прав');
    }
    if (Meteor.user().users !== true)
        throw new Meteor.Error('permissions', 'Недостаточно прав');

    return user;
}

/**
 * performing cleanup based on SimpleSchema.clean() function.
 * @param doc: user document to cleanup
 * @param options
 */
Users.cleanup = function (doc, options = {}) {
    return doc;
    return Users.schema.clean(doc, options);
};

/*todo реализовать нормальную валидацию и т.д.*/
Users.getLabelFor = function (key) {
    switch (key) {
        case 'name':
            return 'Название';
        case 'company':
            return 'Компания';
        case 'minPoints':
            return 'Мин. кол-во очков';
        case 'maxPoints':
            return 'Макс. кол-во очков';
        case 'type':
            return 'Тип акции';
        case 'expire':
            return 'Дата окончания';
        case 'status':
            return 'Статус акции';
        default:
            return key;
    }
}


/**
 *
 * @param user: doc to validate
 // * @param trimStrings: if true, trims all strings before validating (to avoid '  '==true)
 * @param cleanup: if true, then performing Users.cleanup before validating.
 * @returns {{}}
 */
Users.validateInsert = function (user, {cleanup = false}) {
    if (cleanup) {
        user = Users.cleanup(user);
    }

    Users.validateAccess(user);

    Users.validateSchema(user);

    return user;
};

Users.validateUpdate = function (user, setFields, {cleanup = false}) {
    // чтобы полноценно все проверить (например, мы изменили max. Нужно чтобы он был >=min)
    for (let key of Object.getOwnPropertyNames(setFields)) {
        user[key] = setFields[key];
    }

    if (cleanup) {
        user = Users.cleanup(user);
    }

    Users.validateAccess(user);

    Users.validateSchema(user);

    return user;
};

Users.validateRemove = function (user) {
    Users.validateAccess(user);

    return user;
};

/**
 * register insert|update|delete in the system
 * @param userName: {string} 'insert'|'update'|'delete'|custom
 * @param user: user document
 */
Users.register = function (userName, user) {
    let timeline = Timeline.initDefaults();
    timeline.item = user._id;
    if (userName == 'removeUser') {
        timeline.revert = true;
        timeline.object = user;
    }
    timeline.user = userName;
    Timeline.insert(timeline);
};

Users.registerInsert = function (user) {
    Users.register('createNewUser', user);
};

Users.registerUpdate = function (user) {
    Users.register('updateUser', user);
};

Users.registerRemove = function (user) {
    Users.register('removeUser', user);
};
