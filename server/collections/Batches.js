import SimpleSchema from 'simpl-schema'
import {getErrorMessageFor} from "./commonFunctions";

Batches = new Mongo.Collection('batches');


Batches.initDefaults = function (doc = {}) {
    doc._id = doc._id || Random.id();
    doc._createdAt = doc._createdAt || Date.now();
    doc.company = doc.company || Meteor.user().company;
    doc.user = doc.user || Meteor.user()._id;
    doc.name = doc.name || 'Название товара не указано';

    doc.status = doc.status || 'active';

    return doc;
};


Batches.schema = new SimpleSchema({
    _id: {
        type: String,
        optional: true
    },
    _createdAt: {
        type: Date,
        optional: true
    },

    company: String,
    user: String,
    name: String,
    action: String,
    survey: {
        type: String,
        optional: true
    },
    item: {
        type: String,
        optional: true
    },

    amount: {
        type: SimpleSchema.Integer,
        min: 1
    },
    status: {
        type: String,
        allowedValues: ['active', 'disabled']
    },

}, {requiredByDefault: true});


/**
 *
 * @param batch: doc to validate
 * @param options: options to be passed to SimpleSchema.validate function
 */
Batches.validateSchema = function (batch, options = {}) {
    let context = Batches.schema.newContext();

    context.validate(batch, options);
    if (!context.isValid()) {
        let error = context.validationErrors()[0];

        throw new Meteor.Error('schema', getErrorMessageFor(error, Batches.getLabelFor));
    }

    return batch;
}

/**
 *
 * @param batch: doc to validate
 * @returns {*}
 */
Batches.validateAccess = function (batch) {
    /* безопасность */
    if (!Meteor.user().company)
        throw new Meteor.Error('permissions', 'Недостаточно прав');
    if (batch.company != Meteor.user().company)
        throw new Meteor.Error('permissions', 'Недостаточно прав');
    if (Meteor.user().status == 'disabled') {
        throw new Meteor.Error('permissions', 'Недостаточно прав');
    }
    if (Meteor.user().batches !== true)
        throw new Meteor.Error('permissions', 'Недостаточно прав');

    return batch;
}

/**
 * performing cleanup based on SimpleSchema.clean() function.
 * @param doc: batch document to cleanup
 * @param options
 */
Batches.cleanup = function (doc, options = {}) {
    return Batches.schema.clean(doc, options);
};

Batches.getLabelFor = function (key) {
    switch (key) {
        case 'name':
            return 'Название продукта';
        case 'company':
            return 'Компания';
        case 'user':
            return 'Пользователь';
        case 'batch':
            return 'Акция';
        case 'survey':
            return 'Опрос';
        case 'amount':
            return 'Кол-во';
        case 'status':
            return 'Статус';
        default:
            return key;


    }
}

/**
 *
 * @param batch: doc to validate
 // * @param trimStrings: if true, trims all strings before validating (to avoid '  '==true)
 * @param cleanup: if true, then performing Batches.cleanup before validating.
 * @returns {{}}
 */
Batches.validateInsert = function (batch, {cleanup = false}) {
    if (cleanup) {
        batch = Batches.cleanup(batch);
    }

    Batches.validateAccess(batch);

    Batches.validateSchema(batch);

    return batch;
};

Batches.validateUpdate = function (batch, setFields, {cleanup = false}) {
    // чтобы полноценно все проверить (например, мы изменили max. Нужно чтобы он был >=min)
    for (let key of Object.getOwnPropertyNames(setFields)) {
        batch[key] = setFields[key];
    }

    if (cleanup) {
        batch = Batches.cleanup(batch);
    }

    Batches.validateAccess(batch);

    Batches.validateSchema(batch);

    return batch;
};

Batches.validateRemove = function (batch) {
    Batches.validateAccess(batch);

    return batch;
};

/**
 * register insert|update|delete in the system
 * @param batchName: {string} 'insert'|'update'|'delete'|custom
 * @param batch: batch document
 */
Batches.register = function (batchName, batch) {
    let timeline = Timeline.initDefaults();
    timeline.item = batch._id;
    timeline.batch = batch;
    Timeline.insert(timeline);
};

Batches.registerInsert = function (batch) {
    Batches.register('createNewBatch', batch);
};

Batches.registerUpdate = function (batch) {
    Batches.register('updateBatch', batch);
};

Batches.registerRemove = function (batch) {
    Batches.register('removeBatch', batch);
};
