import SimpleSchema from 'simpl-schema'
import {getErrorMessageFor} from "./commonFunctions";

Contents = new Mongo.Collection('contents');

Contents.initDefaults = function (doc = {}) {
    doc._id = doc._id || Random.id();
    doc._createdAt = doc._createdAt || Date.now();
    doc.company = doc.company || Meteor.user().company; // impossible for server-initiated operations
    doc.images = doc.images || [];

    doc.title = doc.title || 'Новый контент';
    doc.status = doc.status || 'active';

    return doc;
};


Contents.schema = new SimpleSchema({
    _id: {
        type: String,
        optional: true
    },
    _createdAt: {
        type: Date,
        optional: true
    },
    title: {
        type: String,
    },
    text: {
        type: String,
        optional: true
    },
    survey: {
        type: String,
        optional: true
    },
    company: String,
    images: {
        type: Array,
        optional: true
    },
    'images.$': String,
    status: {
        type: String,
        allowedValues: ['active', 'disabled']
    },

}, {requiredByDefault: true});


/**
 *
 * @param content: doc to validate
 * @param options: options to be passed to SimpleSchema.validate function
 */
Contents.validateSchema = function (content, options = {}) {
    let context = Contents.schema.newContext();

    context.validate(content, options);
    if (!context.isValid()) {
        let error = context.validationErrors()[0];

        throw new Meteor.Error('schema', getErrorMessageFor(error, Contents.getLabelFor));
    }

    return content;
}

/**
 *
 * @param content: doc to validate
 * @returns {*}
 */
Contents.validateAccess = function (content) {
    /* безопасность */
    if (!Meteor.user().company)
        throw new Meteor.Error('permissions', 'Недостаточно прав');
    if (content.company != Meteor.user().company)
        throw new Meteor.Error('permissions', 'Недостаточно прав');
    if (Meteor.user().status == 'disabled') {
        throw new Meteor.Error('permissions', 'Недостаточно прав');
    }
    if (Meteor.user().contents !== true)
        throw new Meteor.Error('permissions', 'Недостаточно прав');

    return content;
}

/**
 * performing cleanup based on SimpleSchema.clean() function.
 * @param doc: content document to cleanup
 * @param options
 */
Contents.cleanup = function (doc, options = {}) {
    return Contents.schema.clean(doc, options);
};

Contents.getLabelFor = function (key) {
    switch (key) {
        case 'title':
            return 'Название';
        case 'description':
            return 'Описание';
        case 'company':
            return 'Компания';
        case 'images':
            return 'Галерея изображений';
        case 'videos':
            return 'Галерея видео';
        case 'status':
            return 'Статус контента';
        default:
            return key;
    }
}


/**
 *
 * @param content: doc to validate
 // * @param trimStrings: if true, trims all strings before validating (to avoid '  '==true)
 * @param cleanup: if true, then performing Contents.cleanup before validating.
 * @returns {{}}
 */
Contents.validateInsert = function (content, {cleanup = false}) {
    if (cleanup) {
        content = Contents.cleanup(content);
    }

    Contents.validateAccess(content);

    Contents.validateSchema(content);

    return content;
};

Contents.validateUpdate = function (content, setFields, {cleanup = false}) {
    // чтобы полноценно все проверить (например, мы изменили max. Нужно чтобы он был >=min)
    for (let key of Object.getOwnPropertyNames(setFields)) {
        content[key] = setFields[key];
    }

    if (cleanup) {
        content = Contents.cleanup(content);
    }

    Contents.validateAccess(content);

    Contents.validateSchema(content);

    return content;
};

Contents.validateRemove = function (content) {
    Contents.validateAccess(content);

    return content;
};

/**
 * register insert|update|delete in the system
 * @param actionName: {string} 'insert'|'update'|'delete'|custom
 * @param content: content document
 */
Contents.register = function (actionName, content) {
    let timeline = Timeline.initDefaults();
    timeline.item = content._id;
    timeline.content = content;
    Timeline.insert(timeline);
};

Contents.registerInsert = function (content) {
    Contents.register('createNewContent', content);
};

Contents.registerUpdate = function (content) {
    Contents.register('updateContent', content);
};

Contents.registerRemove = function (content) {
    Contents.register('removeContent', content);
};
