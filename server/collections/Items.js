import SimpleSchema from 'simpl-schema'
import {getErrorMessageFor} from "./commonFunctions";
Items = new Mongo.Collection('items');


Items.initDefaults = function (doc = {}) {
    doc._id = doc._id || Random.id();
    doc._createdAt = doc._createdAt || Date.now();
    doc.company = doc.company || Meteor.user().company;

    doc.active = doc.active || true;

    return doc;
};


Items.schema = new SimpleSchema({
    _id: {
        type: String,
        optional: true
    },
    _createdAt: {
        type: Date,
        optional: true
    },
    company: String,
    action: String,
    batch: String,
    barcode: String,
    active: {
        type: Boolean,
        optional: true
    },

}, {requiredByDefault: true});


/**
 *
 * @param item: doc to validate
 * @param options: options to be passed to SimpleSchema.validate function
 */
Items.validateSchema = function (item, options = {}) {
    let context = Items.schema.newContext();

    context.validate(item, options);
    if (!context.isValid()) {
        let error = context.validationErrors()[0];

        throw new Meteor.Error('schema', getErrorMessageFor(error, Items.getLabelFor));
    }

    return item;
};

/**
 *
 * @param item: doc to validate
 * @returns {*}
 */
Items.validateAccess = function (item) {
    /* безопасность */
    if (!Meteor.user().company)
        throw new Meteor.Error('permissions','Недостаточно прав');
    if (item.company != Meteor.user().company)
        throw new Meteor.Error('permissions','Недостаточно прав');
    if (Meteor.user().status == 'disabled') {
        throw new Meteor.Error('permissions','Недостаточно прав');
    }
    if (Meteor.user().batches !== true)
        throw new Meteor.Error('permissions','Недостаточно прав');

    return item;
};

/**
 * performing cleanup based on SimpleSchema.clean() function.
 * @param doc: item document to cleanup
 * @param options
 */
Items.cleanup = function (doc, options = {}) {
    return Items.schema.clean(doc, options);
};

Items.getLabelFor = function (key) {
    switch (key) {
        case 'company':
            return 'Компания';
        case 'action':
            return 'Акция';
        case 'batch':
            return 'Партия';
        case 'status':
            return 'Статус';
        default:
            return key;

    }
};

/**
 *
 * @param item: doc to validate
 // * @param trimStrings: if true, trims all strings before validating (to avoid '  '==true)
 * @param cleanup: if true, then performing Items.cleanup before validating.
 * @returns {{}}
 */
Items.validateInsert = function (item, {cleanup = false}) {
    if (cleanup) {
        item = Items.cleanup(item);
    }

    Items.validateAccess(item);

    Items.validateSchema(item);

    return item;
};

Items.validateUpdate = function (item, setFields, {cleanup = false}) {
    // чтобы полноценно все проверить (например, мы изменили max. Нужно чтобы он был >=min)
    for (let key of Object.getOwnPropertyNames(setFields)) {
        item[key] = setFields[key];
    }

    if (cleanup) {
        item = Items.cleanup(item);
    }

    Items.validateAccess(item);

    Items.validateSchema(item);

    return item;
};

Items.validateRemove = function (item) {
    Items.validateAccess(item);

    return item;
};

/**
 * register insert|update|delete in the system
 * @param itemName: {string} 'insert'|'update'|'delete'|custom
 * @param item: item document
 */
Items.register = function (itemName, item) {
    let timeline = Timeline.initDefaults();
    timeline.item = item._id;
    timeline.item = item;
    Timeline.insert(timeline);
};

Items.registerInsert = function (item) {
    Items.register('createNewItem', item);
};

Items.registerUpdate = function (item) {
    Items.register('updateItem', item);
};

Items.registerRemove = function (item) {
    Items.register('removeItem', item);
};
