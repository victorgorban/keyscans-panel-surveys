import SimpleSchema from 'simpl-schema'

 export function getErrorMessageFor(errorMessage, getLabelFor) {
    // console.log(SimpleSchema.ErrorTypes);
    // console.log(errorMessage);

    switch (errorMessage.type) {
        case SimpleSchema.ErrorTypes.REQUIRED:
            return `Необходимо указать ${getLabelFor(errorMessage.name)}`;
        case SimpleSchema.ErrorTypes.MIN_STRING:
            return `${getLabelFor(errorMessage.name)}: Строка должна быть длиннее ${errorMessage.min ? errorMessage.min : ""}`;
        case SimpleSchema.ErrorTypes.MAX_STRING:
            return `${getLabelFor(errorMessage.name)}: Строка должна быть короче ${errorMessage.max ? errorMessage.max : ""}`;
        case SimpleSchema.ErrorTypes.MIN_NUMBER:
        case SimpleSchema.ErrorTypes.MIN_NUMBER_EXCLUSIVE:
            return `${getLabelFor(errorMessage.name)}: Число должно быть больше ${errorMessage.min ? errorMessage.min : ""}`;
        case SimpleSchema.ErrorTypes.MAX_NUMBER:
        case SimpleSchema.ErrorTypes.MAX_NUMBER_EXCLUSIVE:
            return `${getLabelFor(errorMessage.name)}: Число должно быть меньше ${errorMessage.max ? errorMessage.max : ""}`;
        case SimpleSchema.ErrorTypes.MIN_DATE:
            return `${getLabelFor(errorMessage.name)}: Дата должна быть больше ${errorMessage.min ? errorMessage.min : ""}`;
        case SimpleSchema.ErrorTypes.MAX_DATE:
            return `${getLabelFor(errorMessage.name)}: Дата должна быть меньше ${errorMessage.max ? errorMessage.max : ""}`;
        case SimpleSchema.ErrorTypes.BAD_DATE:
            return `${getLabelFor(errorMessage.name)}: Некорректная дата`;
        case SimpleSchema.ErrorTypes.MIN_COUNT:
            return `${getLabelFor(errorMessage.name)}: Элементов должно быть больше`;
        case SimpleSchema.ErrorTypes.MAX_COUNT:
            return `${getLabelFor(errorMessage.name)}: Элементов должно быть меньше`;
        case SimpleSchema.ErrorTypes.MUST_BE_INTEGER:
            return `${getLabelFor(errorMessage.name)}: Ожидалось целое число, получено ${errorMessage.value ? errorMessage.value : ""}`;
        case SimpleSchema.ErrorTypes.VALUE_NOT_ALLOWED:
            return `${getLabelFor(errorMessage.name)}: Значение недопустимо`;
        case SimpleSchema.ErrorTypes.EXPECTED_TYPE:
            return `${getLabelFor(errorMessage.name)}: Ожидался другой тип`;
        case SimpleSchema.ErrorTypes.FAILED_REGULAR_EXPRESSION:
            return `${getLabelFor(errorMessage.name)}: Не соответствует шаблону`;
        case SimpleSchema.ErrorTypes.KEY_NOT_IN_SCHEMA:
            return `${getLabelFor(errorMessage.name)}: Поле не допустимо в схеме`;
        default:
            return `Неизвестная ошибка ${errorMessage.type} для ${getLabelFor(errorMessage.name)}`;
    }
}