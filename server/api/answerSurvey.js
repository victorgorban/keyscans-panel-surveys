import {random, showError, showSuccess, verifyUserIdAndToken} from './common'
import {Random} from 'meteor/random'
import SimpleSchema from 'simpl-schema'

let resultSchema = new SimpleSchema({
    _id: String, // _id опроса
    questions: Array,
    'questions.$': Object,
    'questions.$._id': String, // question _id
    'questions.$.answer': { // только для type != select (где просто ответ)
        type: SimpleSchema.oneOf(String, SimpleSchema.Integer),
        optional: true
    },
    'questions.$.answers': { // только для type=='select'
        type: Array,
        optional: true
    },
    'questions.$.answers.$': String, // _id выбранного вопроса
    'questions.$.customAnswer': { // только для type=='select' и customAnswer == true
        type: String, // custom ответ
        optional: true
    },
});

export function checkAnswersMatchQuestions(questions, answerQuestions) {
//    проверяю что в answers есть все required questions
    questions.forEach(function (question) {
        let answer = answerQuestions.find((a) => a._id == question._id);
        if (question.required) {
            if (!answer) {
                throw 'answer for required question not found';
            }
        }

    });
//    проверяю что нет лишних вопросов
    answerQuestions.forEach((answer) => {
        let question = questions.find((q) => q._id == answer._id);
        if (!question) {
            throw 'extra answer detected';
        }
    });

}

/**
 *
 * @param question - survey question to process
 * @param answeredQuestion - the corresponding question from answer
 * @param questionToSave - the initialized question to save.
 * @returns {*}
 */
function processQuestionOfTypeSelect(question, answeredQuestion, questionToSave) {
    // проверяем customAnswer. Если есть, добавляем его
    if (question.customAnswer) {
        questionToSave.customAnswer = answeredQuestion.customAnswer; // будет null или undefined - так и сохраним.
        let counter = question.customAnswered || 0;
        counter++;
        question.customAnswered = counter;
    }
    // выбранные юзером ответы
    let selected = answeredQuestion.answers;
    if (!selected) {
        throw 'expected answers array on question ' + question._id;
    }

    // если !question.multiple, то answer должен быть один. Проверяем это.
    if (!question.multiple) {
        if (selected.length > 1) {
            throw 'expected single answer';
        }
    }

    //Неполностью копируем исходные answers
    questionToSave.answers = question.answers.map(a => {
        return {
            _id: a._id,
            text: a.text
        }
    });

    // если answer выбран, делаем соотв. пометку
    questionToSave.answers.forEach(a => {
        if (selected.find(sel => sel == a._id)) {
            a.selected = true;
        }
    });

    // если answer выбран, увеличиваем счетчик answered в ответе исходного вопроса (который из survey)
    question.answers.forEach(a => {
        if (selected.find(sel => sel == a._id)) {
            let counter = a.answered || 0;
            counter++;
            a.answered = counter;
        }
    });


    return questionToSave;
}

function processQuestionOfTypeText(question, answeredQuestion, questionToSave) {
    if (question.required && !answeredQuestion.answer) {
        throw 'expected answer in question ' + question._id;
    }

    questionToSave.answer = answeredQuestion.answer;
    return questionToSave;
}

function processQuestionOfTypeDate(question, answeredQuestion, questionToSave) {
    if (question.required && !answeredQuestion.answer) {
        throw 'expected answer in question ' + question._id;
    }
    try {
        questionToSave.answer = new Date(answeredQuestion.answer).getTime();
    } catch (e) {
        throw 'incorrect date format. question: ' + question._id;
    }
    return questionToSave;
}

Meteor.method('answerSurvey', function (userId, token, passedSurvey, lat, lon) {
    let thisUser, errorMsg;
    let survey;
    /*Проверки*/
    {
        if (!userId || !token || !passedSurvey) {
            return showError('please set all params');
        }

        if (!lat || !lon) {
            return showError('please specify your location');
        }

        [thisUser, token, errorMsg] = verifyUserIdAndToken(userId, token);
        if (errorMsg) {
            return showError(errorMsg);
        }

        try {
            passedSurvey = JSON.parse(passedSurvey);
        } catch (e) {
            return showError('cannot parse JSON. Ensure that you\'re passing valid json string.');
        }

        try {
            // validate schema
            passedSurvey = resultSchema.clean(passedSurvey, {
                filter: true,
                autoConvert: true,
                removeEmptyStrings: false,
                trimStrings: true,
                getAutoValues: true,
                removeNullsFromArrays: false,
            });
            resultSchema.validate(passedSurvey);
        } catch (e) {
            return showError('check the input format, schema is incorrect. error: ' + e);
        }

        survey = Surveys.findOne(passedSurvey._id);
        if (!survey) {
            return showError('survey not found');
        }
        if (survey.test) {
            return showError('this is a test, not a survey');
        }
        if (survey.status!='active') {
            return showError('Survey is not active');
        }

        if (Enrollments.findOne({
            company: survey.company, // points vendor
            user: thisUser._id,
            reason: 'survey',
            item: survey._id
        })) {
            return showError('survey already answered');
        }

        // Проверка что ответы совпадают со структурой вопросов
        try {
            checkAnswersMatchQuestions(survey.questions, passedSurvey.questions);
        } catch (e) {
            return showError('answers don\'t match questions structure. error: ' + e);
        }
    }

    try {
        /*Уже убедились, что лишних вопросов нет, и получены ответы на все required.*/
        /*result: survey, questions [{title, answers|answer, custom,}]*/
        let resultToSave = {
            _id: Random.id(),
            _createdAt: new Date(),
            survey: survey._id,
            title: survey.title,
            test: false,
            user: thisUser._id,
            company: survey.company,

            questions: []
        };

        let counter = survey.answered || 0;
        counter++;
        survey.answered = counter;

        survey.questions.forEach(question => {
            let answeredQuestion = passedSurvey.questions.find(a => a._id == question._id);
            let questionToSave = {
                _id: question._id,
                title: question.title,
                type: question.type
            };

            if (!answeredQuestion) {
                // questionToSave.notAnswered = true;
                resultToSave.questions.push(questionToSave);
                return;
            } else {
                let counter = question.answered || 0;
                counter++;
                question.answered = counter;
            }

            switch (question.type) {
                case 'select':
                    questionToSave = processQuestionOfTypeSelect(question, answeredQuestion, questionToSave);
                    break;
                case 'text':
                    questionToSave = processQuestionOfTypeText(question, answeredQuestion, questionToSave);
                    break;
                case 'date':
                    questionToSave = processQuestionOfTypeDate(question, answeredQuestion, questionToSave);
                    break;
            }

            resultToSave.questions.push(questionToSave);
        });

        Results.insert({
            ...resultToSave,
            user: thisUser._id,
            lat,
            lon
        });

        //todo нужны counters, т.к. нужна какая-то статистика.
        //counters для каждого ответа (selected|answered: n) и для всего опроса (answered: n)
        //пока что добавляю counters, а там посмотрим. Просмотр result'а тоже надо сделать, хоть на потом будет.
        // но ведь опрос же может измениться? Тогда единственным логичным (и честным) вариантом будет запрещать редактирование опросов.

        let points = 0;
        var action = Actions.findOne(survey.action);
        let isAction = !!action;
        // если опрос еще и акционный, то добавить акционные очки
        if (action) {
            let actionPoints = 0;
            actionPoints = random(action.maxPoints - action.minPoints) + (action.minPoints);
            if (action.current < points) {
                actionPoints -= action.current;
            }
            points += actionPoints;
            Actions.update(action._id, {$inc: {current: -points}});
        }

        points += 90;
        Meteor.users.update({_id: thisUser._id}, {$inc: {points}});
        Enrollments.insert({
            _id: Random.id(),
            _createdAt: new Date(),
            company: resultToSave.company, // points vendor
            user: thisUser._id,
            points,
            reason: 'survey',
            action: action._id,
            item: survey._id,
            result: resultToSave._id,
        });

        
        Surveys.update({_id: survey._id}, survey);

        return showSuccess({
            points,
            action: isAction
        });

    } catch (e) {
        return showError('error: ' + (e.message ? e.message : e));
    }

}, {
    url: '/api/answerSurvey',
    httpMethod: 'post',
    getArgsFromRequest: function (request) {
        return [
            request.headers['x-user-id'],
            request.headers['x-auth-token'],
            request.body.result,
            request.body.latitude,
            request.body.longitude,
        ];
    },
});

Meteor.method('answerSurveyAnonim', function (passedSurvey, lat, lon) {
    let survey;
    let thisUser = {};
    /*Проверки*/
    {
        if (!passedSurvey) {
            return showError('please set all params');
        }

        if (!lat || !lon) {
            return showError('please specify your location');
        }

        try {
            passedSurvey = JSON.parse(passedSurvey);
        } catch (e) {
            return showError('cannot parse JSON. Ensure that you\'re passing valid json string.');
        }

        try {
            // validate schema
            passedSurvey = resultSchema.clean(passedSurvey, {
                filter: true,
                autoConvert: true,
                removeEmptyStrings: false,
                trimStrings: true,
                getAutoValues: true,
                removeNullsFromArrays: false,
            });
            resultSchema.validate(passedSurvey);
        } catch (e) {
            return showError('check the input format, schema is incorrect. error: ' + e);
        }

        survey = Surveys.findOne(passedSurvey._id);
        if (!survey) {
            return showError('survey not found');
        }
        if (survey.test) {
            return showError('this is a test, not a survey');
        }
        if (survey.status!='active') {
            return showError('Survey is not active');
        }

        // Проверка что ответы совпадают со структурой вопросов
        try {
            checkAnswersMatchQuestions(survey.questions, passedSurvey.questions);
        } catch (e) {
            return showError('answers don\'t match questions structure. error: ' + e);
        }
    }

    try {
        /*Уже убедились, что лишних вопросов нет, и получены ответы на все required.*/
        /*result: survey, questions [{title, answers|answer, custom,}]*/
        let resultToSave = {
            _id: Random.id(),
            _createdAt: new Date(),
            survey: survey._id,
            title: survey.title,
            test: false,
            user: thisUser._id,
            company: survey.company,

            questions: []
        };

        let counter = survey.answered || 0;
        counter++;
        survey.answered = counter;

        survey.questions.forEach(question => {
            let answeredQuestion = passedSurvey.questions.find(a => a._id == question._id);
            let questionToSave = {
                _id: question._id,
                title: question.title,
                type: question.type
            };

            if (!answeredQuestion) {
                // questionToSave.notAnswered = true;
                resultToSave.questions.push(questionToSave);
                return;
            } else {
                if (answeredQuestion.answer || (answeredQuestion.answers && answeredQuestion.answers.length)) {
                    let counter = question.answered || 0;
                    counter++;
                    question.answered = counter;
                }
            }

            switch (question.type) {
                case 'select':
                    questionToSave = processQuestionOfTypeSelect(question, answeredQuestion, questionToSave);
                    break;
                case 'text':
                    questionToSave = processQuestionOfTypeText(question, answeredQuestion, questionToSave);
                    break;
                case 'date':
                    questionToSave = processQuestionOfTypeDate(question, answeredQuestion, questionToSave);
                    break;
            }

            resultToSave.questions.push(questionToSave);
        });

        Results.insert({
            ...resultToSave,
            user: thisUser._id,
            latitude: lat,
            longitude: lon
        });

        
        Surveys.update({_id: survey._id}, survey);

        return showSuccess({});

    } catch (e) {
        return showError('error: ' + (e.message ? e.message : e));
    }

}, {
    url: '/api/answerSurveyAnonim',
    httpMethod: 'post',
    getArgsFromRequest: function (request) {
        return [
            request.body.result,
            request.body.latitude,
            request.body.longitude,
        ];
    },
});
