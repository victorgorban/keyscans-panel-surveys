import _ from 'lodash'
import {showError} from "../common";

SSR.compileTemplate('renderAnswerSurvey', Assets.getText('renderAnswerSurvey.html'));
SSR.compileTemplate('renderAnswerTest', Assets.getText('renderAnswerTest.html'));
SSR.compileTemplate('apiResult', Assets.getText('apiResult.html'));

Template.apiResult.helpers({
    text() {
        return new Template.instance().data.text;
    },
    type() {
        return new Template.instance().data.type;
    },
});


let helpers = {
    linkx: function (id) {
        if (Images.findOne(id)) {
            return Images.findOne(id).link('thumbnail');
        }
    },
    survey() {
        return Template.instance().data.survey;
    },
    russianPoints(points) {
        let start = (points || 0) + ' ';
        switch (_.last(points + '')) {
            case '0':
                return start + 'баллов'
            case '1':
                return start + 'балл'
            case '2':
            case '3':
                return start + 'балла'
            default:
                return start + 'баллов'
        }
    },
    notEmpty(...values) {
        /*если все пустые, возвращаем false*/
        let count = values.length - 1; // последний параметр - это "spacebars kw"
        for (let value of values) {
            if (typeof value == 'string') {
                value = value.trim();
            }
            if (typeof value == 'number') // потому что 0 - это не пусто.
            {
                continue;
            }
            if (!value) {
                count--;
            }
        }
        if (!count) // все пустые
        {
            return false;
        }
        return true;

    },

    equal(value1, value2) {
        return value1 == value2;
    },

    checked(value) {
        return value ? 'checked' : '';
    },
    requiredChecked(question) {
        return question.required === true ? 'checked' : "";
    },
    isMultiplePossible(question) {
        return question.type == 'select';
    },
    multipleChecked(question) {
        return question.multiple === true ? 'checked' : "";
    },
    isRequired: function (question) {
        if (question.required) {
            return 'true';
        } else {
            return 'false';
        }
    },
    isMultiple: function (question) {
        if (question.multiple) {
            return 'true';
        } else {
            return 'false';
        }
    },
    isCustomAnswer: function (question) {
        if (question.customAnswer) {
            return 'true';
        } else {
            return 'false';
        }
    },
    radioOrCheckbox(question) {
        return question.multiple ? "checkbox" : 'radio';
    },
    questionType: function (question) {
        return question.type;
    },
};

Template.renderAnswerSurvey.helpers(helpers);
Template.renderAnswerTest.helpers(helpers);

WebApp.connectHandlers.use('/answerSurvey/', (req, res, next) => {
    var body = "";

    req.on('data', Meteor.bindEnvironment(function (data) {
        body += data;
    }));

    var parts = req.url.split("/");
    var id = parts[1];

    var survey = Surveys.findOne(id);

    if (!survey) {
        res.writeHead(404);
        res.end(SSR.render('apiResult', {
            type: 'error',
            text: 'Опрос не найден'
        }));
        return;
    }
    if (survey.test) {
        res.writeHead(404);
        res.end(SSR.render('apiResult', {
            type: 'error',
            text: 'Это не опрос'
        }));
        return;
    }
    if (survey.status!='active') {
        res.writeHead(404);
        res.end(SSR.render('apiResult', {
            type: 'error',
            text: 'Опрос неактивен'
        }));
        return;
    }

    res.end(SSR.render('renderAnswerSurvey', {
        survey: survey
    }));
});

WebApp.connectHandlers.use('/answerTest/', (req, res, next) => {
    var body = "";

    req.on('data', Meteor.bindEnvironment(function (data) {
        body += data;
    }));

    var parts = req.url.split("/");
    var id = parts[1];

    var test = Surveys.findOne(id);

    if (!test) {
        res.writeHead(404);
        res.end(SSR.render('apiResult', {
            type: 'error',
            text: 'Тест не найден'
        }));
        return;
    }
    if (!test.test) {
        res.writeHead(404);
        res.end(SSR.render('apiResult', {
            type: 'error',
            text: 'Это не тест'
        }));
        return;
    }
    if (test.status!='active') {
        res.writeHead(404);
        res.end(SSR.render('apiResult', {
            type: 'error',
            text: 'Тест неактивен'
        }));
        return;
    }

    res.end(SSR.render('renderAnswerTest', {
        survey: test
    }));
});
