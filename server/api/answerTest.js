import {random, showError, showSuccess, verifyUserIdAndToken} from './common'
import {Random} from 'meteor/random'
import SimpleSchema from 'simpl-schema'

let resultSchema = new SimpleSchema({
    _id: String, //_id опроса
    questions: Array,
    'questions.$': Object,
    'questions.$._id': String, // question _id
    'questions.$.answers': { // только для type=='select'
        type: Array,
        optional: true
    },
    'questions.$.answers.$': String, // _id выбранного вопроса
}); // в тестах - только select. Никаких customAnswer, никаких text и date.

export function checkAnswersMatchQuestions(questions, answerQuestions) {
//    проверяю что в answers есть все required questions
    questions.forEach(function (question) {
        let answer = answerQuestions.find((a) => a._id == question._id);
        if (question.required) {
            if (!answer) {
                throw 'answer for required question not found';
            }
        }

    });
//    проверяю что нет лишних вопросов
    answerQuestions.forEach((answer) => {
        let question = questions.find((q) => q._id == answer._id);
        if (!question) {
            throw 'extra answer detected';
        }
    });

}

/**
 *
 * @param question - survey question to process
 * @param answeredQuestion - the corresponding question from answer
 * @param questionToSave - the initialized question to save.
 * @returns {*}
 */
function processQuestionOfTypeSelect(question, answeredQuestion, questionToSave) {
    let points = 0;
    // проверяем customAnswer. Если есть, добавляем его
    if (question.customAnswer) {
        questionToSave.customAnswer = answeredQuestion.customAnswer; // будет null или undefined - так и сохраним.
    }
    //    ожидается answers
    let selected = answeredQuestion.answers;
    if (!selected) {
        throw 'expected answers array on question ' + question._id;
    }

    // если !question.multiple, то answer должен быть один. Проверяем это.
    if (!question.multiple) {
        if (selected.length > 1) {
            throw 'expected single answer';
        }
    }

    //Неполностью копируем исходные answers
    questionToSave.answers = question.answers.map(a => {
        return {
            _id: a._id,
            text: a.text,
            correct: a.correct,
        }
    });

    // если answer выбран, делаем соотв. пометку
    questionToSave.answers.forEach(a => {
        if (selected.find(sel => sel == a._id)) {
            a.selected = true;
        }
    });

    let isCorrect = true;
    question.answers.forEach(a => {
        let selectedOne = selected.find(id => id == a._id);
        if (question.multiple) {
            if (a.correct) // для каждого верного ответа проверяем, что юзер выбрал его.
            {
                if (!selectedOne) {
                    isCorrect = false;
                }
            }
            if (!a.correct) // для каждого не верного ответа проверяем, что юзер не выбрал его.
            {
                if (selectedOne) {
                    isCorrect = false;
                }
            }
        } else { // single
            // нужно проверить, верно ли ответил человек: его ответ - correct
            if (selectedOne) // если человек выбрал не верный ответ, то это !isCorrect.
            {
                if (!a.correct) {
                    isCorrect = false;
                }
            }
        }
    });

    // если answer выбран, увеличиваем счетчик answered в ответе исходного вопроса (который из survey)
    question.answers.forEach(a => {
        if (selected.find(sel => sel == a._id)) {
            let counter = a.answered || 0;
            counter++;
            a.answered = counter;
        }
    });


    return [questionToSave, isCorrect];
}

Meteor.method('answerTest', function (userId, token, passedTest) {
    let thisUser, errorMsg;
    let test;
    /*Проверки*/
    {
        if (!userId || !token || !passedTest) {
            return showError('please set all params');
        }

        [thisUser, token, errorMsg] = verifyUserIdAndToken(userId, token);
        if (errorMsg) {
            return showError(errorMsg);
        }

        try {
            passedTest = JSON.parse(passedTest);
        } catch (e) {
            return showError('cannot parse JSON. Ensure that you\'re passing valid json string.');
        }

        try {
            // validate schema
            passedTest = resultSchema.clean(passedTest, {
                filter: true,
                autoConvert: true,
                removeEmptyStrings: false,
                trimStrings: true,
                getAutoValues: true,
                removeNullsFromArrays: false,
            });
            resultSchema.validate(passedTest);
        } catch (e) {
            return showError('check the input format, schema is incorrect. error: ' + e);
        }

        test = Surveys.findOne(passedTest._id);
        if (!test) {
            return showError('test not found');
        }
        if (!test.test) {
            return showError('this is not a test');
        }
        if (test.status != 'active') {
            return showError('Test is not active');
        }

        if (Enrollments.findOne({
            company: test.company, // points vendor
            user: thisUser._id,
            reason: 'test',
            item: test._id
        })) {
            return showError('test already passed');
        }

        // Проверка что ответы совпадают со структурой вопросов
        try {
            checkAnswersMatchQuestions(test.questions, passedTest.questions);
        } catch (e) {
            return showError('answers don\'t match questions structure. error: ' + e);
        }
    }

    try {
        /*Уже убедились, что лишних вопросов нет, и получены ответы на все required.*/
        /*result: survey, questions [{title, answers|answer, custom,}]*/
        let resultToSave = {
            _id: Random.id(),
            _createdAt: new Date(),
            test: true,
            survey: test._id,
            title: test.title,
            user: thisUser._id,
            company: test.company,
            questions: [],

            correct: 0,
            totalPoints: 0
        };

        let counter = test.answered || 0;
        let totalPoints = 0; // очки за тест
        test.totalPoints = calcTotalPoints(test);
        counter++;
        test.answered = counter;

        test.questions.forEach(question => {
            let answeredQuestion = passedTest.questions.find(a => a._id == question._id);
            let questionToSave = {
                _id: question._id,
                title: question.title,
                type: question.type
            };

            if (!answeredQuestion) {
                questionToSave.notAnswered = true;
                resultToSave.questions.push(questionToSave);
                return;
            }

            let counter = question.answered || 0;
            counter++;
            question.answered = counter;

            if (question.type === 'select') {
                let isCorrect;
                [questionToSave, isCorrect] = processQuestionOfTypeSelect(question, answeredQuestion, questionToSave);
                if (isCorrect) {
                    resultToSave.correct += 1;
                    totalPoints += question.points;
                    resultToSave.totalPoints += question.points;
                }
            } else {
                // такого не должно быть
                console.log('test: detected question of type !select');
                throw 'test: detected question of type !select';
            }

            resultToSave.questions.push(questionToSave);
        });

        Results.insert(resultToSave);

        let points = resultToSave.totalPoints / test.totalPoints * 20;
        var action = Actions.findOne(test.action);
        let isAction = !!action;
        // если опрос еще и акционный, то добавить акционные очки
        if (action) {
            let actionPoints = 0;
            actionPoints = random(action.maxPoints - action.minPoints) + (action.minPoints);
            if (action.current < points) {
                actionPoints -= action.current;
            }
            points += actionPoints;
            Actions.update(action._id, {$inc: {current: -points}});
        }

        points += 180 * (resultToSave.correct / resultToSave.questions.length);
        Meteor.users.update({_id: thisUser._id}, {$inc: {points}});

        Enrollments.insert({
            _id: Random.id(),
            _createdAt: new Date(),
            company: resultToSave.company, // points vendor
            user: thisUser._id,
            points,
            reason: 'test',
            action: action._id,
            item: test._id,
            result: resultToSave._id,
        });

        
        Surveys.update({_id: test._id}, test);

        return showSuccess({
            points,
            action: isAction,
            correctPercent: (resultToSave.correct / resultToSave.questions.length) * 100
        });

    } catch (e) {
        return showError('error: ' + (e.message ? e.message : e));
    }


}, {
    url: '/api/answerTest',
    httpMethod: 'post',
    getArgsFromRequest: function (request) {
        return [
            request.headers['x-user-id'],
            request.headers['x-auth-token'],
            request.body.result,
        ];
    },
});

Meteor.method('answerTestAnonim', function (passedTest, lat, lon) {
    let thisUser = {};
    let test;
    /*Проверки*/
    {
        try {
            passedTest = JSON.parse(passedTest);
        } catch (e) {
            return showError('cannot parse JSON. Ensure that you\'re passing valid json string.');
        }

        if (!lat || !lon) {
            return showError('please specify your location');
        }

        try {
            // validate schema
            passedTest = resultSchema.clean(passedTest, {
                filter: true,
                autoConvert: true,
                removeEmptyStrings: false,
                trimStrings: true,
                getAutoValues: true,
                removeNullsFromArrays: false,
            });
            resultSchema.validate(passedTest);
        } catch (e) {
            return showError('check the input format, schema is incorrect. error: ' + e);
        }

        test = Surveys.findOne(passedTest._id);
        if (!test) {
            return showError('test not found');
        }
        if (!test.test) {
            return showError('this is a survey, not a test');
        }
        if (test.status != 'active') {
            return showError('Test is not active');
        }

        // Проверка что ответы совпадают со структурой вопросов
        try {
            checkAnswersMatchQuestions(test.questions, passedTest.questions);
        } catch (e) {
            return showError('answers don\'t match questions structure. error: ' + e);
        }
    }

    try {
        /*Уже убедились, что лишних вопросов нет, и получены ответы на все required.*/
        /*result: survey, questions [{title, answers|answer, custom,}]*/
        let resultToSave = {
            _id: Random.id(),
            _createdAt: new Date(),
            test: true,
            survey: test._id,
            title: test.title,
            user: thisUser._id,
            company: test.company,
            questions: [],

            correct: 0,
            totalPoints: 0
        };

        let counter = test.answered || 0;
        let totalPoints = 0; // очки за тест
        counter++;
        test.answered = counter;
        test.totalPoints = calcTotalPoints(test);

        test.questions.forEach(question => {
            let answeredQuestion = passedTest.questions.find(a => a._id == question._id);
            let questionToSave = {
                _id: question._id,
                title: question.title,
                type: question.type
            };

            if (!answeredQuestion) {
                questionToSave.notAnswered = true;
                resultToSave.questions.push(questionToSave);
                return;
            }

            let counter = question.answered || 0;
            counter++;
            question.answered = counter;

            if (question.type === 'select') {
                let isCorrect;
                [questionToSave, isCorrect] = processQuestionOfTypeSelect(question, answeredQuestion, questionToSave);
                if (isCorrect) {
                    resultToSave.correct += 1;
                    totalPoints += question.points;
                    resultToSave.totalPoints += question.points;
                }
            } else {
                // такого не должно быть
                console.log('test: detected question of type !select');
                throw 'test: detected question of type !select';
            }

            resultToSave.questions.push(questionToSave);
        });

        Results.insert({
            ...resultToSave,
            user: thisUser._id,
            latitude: lat,
            longitude: lon
        });

        let points = resultToSave.totalPoints / test.totalPoints * 20;

        Meteor.users.update({_id: thisUser._id}, {$inc: {points}});

        
        Surveys.update({_id: test._id}, test);

        return showSuccess({
            points,
            correctPercent: (resultToSave.correct / resultToSave.questions.length) * 100,
            totalPoints: resultToSave.totalPoints + ' из ' + (test.totalPoints || 0),
        });

    } catch (e) {
        return showError('error: ' + (e.message ? e.message : e));
    }


}, {
    url: '/api/answerTestAnonim',
    httpMethod: 'post',
    getArgsFromRequest: function (request) {
        return [
            request.body.result,
            request.body.latitude,
            request.body.longitude,
        ];
    },
});


function calcTotalPoints(test) {
    let total = 0;

    for (let q of test.questions) {
        total += q.points;
    }
    return total;
}