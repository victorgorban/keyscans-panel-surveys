export {Random} from 'meteor/random'

const fs = require('fs');

export var random = function (n) {
    return Math.floor(Math.random() * n);
};


export function showSuccess(data, message) {
    return {
        status: 'success',
        message: message ? message : '',
        data: data ? data : {},
    }
}

export function showError(message) {
    return {
        status: 'error',
        message: message ? message : '',
    }
}

/**
 * действия с акциями при сканировании товара и в принципе зачислением балло
 * @param thisUser - User object
 * @param item - Item object
 */
export function productScanAction(thisUser, item) {
    try {
        // return [,,'Come on!'];

        var item = Items.findOne(item);
        if (!item) {
            return [, , 'invalid product scan'];
        }

        // return [,,'Come on2!'];

        // if (product.action) {
        //
        //   // console.log('product.action');
        //   var action = Actions.findOne(product.action);
        //   if (thisUser.subscribes.includes(action.company)) {
        //
        //     // console.log('subscribes.includes');
        //     var act = action.action;
        //     var earnedPoints;
        //     if (act == 'add') {
        //       earnedPoints = product.rewardPoints + action.points;
        //     }
        //     if (act == 'mult') {
        //       earnedPoints = product.rewardPoints * action.points;
        //     }
        //     if (action.budget >= earnedPoints - product.rewardPoints) {
        //       // console.log('action.budget >= earnedPoints - product.rewardPoints');
        //
        //       Actions.update(action._id, {$inc: {budget: -(earnedPoints - product.rewardPoints)}})
        //       Meteor.users.update(thisUser._id, {$inc: {points: earnedPoints}});
        //       Enrollments.insert({
        //                            _id: Random.id(),
        //                            _createdAt: new Date(),
        //                            vendor: action.company,
        //                            user: thisUser._id,
        //                            points: earnedPoints,
        //                            reason: 'акционный товар',
        //                            item: product._id,
        //                          });
        //     }
        //   }
        // }
        return [thisUser, item];
    } catch (e) {
        return [, , e.message];
    }

}


//  DONE: ASYNC/AWAIT!!! Meteor.call вызывается асинхронно, но типа синхронно. Достаточно try-catch См. в доках.


/**
 * Decode base64 image
 *.e.g. data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAAAPAQMAAABeJUoFAAAABlBMVEX///8AAABVwtN+AAAAW0lEQVQImY2OMQrAMAwDjemgJ3jI0CFDntDBGKN3hby9bWi2UqrtkJAk8k/m5g4vGBCprKRxtzQR3mrMlm2CKpjIK0ZnKYiOjuUooS9ALpjV2AjiGY3Dw+Pj2gmnNxItbJdtpAAAAABJRU5ErkJggg==
 */
export function decodeBase64(dataString) {
    var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
        response = {};

    if (matches.length !== 3) {
        return new Error('Invalid input string');
    }

    response.type = matches[1];
    response.data = new Buffer(matches[2], 'base64');

    return response;
}


export function verifyUserIdAndToken(userId, token) {
    try {

        var thisUser = Meteor.users.findOne(userId);

        if (!thisUser) {
            return [thisUser, token, 'user not exist'];
        }

        if (thisUser.status != 'active') {
            return [thisUser, token, 'user archived'];
        }

        if (!thisUser.services.resume) {
            thisUser.services.resume = {};
        }

        if (!thisUser.services.resume.loginTokens) {
            thisUser.services.resume.loginTokens = [];
        }
        var tokenId = thisUser.services.resume.loginTokens.findIndex((elem) => elem.hashedToken == token);

        if (tokenId < 0) {
            return [thisUser, token, 'You are logged out'];
        }
    } catch (e) {
        return showError(`an error occurred: ${e.message}`)
    }


    return [thisUser, token]
}

export function isEmail(str) {
    var pattern = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    return str.match(pattern);

}

export function getUserProfile(thisUser) {
    var userToReturn = {};
    userToReturn.name = thisUser.name;
    userToReturn.surname = thisUser.surname;
    userToReturn.age = thisUser.age;
    userToReturn.position = thisUser.position;
    userToReturn.sex = thisUser.sex;
    userToReturn.address = thisUser.address;
    userToReturn.jobPlace = thisUser.jobPlace;
    userToReturn.education = thisUser.education;

    var imgId = thisUser.img;
    var imgUrl = '';
    try {

        var media = Images.findOne(imgId);
        if (media) {
            imgUrl = media.link('thumbnail');
        }
        userToReturn.img = imgUrl;

        /*var company = Companys.findOne(thisUser.company);
        if (company) {
          userToReturn.company = company.name;
        } else {
          userToReturn.company = '';
        }*/
        userToReturn.points = thisUser.points;
    } catch (e) {
        return showError('an error occurred: ' + e.message);
    }

    return userToReturn;
}