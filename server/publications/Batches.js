publishComposite('company.batches', function (limit = 40) {
    if (!this.userId) {
        return this.ready();
    } else
        return {
            find() {
                return Batches.find({company: Meteor.user().company}, {
                    sort: {_createdAt: -1},
                    limit: limit
                });
            },
            children: [
                {
                    find(x) {
                        return Actions.find(x.action, {fields: {name: 1}});
                    }
                },
                {
                    find(x) {
                        return Surveys.find(x.survey, {fields: {title: 1}});
                    }
                },
                {
                    find(x) {
                        return Meteor.users.find({_id: x.user}, {
                            fields: {
                                name: 1,
                                name2: 1
                            }
                        });
                    }
                },

            ]
        }
});


publishComposite('batch.one', function (id) {
    if (!this.userId) {
        return this.ready();
    }
    return {
        find() {
            return Batches.find({
                _id: id,
                company: Meteor.user().company
            });
        },
        children: [
            {
                find(x) {
                    return Items.find({batch: x._id});
                },
                children: [
                    {
                        find(x) {
                            return Images.find({_id: x.photo}).cursor;
                        },
                    }
                ]
            },
            {
                find(x) {
                    return Actions.find(x.action, {fields: {name: 1}});
                }
            },
            /*
            {
                find(x) {
                    return Surveys.find(x.survey, {fields: {title: 1}});
                }
            },*/
        ]
    }
});
