publishComposite('survey.stats', function (id) {
    return {
        find() {
            return Surveys.find({
                _id: id, // для теста тоже ок
                company: Meteor.user().company
            });
        },
        children: [
            {
                find(x){
                    return Images.find(x.img).cursor;
                }
            },
            {
                find(x){
                    return Results.find({survey: x._id}, {fields: {questions: 0}});
                }
            },
        ],
    }
});