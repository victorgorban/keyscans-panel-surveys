publishComposite('company.videos', function (limit) {
    if (!this.userId) {
        return this.ready();
    } else
        return {
            find() {
                return Videos.find({'meta.company': Meteor.user().company}, {
                    sort: {_createdAt: -1},
                    limit: limit
                }).cursor;
            },
            children: [
                {
                    find(x) {
                        return Actions.find(x.meta.action, {fields: {name: 1}});
                    }
                },
                {
                    find(x) {
                        return Surveys.find(x.meta.survey, {fields: {title: 1}});
                    }
                },
                {
                    find(x) {
                        return Meteor.users.find({_id: x.meta.user}, {
                            fields: {
                                name: 1,
                                name2: 1
                            }
                        });
                    }
                }
            ]
        }
});


Meteor.publish('video.one', function (id) {
    return Videos.find(id).cursor;
});