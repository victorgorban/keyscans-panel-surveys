publishComposite('company.contents', function () {
    return {
        find() {
            return Contents.find({company: Meteor.user().company})
        },
        children: [
            {
                find(x) {
                    return Surveys.find(x.survey);
                }
            },
        ]
    };
});

publishComposite('content.one', function (id) {
    return {
        find() {
            return Contents.find({
                _id: id,
                company: Meteor.user().company
            });
        },
        children: [
            {
                find(x) {
                    let images = Images.find({_id: {$in: x.images}}).cursor;
                    return images;
                    // return Images.find({ _id: { $in: x.images}}).cursor;
                }
            },
        ],
    }
})
;


