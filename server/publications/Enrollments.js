

publishComposite('company.enrollments', function ({user: userId, action: actionId}) { // это декомпозиция, если что. Игнорирую все кроме user и action
  return {
    find() {
      let options = {company: Meteor.user().company};
      // эти грубые проверки нужны, т.к. при userId==undefined Enrollments.find().count()==0
      if (userId){
        options.user = userId;
      }
      if (actionId){
        options.action = actionId;
      }
      console.log(Enrollments.find(options).count());

      return Enrollments.find(options);
    },
    children: [
      {
        find(x) { // search item
          // console.log('action.item: '+action.item);
          return Meteor.users.find(x.user);
        },
      },
      {
        find(x) { // search item
          // console.log('action.item: '+action.item);
          return Actions.find({_id: x.action, company: Meteor.user().company});
        },
      },
    ],
  }
});
