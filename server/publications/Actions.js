Meteor.publish('company.actions', function () {
  return Actions.find({company: Meteor.user().company});
});


Meteor.publish('actions.type', function (type) {
  return Actions.find({
    company: Meteor.user().company,
    type: type
  });
});

publishComposite('action.one', function (id) {
    return {
        find() {
            return Actions.find({_id: id});
        },
        children: [
        ],
    }
})
;


