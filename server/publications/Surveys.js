publishComposite('survey.one', function (id) {
    return {
        find() {
            return Surveys.find({
                _id: id,
                test: {$ne: true},
                company: Meteor.user().company
            });
        },
        children: [
            {
                find(x){
                    return Images.find(x.img).cursor;
                }
            }
        ],
    }
});

publishComposite('company.surveys', function () {
    return {
        find() {
            return Surveys.find({
                test: {$ne: true},
                company: Meteor.user().company
            });
        },
        children: [
            {
                find(x) {
                    return Actions.find(x.action, {fields: {name: 1}});
                },
            }
        ],
    }
});

publishComposite('company.surveysTitles', function () {
    return {
        find() {
            return Surveys.find({
                test: {$ne: true},
                company: Meteor.user().company
            }, {fields: {title: 1}});
        },
        children: [],
    }
});

publishComposite('test.one', function (id) {
    return {
        find() {
            return Surveys.find({
                _id: id,
                test: true,
                company: Meteor.user().company
            });
        },
        children: [
            {
                find(x){
                    return Images.find(x.img).cursor;
                }
            }
        ],
    }
});

publishComposite('company._tests', function () {
    return {
        find() {
            return Surveys.find({
                test: true,
                company: Meteor.user().company
            });
        },
        children: [
            {
                find(x) {
                    return Actions.find(x.action, {fields: {name: 1}});
                },
            }
        ],
    }
});

publishComposite('company.testsTitles', function () {
    return {
        find() {
            return Surveys.find({
                test: true,
                company: Meteor.user().company
            }, {fields: {title: 1}});
        },
        children: [],
    }
});


