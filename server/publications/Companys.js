

publishComposite('company.data', function () {
  if (!this.userId) {
    return this.ready();
  } else {
    return {
      find() {
        return Companys.find({_id: Meteor.user().company});
      },
      children: [
        {
          find(x){
            return Images.find({ _id: x.img}).cursor;
          }
        }
      ],
    }
  }
});
